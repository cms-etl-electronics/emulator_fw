`timescale 10ps / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Fri Feb 12 12:40:40 CST 2021
//
// Module Name: deserializer with trigger data
// Project Name: ETROC2 readout
// Description: 
// Dependencies: 
// 
// LSB first deserializer
//  for logic check only
//
// revision: 2022-04-18

//////////////////////////////////////////////////////////////////////////////////


module DeserializerWithTriggerData2 (
    input                   bitCK,
	 input   clk8,//2022
	 input  clk40RB,//2022
    input                   reset,
    input                   disSCR,
    input [1:0]             rate,      //serializer rate
    input [4:0]             delay,   //delay from 0 to 31
	input                   sin,         //serial data
    input [4:0]             trigDataSize,   //from 0 to 16
    output [15:0]           trigData,
    output                  wordTrigClk,
    output                  word40CK,
	output  BadSync6sec,    // The Sync become bad for a few seconds 
 	output  Trig1b,    // Tigger bit 
	output  DV3p,    // data valid of the output data 

	output [39:0]       dout         //input data
);

    wire word32CK;
    wire [31:0] word32;
    deserializer #(.WORDWIDTH(32),.WIDTH(5)) des32(
        .bitCK(bitCK),
        .reset(reset),
        .disSCR(disSCR),
        .delay(delay),
        .sin(sin),
        .wordCK(word32CK),
        .dout(word32)
    );

    wire word16CK;
    wire [15:0] word16;
    deserializer #(.WORDWIDTH(16),.WIDTH(4)) des16(
        .bitCK(bitCK),
        .reset(reset),
        .disSCR(disSCR),
        .delay(delay[3:0]),
        .sin(sin),
        .wordCK(word16CK),
        .dout(word16)
    );

    wire word8CK;
    wire [7:0] word8;
    deserializer #(.WORDWIDTH(8),.WIDTH(3)) des8(
        .bitCK(bitCK),
		  .clk40RB(clk40RB),//2022
        .reset(reset),
        .disSCR(disSCR),
        //.delay(delay[2:0]),
        .delay(3'b000),
        .sin(sin),
        .wordCK(word8CK),
        .dout(word8)
    );

    wire [7:0] OutWord40b;
    WordHeaderFinder1 WordHeaderFinder1(
        .bitCK(bitCK),
		  .clk40RB(clk40RB),//2022
        .RawWord(word8),
        .BadSync6sec(BadSync6sec),
        .Trig1b(Trig1b),
        .DV3p(DV3p),
        .dout(OutWord40b)
    );
	 
	 
    assign wordTrigClk = rate == 2'b00 ? word8CK :(rate == 2'b01 ? word16CK : word32CK);

    reg [10 : 0] counter;  
    wire [10:0] stepsize8 = {4'd0,(5'd8 - trigDataSize),2'b00};
    wire [10:0] stepsize16 = {5'd0,(5'd16 - trigDataSize),1'b0};
    wire [10:0] stepsize32 = {5'd0,(6'd32 - {1'b0,trigDataSize})};

    wire [10:0] stepsize = rate == 2'b00 ? stepsize8 : (rate == 2'b01 ? stepsize16 : stepsize32);

    reg [127:0] delayCell;
    wire delayedRST;
	 // 2022 for setup slack  deleted dbw
//    always @(posedge bitCK) 
//    begin
//            delayCell <= {delayCell[126:0],reset};
//    end
//    assign delayedRST = delayCell[127];
// 2022 end
	 assign delayedRST =reset;


    assign dout = OutWord40b;

endmodule