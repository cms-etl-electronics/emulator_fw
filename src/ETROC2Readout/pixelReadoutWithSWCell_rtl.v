//all verilog files for pixelReadoutWithSWCell.v 
//if define pixelTMR, this includes all files for pixelReadout module for synthersis. 
//Only for latch-based memory
//define LATCH_RAM,ICG_LMEM,INITIALIZE_MEMORY

`include "commonDefinition.v"
`include "pixelReadoutWithSWCell.v"
`include "SWCell.v"
`include "pixelReadout.v"
`ifdef pixelTMR
    `include "pixelTMR2/TDCTestPatternGenTMR.v"
    `include "pixelTMR2/PRBS31TMR.v"
    `include "pixelTMR2/TestL1GeneratorTMR.v"
    `include "pixelTMR2/BCIDCounterTMR.v"
    `include "majorityVoter.v"
    `include "fanout.v"
`else
    `include "TDCTestPatternGen.v"
    `include "PRBS31.v"
    `include "TestL1Generator.v"
    `include "BCIDCounter.v"
`endif
`include "CircularBuffer.v"
`include "gateClockCell.v"
`include "memBlock/cb_data_mem_rtl.v"
`include "hitSRAMCircularBuffer.v"
`include "memBlock/cb_hit_mem8_rtl.v"
`include "L1EventBuffer.v"
`include "hitSRAML1Buffer.v"
`include "memBlock/L1_hit_mem_rtl.v"
`include "latchedBasedRAM.v"
