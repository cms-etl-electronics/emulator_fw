//all verilog files for pixelReadoutWithSWCell.v 
//This version is special for debug purpose

`include "pixelReadoutTMR/BCIDCounterTMR.v"
`include "pixelReadoutTMR/cb_data_mem_rtlTMR.v"
`include "pixelReadoutTMR/cb_hit_mem8_rtlTMR.v"
`include "pixelReadoutTMR/CircularBufferTMR.v"
`include "pixelReadoutTMR/gateClockCellTMR.v"
`include "pixelReadoutTMR/hitSRAMCircularBufferTMR.v"
`include "pixelReadoutTMR/hitSRAML1BufferTMR.v"
`include "pixelReadoutTMR/L1_hit_mem_rtlTMR.v"
`include "pixelReadoutTMR/L1EventBufferTMR.v"
`include "pixelReadoutTMR/latchedBasedRAM.v"
`include "pixelReadoutTMR/pixelReadoutTMR.v"
`include "pixelReadoutTMR/pixelReadoutWithSWCell.v"
`include "pixelReadoutTMR/PRBS31TMR.v"
`include "pixelReadoutTMR/TDCTestPatternGenTMR.v"
`include "pixelReadoutTMR/TestL1GeneratorTMR.v"
`include "pixelReadoutTMR/SWCell.v"

