//all verilog files for pixelReadoutWithSWCell.v 
//This version is special for debug purpose

`include "commonDefinition.v"
`include "pixelReadoutTMR/pixelReadoutWithSWCell_include.v"
`include "pixelReadoutTMR/pixelReadoutCol.v"
`include "./pixelReadoutCol_tb_ocp01.v"

`include "globalReadoutForPixelTest.v"
`include "TestL1Generator.v"
`include "TDCTestPatternGen.v"
`include "BCIDCounter.v"
`include "BCIDBuffer.v"
`include "globalReadoutController.v"
`include "CircularBufferAddr.v"
`include "L1BufferAddr.v"
`include "FIFOWRCtrlerDDR.v"
`include "memBlock/cb_data_mem_decoder.v"
`include "memBlock/BCID_mem_rtl.v"
`include "PRBS31.v"
