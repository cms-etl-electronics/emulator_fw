
`define SIM
module DELWRAPPER(
  input I,
  output Z
);
// tmrg do_not_touch
// tmrg do_not_triplicate DL

`ifdef SIM
  assign #(700:1000:1300) Z = I;
`else
  DEL1 DL(.I(I), .Z(Z));
`endif


endmodule
