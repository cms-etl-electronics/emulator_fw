`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Mon Oct 25 16:58:00 CDT 2021
// Module Name: triggerSynchronizer
// Project Name: ETROC2 readout
// Description: 
// Dependencies: 
// 


//////////////////////////////////////////////////////////////////////////////////


module triggerSynchronizer(
    input           clk,            //
    input           reset,
    input [15:0]    encTrigHits,       //16 bits trigger bits
    input [5:0]     trigDataSize,   //output trigger data size:16,8,4,2,1. If trigDataSize is 0, disabled
    input [11:0]    gapPosition,
    output [11:0]   syncBCID,
    output          recSynched,
    output [15:0]   trigHits       //encoded trigHits  
);
localparam init = 2'b00;
localparam searching = 2'b01;
localparam synched = 2'b10;
localparam BCIDPeriod = 12'd3564;

wire [15:0] prbs;
wire [11:0] recBCID;
reg [15:0] trigHitsReg;
reg [11:0] syncBCIDReg;
assign trigHits = trigHitsReg;
assign syncBCID = syncBCIDReg;

PRBS17 PRBS17Rec
(
        .clk(clk),
        .reset(reset),
        .dis(1'b0),
        .enableBCID(1'b1),
        .BCID(recBCID),
        .prbs(prbs)
);

reg [7:0] confirmCount;
reg [15:0] tablePRBS [3563:0];
reg [11:0] address;
reg [1:0] state;
reg firstMatch;
wire [7:0] targetTimes;
wire [15:0] mask;
assign targetTimes = trigDataSize == 5'd16 ? 8'd8 : (
                     trigDataSize == 5'd8  ? 8'd16 : (
                     trigDataSize == 5'd4  ? 8'd32 : (
                     trigDataSize == 5'd2  ? 8'd64 : 8'd128
                     )));  
assign mask = trigDataSize == 5'd16 ? 16'HFFFF : (
                     trigDataSize == 16'd8  ? 16'H00FF : (
                     trigDataSize == 16'd4  ? 16'H000F : (
                     trigDataSize == 16'd2  ? 16'H0003 : 16'H0001
                     )));  

always @(posedge clk) 
begin
    if(~reset)
    begin
        state <= init;
        address <= 12'd0;
    end
    else if(state == init)
    begin
        if(address == (BCIDPeriod-1))
        begin
            state <=  searching;
            confirmCount <= 8'd0;
            firstMatch <= 1'b0;
            tablePRBS[address][0] <= prbs[0]&mask[0];
            tablePRBS[address][1] <= prbs[1]&mask[1];
            tablePRBS[address][2] <= prbs[2]&mask[2];
            tablePRBS[address][3] <= prbs[3]&mask[3];
            tablePRBS[address][4] <= prbs[4]&mask[4];
            tablePRBS[address][5] <= prbs[5]&mask[5];
            tablePRBS[address][6] <= prbs[6]&mask[6];
            tablePRBS[address][7] <= prbs[7]&mask[7];
            tablePRBS[address][8] <= prbs[8]&mask[8];
            tablePRBS[address][9] <= prbs[9]&mask[9];
            tablePRBS[address][10] <= prbs[10]&mask[10];
            tablePRBS[address][11] <= prbs[11]&mask[11];
            tablePRBS[address][12] <= prbs[12]&mask[12];
            tablePRBS[address][13] <= prbs[13]&mask[13];
            tablePRBS[address][14] <= prbs[14]&mask[14];
            tablePRBS[address][15] <= prbs[15]&mask[15];
        end
        else
        begin
            address <= (address+1)%BCIDPeriod; 
//            tablePRBS[address] <= prbs; 
            tablePRBS[address][0] <= prbs[0]&mask[0];
            tablePRBS[address][1] <= prbs[1]&mask[1];
            tablePRBS[address][2] <= prbs[2]&mask[2];
            tablePRBS[address][3] <= prbs[3]&mask[3];
            tablePRBS[address][4] <= prbs[4]&mask[4];
            tablePRBS[address][5] <= prbs[5]&mask[5];
            tablePRBS[address][6] <= prbs[6]&mask[6];
            tablePRBS[address][7] <= prbs[7]&mask[7];
            tablePRBS[address][8] <= prbs[8]&mask[8];
            tablePRBS[address][9] <= prbs[9]&mask[9];
            tablePRBS[address][10] <= prbs[10]&mask[10];
            tablePRBS[address][11] <= prbs[11]&mask[11];
            tablePRBS[address][12] <= prbs[12]&mask[12];
            tablePRBS[address][13] <= prbs[13]&mask[13];
            tablePRBS[address][14] <= prbs[14]&mask[14];
            tablePRBS[address][15] <= prbs[15]&mask[15];

        end
    end
    else if(state == searching)
    begin
        if(confirmCount == targetTimes )
        begin
            state <=  synched;
            syncBCIDReg <= (gapPosition+1)%BCIDPeriod;
        end
        else if(firstMatch == 1'b0)
        begin
            if(tablePRBS[gapPosition] == encTrigHits)begin
                address <= 12'd0; //counter 
                firstMatch <= 1'b1;
            end
        end
        else
        begin
            address <= (address+1)%BCIDPeriod;
            if(address == BCIDPeriod-1)begin
                if(tablePRBS[gapPosition] == encTrigHits)begin
                    confirmCount <= confirmCount + 1;
                    address <= 12'd0;
                end
                else begin
                    confirmCount <= 1'b0;
                    firstMatch <= 1'b0; //start over again                    
                end
            end 
        end
    end
    else if (state == synched)
    begin
        syncBCIDReg <= (syncBCIDReg + 1)%BCIDPeriod;
        trigHitsReg[0] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][0]^encTrigHits[0];
        trigHitsReg[1] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][1]^encTrigHits[1];
        trigHitsReg[2] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][2]^encTrigHits[2];
        trigHitsReg[3] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][3]^encTrigHits[3];
        trigHitsReg[4] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][4]^encTrigHits[4];
        trigHitsReg[5] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][5]^encTrigHits[5];
        trigHitsReg[6] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][6]^encTrigHits[6];
        trigHitsReg[7] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][7]^encTrigHits[7];
        trigHitsReg[8] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][8]^encTrigHits[8];
        trigHitsReg[9] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][9]^encTrigHits[9];
        trigHitsReg[10] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][10]^encTrigHits[10];
        trigHitsReg[11] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][11]^encTrigHits[11];
        trigHitsReg[12] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][12]^encTrigHits[12];
        trigHitsReg[13] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][13]^encTrigHits[13];
        trigHitsReg[14] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][14]^encTrigHits[14];
        trigHitsReg[15] <= tablePRBS[(syncBCIDReg+1)%BCIDPeriod][15]^encTrigHits[15];
    end
end

assign recSynched = (state == synched);

endmodule