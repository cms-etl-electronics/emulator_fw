`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Sat May  1 21:08:38 CDT 2021
// Module Name: DelayLine
// Project Name: ETROC2 readout
// Description: 
// Dependencies: No
// 
// Revision:
// Revision 0.01 - File Created

// 
//////////////////////////////////////////////////////////////////////////////////

module delayLine
(
    input bitClk,
    input [5:0] delay,
    input sin,  //input serial data 
    output sout  //output serial data
);

	// 2022 
	reg sin_r;
	always @( posedge bitClk) begin
		sin_r<=sin;
	end
	
	// 2022  end
    reg[63:0] mem;
    //2022 always @( posedge bitClk) begin
    always @( negedge bitClk) begin
        mem[63:0] <={mem[62:0],sin};  //2022{mem[62:0],sin_r};
    end

   // assign sout = mem[delay[5:0]];
	assign sout= sin_r;// 20220215

endmodule
