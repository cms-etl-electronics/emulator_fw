
//all verilog files needed for globalReadout_tb.v
`include "globalReadout_tb.v"
`include "pixelReadoutWithSWCell_rtl.v"
`include "globalReadout_rtl.v"

`include "fullChainDataCheck.v"
`include "pixelReadoutCol.v"
`include "SixteenColChain.v"
`include "DataExtract.v"
`include "DataExtractUnit.v"
`include "Descrambler.v"
`include "Deserializer.v"
`include "DeserializerWithTriggerData.v"
`include "dataRecordCheck.v"
`include "delayLine.v"
`include "multiplePixelL1TDCDataCheck.v"
`include "PRBS7.v"
