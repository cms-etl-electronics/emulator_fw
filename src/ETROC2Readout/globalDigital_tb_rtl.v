
//all verilog files needed for globalDigital_tb.v
`define GLOBALTMR

`include "globalDigital_tb.v"
`include "pixelReadoutWithSWCell_rtl.v"
`ifdef GLOBALTMR
`include "./globalTMR/globalDigitalTMR_include.v"
`else
`include "globalDigital_rtl.v"
`endif
`include "fullChainDataCheck.v"
`include "dataStreamCheck.v"
`include "pixelReadoutCol.v"
`include "SixteenColPixels.v"
`include "DataExtract.v"
`include "DataExtractUnit.v"
`include "Descrambler.v"
`include "Deserializer.v"
`include "DeserializerWithTriggerData.v"
`include "dataRecordCheck.v"
`include "delayLine.v"
`include "multiplePixelL1TDCDataCheck.v"
`include "PRBS7.v"
`include "CRC8.v"

