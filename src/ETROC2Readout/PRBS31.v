`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Thu Feb 25 15:08:00 CST 2021
// Module Name: PRBS31
// Project Name: ETROC2 readout
// Description: 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// polynomial:  
// X^31 + X^28 + 1
// 
//////////////////////////////////////////////////////////////////////////////////

// `ifndef PRBS31
// `define PRBS31
//pure combinational logic
`ifndef NOPIXEL_MEM
module forwardPRBS31Seed #(
    parameter WORDWIDTH = 15,
    parameter FORWARDSTEPS = 1
)
(
    input [30:0] seed,
    output [30:0] newSeed
);
// tmrg default triplicate

    wire [30:0] c[WORDWIDTH*FORWARDSTEPS:0]; //chain for iteration
    assign c[0] = seed;
    generate
        genvar i;
        for (i = 0 ; i < WORDWIDTH*FORWARDSTEPS; i = i + 1)
        begin : loop_itr
            assign c[i+1] = {c[i][3]^c[i][0],c[i][30:1]}; //LSB out
        end
    endgenerate
    assign newSeed = c[WORDWIDTH*FORWARDSTEPS];
endmodule
`endif

module nextPRBS31Word #(
    parameter WORDWIDTH = 15
)
(
    input  [30:0] seed,
    output [30:0] nextWord
);
// tmrg default triplicate

    wire [30:0] c[WORDWIDTH:0]; //chain for iteration
    assign c[0] = seed;
    generate
        genvar i;
        for (i = 0 ; i < WORDWIDTH; i = i + 1)
        begin : loop_itr
 //           assign nextWord[i] = c[i][3]^c[i][0];    
            assign c[i+1] = {c[i][3]^c[i][0],c[i][30:1]}; //LSB out
        end
    endgenerate
    assign nextWord = c[WORDWIDTH];
endmodule

`ifndef NOPIXEL_MEM
module PRBS31 #(
    parameter WORDWIDTH = 15,   //WORDWIDTH < 31
    parameter FORWARDSTEPS = 1
)
`else
module PRBS31 #(
    parameter WORDWIDTH = 15   //WORDWIDTH < 31
)
`endif
(
	input           clk,            //40MHz
	input           reset,         //
	input           dis,          //
    input [30:0]    seed,
	output [WORDWIDTH-1:0]    prbs
);
// tmrg default triplicate

    reg [30:0] rTMR;
    wire [30:0] newSeed;
`ifndef NOPIXEL_MEM
    forwardPRBS31Seed #(  .WORDWIDTH(WORDWIDTH),
                    .FORWARDSTEPS(FORWARDSTEPS))forwardPRBS31SeedInst
    (
        .seed(seed),
        .newSeed(newSeed)
    );
`else
    assign newSeed = seed;
`endif
    wire [30:0] nextWord;
    wire [30:0] nextWordVoted = nextWord;

    always @(posedge clk) 
    begin
        if(!reset)
        begin
            rTMR <= newSeed;
        end
        else if(!dis)
        begin
            rTMR <= nextWordVoted;
        end            
    end

    nextPRBS31Word #(.WORDWIDTH(WORDWIDTH)) nextWordInst
    (
        .seed(rTMR),
        .nextWord(nextWord)
    );

    assign prbs = rTMR[WORDWIDTH-1:0];

 endmodule
// `endif