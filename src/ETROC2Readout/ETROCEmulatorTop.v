`timescale 1ns / 10ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Thu Dec 16 12:23:48 CST 2021
// Module Name: ETROCEmulatorTop
// Project Name: ETROC2 readout
// Description: 
// Dependencies: globalReadout
// 
// Revision:
// Revision 0.01 - File Created

// 
//////////////////////////////////////////////////////////////////////////////////
//`define DEBUG 
`include "commonDefinition.v"

module ETROCEmulatorTop #(
    parameter L1ADDRWIDTH = 7,
    parameter BCSTWIDTH = 27,
    parameter PIXELROW = 16
)
(
//clock and reset signals
	input                   clk,            //40MHz
    input                   clk1280,        //1280 MHz clock
    input                   clk320,
	 input                   clk40,
    input                   clkPolarity,
    input                   reset,          //reset signal from slow control
    input [16:0]            chipId,
//slow control configuration
    //readout clock 
    // input [4:0]             readoutClockDelayPixel,
    // input [4:0]             readoutClockWidthPixel,
    // input [4:0]             readoutClockDelayGlobal,
    // input [4:0]             readoutClockWidthGlobal,
    //serializer
 //   input [1:0]             serRateRight,        //rate of serializer.
//    input [1:0]             serRateLeft,         //rate of serializer.
    input                   linkResetSlowControl,
    input                   linkResetTestPattern, //0: PRBS7, 1: fixed pattern specified by user
    input [31:0]            linkResetFixedPattern,                  
    //trigger
    input [11:0]            emptySlotBCID,       
    input [2:0]             triggerGranularity,   //3-bit            how many bits of trigger data, from 0 to 16.
    //frame builder
    input                   disScrambler,

    //data source switcher
    input                   mergeTriggerData,   //if combine data and trig when there are two ports. if there is only one port, it is meaningless.
    input                   singlePort,      //use two output ports or single port
    //on-chip L1A
//	input [1:0]             onChipL1AConf,      //00 and 01, onchip L1A disable, 10, onchip L1A is periodic, 11, onchip L1A is random
    //BCID counter
//    input [11:0]            BCIDoffset,     //
    //fast command  
//	input                   fcSelfAlign,			// 
//	input                   fcAlignStart,		    // fast command clock align command. initialize the clock phase alignment process at its rising edge -sefAligner
//	input                   fcClkDelayEn,			// enable signal of the clock delay -manual
//	input                   fcDataDelayEn,			// enable signal of the command delay  -manual
//    output                  readoutClock,
//slow control status
//	output                  fcBitAlignError,	// error indicator of the bit alignment -sefAligner
//	output [3:0]            fcBitAlignStatus,			// detailed error indicator of the bit alignment -sefAligner
//fast command interface
	input                   fcData,					// fast command input
//	output [3:0]            fcAlignFinalState,// state of the bit alignment state machine -sefAligner
//frontend
	// output                  chargeInjection,   //charge injection signal
    // input  [4:0]            chargeInjectionDelay,
//fast commands for waveform sampler
    output                  wsStart,
    output                  wsStop,
//pixels interface
//	input [1:0]     workMode,      //00: normal, 01: self test, periodic trigger fixed TDC data, 10: self test, random TDC data, 11: reserved
//    input [8:0]     L1ADelay,
//    input [8447:0]  TDCDataArray, //TDC source, 30 bits a TDC data, 256 TDCs
	input [6:0]     ocp,  //0.1%, 1%, 2%, 5%, 10% etc
//    input [255:0]   disDataReadout,     //disable data readout

//for trigger path
//    input [255:0]   disTrigPath,
    input           addrOffset,
//output to serializer
    output                  soutRight,  //serializer output
    output                  soutLeft  //serializer output
 //2022output [39:0] dataframe_debug_right,
  //2022  output [39:0] dataframe_debug_left 
	 );

    wire [735:0] colDataChain;
    wire [15:0] colHitChain;
    wire [63:0] trigHitsColumn;
    wire [15:0] colReadChain;
    wire [BCSTWIDTH*16-1:0] colBCSTChain;

    sixteenColPixels #(.L1ADDRWIDTH(`L1BUFFER_ADDRWIDTH),.BCSTWIDTH(BCSTWIDTH),.PIXELROW(PIXELROW))sixteenColPixelsInst(
        .clk(clk),            //40MHz
	    .workMode(2'b10),          //selfTest or not
        .L1ADelay(9'd501),
        .TDCDataArray({33*PIXELROW*16{1'b0}}),  
	    .selfTestOccupancy(ocp),  //0.1%, 1%, 2%, 5%, 10% etc
        .disDataReadout({PIXELROW*16{1'b0}}),
        .disTrigPath({16*PIXELROW{1'b0}}),
        .upperTOATrig(10'h3FF),
        .lowerTOATrig(10'h000),
        .upperTOTTrig(9'h1FF),
        .lowerTOTTrig(9'h000),
        .upperCalTrig(10'h3FF),
        .lowerCalTrig(10'h000),
        .upperTOA(10'h3FF),
        .lowerTOA(10'h000),
        .upperTOT(9'h1FF),
        .lowerTOT(9'h000),
        .upperCal(10'h3FF),
        .lowerCal(10'h000),
        .addrOffset(1'b1),
        .colDataChain(colDataChain),
        .colHitChain(colHitChain),
        .trigHitsColumn(trigHitsColumn),
        .colReadChain(colReadChain),
        .colBCSTChain(colBCSTChain)
    );

    wire [3:0] fc_state_bitAlign;
    wire [3:0] fc_ed;
//    wire readoutClock;

   globalDigital #(.L1ADDRWIDTH(`L1BUFFER_ADDRWIDTH),.BCSTWIDTH(BCSTWIDTH))globalDigitalInst(
 //clock and reset signals
	    .clk(clk),//2022 (clk),            //40MHz
        .clk1280(clk1280),
        .clk320(clk320),
		  .clk40(clk40),
        .clkPolarity(clkPolarity),
        .reset(reset),        //reset by slow control? 
//        .readoutClock(readoutClock),
        .chipId(chipId),

//slow control configuration
    //serializer
        // .readoutClockDelayPixel(readoutClockDelayPixel),
        // .readoutClockWidthPixel(readoutClockWidthPixel),
        // .readoutClockDelayGlobal(5'd0),
        // .readoutClockWidthGlobal(5'd16),
        .serRateRight(2'b00),
        .serRateLeft(2'b00),
        .linkResetSlowControl(linkResetSlowControl),
        .linkResetTestPattern(linkResetTestPattern),
        .linkResetFixedPattern(linkResetFixedPattern),
    //trigger
        .emptySlotBCID(emptySlotBCID),
        .triggerGranularity(triggerGranularity),
    //frame builder
        .disScrambler(disScrambler),
    //data source switcher
        .mergeTriggerData(mergeTriggerData),
        .singlePort(singlePort),
    //test pattern
	    .onChipL1AConf(2'b01),//2022  (2'b11),          
    //BCID counter
        .BCIDoffset(12'h000),
//fast command  
//        .fcSelfAlign(1'b1),
 //       .fcAlignStart(fcAlignStart),
  //      .fcClkDelayEn(1'b0),
  //      .fcDataDelayEn(1'b0),
    //slow control status
  //      .fcBitAlignError(fcBitAlignError),
   //     .fcBitAlignStatus(fcBitAlignStatus),
//fast command interface
        .fcData(fcData),
   //     .fcAlignFinalState(fcAlignFinalState),
//frontend
        // .chargeInjection(chargeInjection),
        // .chargeInjectionDelay(chargeInjectionDelay),
//waveform sampler
        .wsStart(wsStart),
        .wsStop(wsStop),
//pixel interface over SW network
        .colDataChain(colDataChain),
        .colHitChain(colHitChain),
        .trigHitsColumn(trigHitsColumn),
        .colReadChain(colReadChain),
        .colBCSTChain(colBCSTChain),
//output to serializer
        .soutRight(soutRight),   //serializer output
        .soutLeft(soutLeft)   //serializer output
		  //2022 .dataframe_debug_right(dataframe_debug_right),
        //2022.dataframe_debug_left(dataframe_debug_left)
    );       

endmodule
