`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Tue Oct 19 10:30:06 CDT 2021
// Module Name: globalDigital_tb
// Project Name: ETROC2 readout
// Description: 
// Dependencies: No
// 
// Revision:
// Revision 0.01 - File Created

// 
//////////////////////////////////////////////////////////////////////////////////
`include "commonDefinition.v"
//`define TEST_SERRIALIZER_1280
//`define TEST_SERRIALIZER_640
//`define TEST_SERRIALIZER_320
`define TEST_SCRAMBLER
`define GLOBALTMR
module globalDigital_tb;

    localparam BCSTWIDTH = `L1BUFFER_ADDRWIDTH*2+11+2;

    localparam fc_IDLE 		= 8'b1111_0000; //2'hF0 -->n_fc = 0--3'h001 

	reg clk;
    reg clk1280;
    reg clk640;
    reg clk320;
    reg fccAlign;
    reg [16:0] chipId;

    reg [2:0] cnt_fc;
    reg [7:0] fc_byte;
    always@(posedge clk320) begin
        cnt_fc <= cnt_fc + 1;
        if(cnt_fc==3'd0)
            fc_byte <= fc_IDLE; //sampling parallel 8 bits fc at 320 MHz clock
        else
            fc_byte[7:0] <= {fc_byte[6:0],fc_byte[7]}; //shift 8-bit parallel fc in 40 MHz period, serializing to 1 bit
    end

    wire fc_fc;
    assign fc_fc = fc_byte[7];

    reg initReset;
    reg reset2;
    reg dnReset;
    reg [1:0] rateRight; //serializer speed
    reg [1:0] rateLeft; //serializer speed
    reg [6:0] ocp;
    reg disSCR;
    reg mergeTrigData;
    reg singlePort;

    // wire [1:0] rate;

    wire [735:0] colDataChain;
    wire [15:0] colHitChain;
    wire [63:0] trigHitsColumn;
    wire [15:0] colReadChain;
    wire [BCSTWIDTH*16-1:0] colBCSTChain;

    sixteenColPixels #(.L1ADDRWIDTH(`L1BUFFER_ADDRWIDTH),.BCSTWIDTH(BCSTWIDTH))sixteenColPixelsInst(
        .clk(clk),            //40MHz
	    .workMode(2'b10),          //selfTest or not
        .L1ADelay(9'd501),
        .TDCDataArray({8448{1'b0}}),  
	    .selfTestOccupancy(ocp),  //0.1%, 1%, 2%, 5%, 10% etc
        .disDataReadout({256{1'b0}}),
        .disTrigPath({256{1'b0}}),
        .upperTOATrig(10'h3FF),
        .lowerTOATrig(10'h000),
        .upperTOTTrig(9'h1FF),
        .lowerTOTTrig(9'h000),
        .upperCalTrig(10'h3FF),
        .lowerCalTrig(10'h000),
        .upperTOA(10'h3FF),
        .lowerTOA(10'h000),
        .upperTOT(9'h1FF),
        .lowerTOT(9'h000),
        .upperCal(10'h3FF),
        .lowerCal(10'h000),
        .addrOffset(1'b1),
        .colDataChain(colDataChain),
        .colHitChain(colHitChain),
        .trigHitsColumn(trigHitsColumn),
        .colReadChain(colReadChain),
        .colBCSTChain(colBCSTChain)
    );

    wire soutRight;
    wire [2:0] triggerGranularity = 3'd1;

    wire soutLeft;
    wire ws_start;
    wire ws_stop;
    wire chargeInjection;
    wire [3:0] fc_state_bitAlign;
    wire [3:0] fc_ed;
    wire readoutClock;

`ifdef GLOBALTMR
    globalDigitalTMR #(.L1ADDRWIDTH(`L1BUFFER_ADDRWIDTH),.BCSTWIDTH(BCSTWIDTH))globalDigitalInstTMR(
//clock and reset signals
	    .clkA(clk),       
	    .clkB(clk),            
	    .clkC(clk),            
        .clk1280A(clk1280),
        .clk1280B(clk1280),
        .clk1280C(clk1280),
        .resetA(dnReset),    
        .resetB(dnReset),        
        .resetC(dnReset),        
        .readoutClock(readoutClock),
        .chipIdA(chipId),
        .chipIdB(chipId),
        .chipIdC(chipId),
        .readoutClockDelayPixelA(5'd0),
        .readoutClockDelayPixelB(5'd0),
        .readoutClockDelayPixelC(5'd0),

        .readoutClockWidthPixelA(5'd16),
        .readoutClockWidthPixelB(5'd16),
        .readoutClockWidthPixelC(5'd16),

        .readoutClockDelayGlobalA(5'd0),
        .readoutClockDelayGlobalB(5'd0),
        .readoutClockDelayGlobalC(5'd0),

        .readoutClockWidthGlobalA(5'd16),
        .readoutClockWidthGlobalB(5'd16),
        .readoutClockWidthGlobalC(5'd16),

        .serRateRightA(rateRight),
        .serRateRightB(rateRight),
        .serRateRightC(rateRight),

        .serRateLeftA(rateLeft),
        .serRateLeftB(rateLeft),
        .serRateLeftC(rateLeft),

        .linkResetSlowControlA(1'b0),
        .linkResetSlowControlB(1'b0),
        .linkResetSlowControlC(1'b0),

        .linkResetTestPatternA(1'b1),
        .linkResetTestPatternB(1'b1),
        .linkResetTestPatternC(1'b1),

        .linkResetFixedPatternA(32'h3C5C3C5A),
        .linkResetFixedPatternB(32'h3C5C3C5A),
        .linkResetFixedPatternC(32'h3C5C3C5A),

        .emptySlotBCIDA(12'd1177),
        .emptySlotBCIDB(12'd1177),
        .emptySlotBCIDC(12'd1177),

        .triggerGranularityA(triggerGranularity),
        .triggerGranularityB(triggerGranularity),
        .triggerGranularityC(triggerGranularity),

        .disScramblerA(disSCR),
        .disScramblerB(disSCR),
        .disScramblerC(disSCR),

        .mergeTriggerDataA(mergeTrigData),
        .mergeTriggerDataB(mergeTrigData),
        .mergeTriggerDataC(mergeTrigData),

        .singlePortA(singlePort),
        .singlePortB(singlePort),
        .singlePortC(singlePort),

	    .onChipL1AConfA(2'b11),          
	    .onChipL1AConfB(2'b11),          
	    .onChipL1AConfC(2'b11),          

        .BCIDoffsetA(12'h000),
        .BCIDoffsetB(12'h000),
        .BCIDoffsetC(12'h000),

        .fcSelfAlignA(1'b1),
        .fcSelfAlignB(1'b1),
        .fcSelfAlignC(1'b1),

        .fcAlignStartA(fccAlign),
        .fcAlignStartB(fccAlign),
        .fcAlignStartC(fccAlign),

        .fcClkDelayEnA(1'b0),
        .fcClkDelayEnB(1'b0),
        .fcClkDelayEnC(1'b0),

        .fcDataDelayEnA(1'b0),
        .fcDataDelayEnB(1'b0),
        .fcDataDelayEnC(1'b0),

        .fcBitAlignErrorA(fc_bitError),
        .fcBitAlignErrorB(fc_bitError),
        .fcBitAlignErrorC(fc_bitError),

        .fcBitAlignStatusA(fc_ed),
        .fcBitAlignStatusB(fc_ed),
        .fcBitAlignStatusC(fc_ed),

        .fcData(fc_fc),

        .fcAlignFinalStateA(fc_state_bitAlign),
        .fcAlignFinalStateB(fc_state_bitAlign),
        .fcAlignFinalStateC(fc_state_bitAlign),

        .chargeInjection(chargeInjection),

        .chargeInjectionDelayA(5'd24),
        .chargeInjectionDelayB(5'd24),
        .chargeInjectionDelayC(5'd24),

        .wsStartA(ws_start),
        .wsStartB(ws_start),
        .wsStartC(ws_start),

        .wsStopA(ws_stop),
        .wsStopB(ws_stop),
        .wsStopC(ws_stop),

        .colDataChain(colDataChain),
        .colHitChain(colHitChain),
        .trigHitsColumn(trigHitsColumn),
        .colReadChain(colReadChain),
        .colBCSTChain(colBCSTChain),
        .soutRight(soutRight),   //serializer output
        .soutLeft(soutLeft)   //serializer output
    );
`else 
   globalDigital #(.L1ADDRWIDTH(`L1BUFFER_ADDRWIDTH),.BCSTWIDTH(BCSTWIDTH))globalDigitalInst(
 //clock and reset signals
	    .clk(clk),            //40MHz
        .clk1280(clk1280),
        .reset(dnReset),        //reset by slow control? 
        .readoutClock(readoutClock),
        .chipId(chipId),

//slow control configuration
    //serializer
        .readoutClockDelayPixel(5'd0),
        .readoutClockWidthPixel(5'd16),
        .readoutClockDelayGlobal(5'd0),
        .readoutClockWidthGlobal(5'd16),
        .serRateRight(rateRight),
        .serRateLeft(rateLeft),
        .linkResetSlowControl(1'b0),
        .linkResetTestPattern(1'b1),
        .linkResetFixedPattern(32'h3C5C3C5A),
    //trigger
        .emptySlotBCID(12'd1177),
        .triggerGranularity(triggerGranularity),
    //frame builder
        .disScrambler(disSCR),
    //data source switcher
        .mergeTriggerData(mergeTrigData),
        .singlePort(singlePort),
    //test pattern
	    .onChipL1AConf(2'b11),          
    //BCID counter
        .BCIDoffset(12'h000),
//fast command  
        .fcSelfAlign(1'b1),
        .fcAlignStart(fccAlign),
        .fcClkDelayEn(1'b0),
        .fcDataDelayEn(1'b0),
    //slow control status
        .fcBitAlignError(fc_bitError),
        .fcBitAlignStatus(fc_ed),
//fast command interface
        .fcData(fc_fc),
        .fcAlignFinalState(fc_state_bitAlign),
//frontend
        .chargeInjection(chargeInjection),
        .chargeInjectionDelay(5'd24),
//waveform sampler
        .wsStart(ws_start),
        .wsStop(ws_stop),
//pixel interface over SW network
        .colDataChain(colDataChain),
        .colHitChain(colHitChain),
        .trigHitsColumn(trigHitsColumn),
        .colReadChain(colReadChain),
        .colBCSTChain(colBCSTChain),
//output to serializer
        .soutRight(soutRight),   //serializer output
        .soutLeft(soutLeft)   //serializer output
    );       

`endif

    wire [4:0] trigDataSizeRight;
    wire [4:0] trigDataSizeLeft;
    wire [4:0] trigDataSize;
    assign trigDataSize =    triggerGranularity == 3'd1 ? 5'd1 : 
                            (triggerGranularity == 3'd2 ? 5'd2 :
                            (triggerGranularity == 3'd3 ? 5'd4 :
                            (triggerGranularity == 3'd4 ? 5'd8 :
                            (triggerGranularity == 3'd5 ? 5'd16 : 5'd0))));
    assign trigDataSizeRight =  singlePort ? trigDataSize : 
                                (mergeTrigData ? trigDataSize >> 1 : 5'd0); 
    assign trigDataSizeLeft =  singlePort ? 5'd0 : 
                                (mergeTrigData ? trigDataSize >> 1 : trigDataSize);      


    wire right_aligned;
    wire [9:0] RC_right_goodEventRate;
    wire [1:0] RC_right_dataType;
    wire [19:0] RC_right_BCIDErrorCount;
    wire [19:0] RC_right_nullEventCount;
    wire [19:0] RC_right_goodEventCount;
    wire [19:0] RC_right_notHitEventCount;
    wire [19:0] RC_right_L1OverlfowEventCount;
    wire [19:0] RC_right_totalHitsCount;
    wire [19:0] RC_right_dataErrorCount;
    wire [19:0] RC_right_missedHitsCount;
    wire [8:0]  RC_right_hittedPixelCount;
    wire [19:0] RC_right_frameErrorCount;
    wire [19:0] RC_right_mismatchBCIDCount;
    wire [19:0] RC_right_L1FullEventCount;
    wire [19:0] RC_right_L1HalfFullEventCount;
    wire [19:0] RC_right_SEUEventCount;
    wire [19:0] RC_right_hitCountMismatchEventCount;
    wire [15:0] RC_right_triggerDataOut;

    fullChainDataCheck fullChainDataCheckInstRight
    (
        .clk320(clk320),
        .clk640(clk640),
        .clk1280(clk1280),
        .dataRate(rateRight),
        .chipId(chipId),
        .triggerDataSize(trigDataSizeRight),
        .reset(dnReset),
        .reset2(reset2),
        .aligned(right_aligned),
        .disSCR(disSCR),
        .serialIn(soutRight),
        .RC_BCIDErrorCount(RC_right_BCIDErrorCount),
        .RC_dataType(RC_right_dataType),
        .RC_nullEventCount(RC_right_nullEventCount),
        .RC_goodEventCount(RC_right_goodEventCount),
        .RC_notHitEventCount(RC_right_notHitEventCount),
        .RC_L1OverlfowEventCount(RC_right_L1OverlfowEventCount),
        .RC_totalHitsCount(RC_right_totalHitsCount),
        .RC_dataErrorCount(RC_right_dataErrorCount),
        .RC_missedHitsCount(RC_right_missedHitsCount),
        .RC_hittedPixelCount(RC_right_hittedPixelCount),
        .RC_frameErrorCount(RC_right_frameErrorCount),
        .RC_L1FullEventCount(RC_right_L1FullEventCount),
        .RC_L1HalfFullEventCount(RC_right_L1HalfFullEventCount),
        .RC_SEUEventCount(RC_right_SEUEventCount),
        .RC_goodEventRate(RC_right_goodEventRate),
        .RC_hitCountMismatchEventCount(RC_right_hitCountMismatchEventCount),
        .RC_mismatchBCIDCount(RC_right_mismatchBCIDCount),
        .RC_triggerDataOut(RC_right_triggerDataOut)
    );

    wire left_aligned;
    wire [9:0] RC_left_goodEventRate;
    wire [1:0] RC_left_dataType;
    wire [19:0] RC_left_BCIDErrorCount;
    wire [19:0] RC_left_nullEventCount;
    wire [19:0] RC_left_goodEventCount;
    wire [19:0] RC_left_notHitEventCount;
    wire [19:0] RC_left_L1OverlfowEventCount;
    wire [19:0] RC_left_totalHitsCount;
    wire [19:0] RC_left_dataErrorCount;
    wire [19:0] RC_left_missedHitsCount;
    wire [8:0]  RC_left_hittedPixelCount;
    wire [19:0] RC_left_frameErrorCount;
    wire [19:0] RC_left_mismatchBCIDCount;
    wire [19:0] RC_left_L1FullEventCount;
    wire [19:0] RC_left_L1HalfFullEventCount;
    wire [19:0] RC_left_SEUEventCount;
    wire [19:0] RC_left_hitCountMismatchEventCount;
    wire [15:0] RC_left_triggerDataOut;

    fullChainDataCheck fullChainDataCheckInstLeft
    (
        .clk320(clk320),
        .clk640(clk640),
        .clk1280(clk1280),
        .dataRate(rateLeft),
        .chipId(chipId),
        .triggerDataSize(trigDataSizeLeft),
        .reset(dnReset),
        .reset2(reset2),
        .aligned(left_aligned),
        .disSCR(disSCR),
        .serialIn(soutLeft),
        .RC_BCIDErrorCount(RC_left_BCIDErrorCount),
        .RC_dataType(RC_left_dataType),
        .RC_nullEventCount(RC_left_nullEventCount),
        .RC_goodEventCount(RC_left_goodEventCount),
        .RC_notHitEventCount(RC_left_notHitEventCount),
        .RC_L1OverlfowEventCount(RC_left_L1OverlfowEventCount),
        .RC_totalHitsCount(RC_left_totalHitsCount),
        .RC_dataErrorCount(RC_left_dataErrorCount),
        .RC_missedHitsCount(RC_left_missedHitsCount),
        .RC_hittedPixelCount(RC_left_hittedPixelCount),
        .RC_frameErrorCount(RC_left_frameErrorCount),
        .RC_L1FullEventCount(RC_left_L1FullEventCount),
        .RC_L1HalfFullEventCount(RC_left_L1HalfFullEventCount),
        .RC_SEUEventCount(RC_left_SEUEventCount),
        .RC_goodEventRate(RC_left_goodEventRate),
        .RC_hitCountMismatchEventCount(RC_left_hitCountMismatchEventCount),
        .RC_mismatchBCIDCount(RC_left_mismatchBCIDCount),
        .RC_triggerDataOut(RC_left_triggerDataOut)
    );

//     wire wordCK;
//     wire [39:0] word40b;
//     wire deserClk;
// `ifdef TEST_SERRIALIZER_1280
//     assign deserClk = clk1280; 
// `elsif TEST_SERRIALIZER_640
//     assign deserClk = clk640; 
// `elsif TEST_SERRIALIZER_320
//     assign deserClk = clk320; 
// `endif
//     wire dlSout;
//     delayLine dlInst(
//         .bitClk(deserClk),
//         .delay(6'd1),
//         .sin(sout),
//         .sout(dlSout)
//     );

//     wire [15:0] triggerDataOut;
//     wire [4:0] wordAddr;
//     wire wordTrigClk;
//     deserializerWithTriggerData dser40T(
//         .bitCK(deserClk),
//         .reset(dnReset),
//         .rate(rate),
//         .delay(wordAddr),
//     //    .delay(6'h02),
//         .sin(dlSout),
//         .trigData(triggerDataOut),
//         .trigDataSize(trigDataSize),
//         .wordTrigClk(wordTrigClk),
//         .word40CK(wordCK),
//         .dout(word40b)
//     );

//     wire aligned;
//     reg reset2;
//     wire [39:0] receivedDataFrame;
//     wire [9:0] RC_goodEventRate;
//     dataExtract dataExtractInst
//     (
//         .clk(wordCK),
//         .reset(reset2),
//         .wordAddr(wordAddr),
//         .disSCR(disSCR),
//         .din(word40b),
//         .aligned(aligned),
//         .goodEventRate(RC_goodEventRate),
//         .dout(receivedDataFrame)
//     );

// //`ifdef CHECK_DATA_AFTER_RECEIVER
//     wire [1:0] RC_dataType;
//     wire [19:0] RC_BCIDErrorCount;
//     wire [19:0] RC_nullEventCount;
//     wire [19:0] RC_goodEventCount;
//     wire [19:0] RC_notHitEventCount;
//     wire [19:0] RC_L1OverlfowEventCount;
//     wire [19:0] RC_totalHitsCount;
//     wire [19:0] RC_dataErrorCount;
//     wire [19:0] RC_missedHitsCount;
//     wire [8:0] RC_hittedPixelCount;
//     wire [19:0] RC_frameErrorCount;
//     wire [19:0] RC_mismatchBCIDCount;
//     wire [19:0] RC_L1FullEventCount;
//     wire [19:0] RC_L1HalfFullEventCount;
//     wire [19:0] RC_SEUEventCount;
//     wire [19:0] RC_hitCountMismatchEventCount;

//     dataRecordCheck dataRecordCheckReceiver
//     (
//         .clk(wordCK),
//         .reset(reset2),
//         .dataRecord(receivedDataFrame),
//         .BCIDErrorCount(RC_BCIDErrorCount),
//         .dataType(RC_dataType),
//         .nullEventCount(RC_nullEventCount),
//         .goodEventCount(RC_goodEventCount),
//         .notHitEventCount(RC_notHitEventCount),
//         .L1OverlfowEventCount(RC_L1OverlfowEventCount),
//         .totalHitsCount(RC_totalHitsCount),
//         .dataErrorCount(RC_dataErrorCount),
//         .missedHitsCount(RC_missedHitsCount),
//         .hittedPixelCount(RC_hittedPixelCount),
//         .frameErrorCount(RC_frameErrorCount),
//         .L1FullEventCount(RC_L1FullEventCount),
//         .L1HalfFullEventCount(RC_L1HalfFullEventCount),
//         .SEUEventCount(RC_SEUEventCount),
//         .goodEventRate(RC_goodEventRate),
//         .hitCountMismatchEventCount(RC_hitCountMismatchEventCount),
//         .mismatchBCIDCount(RC_mismatchBCIDCount)
//     );
// //`endif 


    initial begin
        clk = 0;
        clk1280 = 1'b0;
        clk640 = 0;
        clk320 = 0;
        cnt_fc = 3'd0;
        chipId = 17'H1ABCD;
	    fccAlign = 0;
        initReset = 1;
        reset2 = 1;
        ocp = 7'h02;  //2%
        disSCR = 1'b1;
        mergeTrigData = 1'b1;
        singlePort = 1'b0;
// `ifdef TEST_SERRIALIZER_1280
//         rateRight = 2'b11; //high speed readout
//         rateLeft = 2'b11; //high speed readout
// `elsif TEST_SERRIALIZER_640
//         rateRight = 2'b01; 
//         rateLeft = 2'b01; 
// `elsif TEST_SERRIALIZER_320
//         rateRight = 2'b00; 
//         rateLeft = 2'b00; 
// `endif

        rateRight = 2'b01; 
        rateLeft  = 2'b01; 

        #25 initReset = 1'b0;
        #100 initReset = 1'b1;
        #10000 reset2 = 1'b0;
	    #10100 fccAlign = 1'b1;
        #10200 reset2 = 1'b1;
        #10400 fccAlign = 1'b0;
`ifdef TEST_SCRAMBLER
        #5000 disSCR = 1'b0;
`else
        #5000 disSCR = 1'b1; 
`endif
        #40000 ocp = 7'h04;
        #40000 ocp = 7'h30;
        #40000 ocp = 7'h06;
        #40000 ocp = 7'h02; 
        #1000000
        $stop;
    end
    always 
        #12.48 clk = ~clk; //25 ns clock period
    
    always
//        #0.390625 clk1280 = ~clk1280;
        #0.390 clk1280 = ~clk1280;

    always
//        #0.78125  clk640 = ~clk640;
        #0.780  clk640 = ~clk640;

    always
//        #1.5625   clk320 = ~clk320;
        #1.56   clk320 = ~clk320;

   always @(posedge clk) 
   begin
        dnReset <= initReset;        
   end
endmodule
