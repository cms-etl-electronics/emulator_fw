`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Tue Oct 19 10:30:06 CDT 2021
// Module Name: ETROC2Emulator_tb
// Project Name: ETROC2 readout
// Description: 
// Dependencies: No
// 
// Revision:
// Revision 0.01 - File Created

// 
//////////////////////////////////////////////////////////////////////////////////
`include "commonDefinition.v"
//`define TEST_SERRIALIZER_1280
//`define TEST_SERRIALIZER_640
//`define TEST_SERRIALIZER_320
`define TEST_SCRAMBLER
module ETROC2Emulator_tb;

    localparam BCSTWIDTH = `L1BUFFER_ADDRWIDTH*2+11+2;

    localparam fc_IDLE 		= 8'b1111_0000; //2'hF0 -->n_fc = 0--3'h001 

	reg clk;
    reg clk1280;
    reg clk640;
    reg clk320;
    reg fccAlign;
    reg [16:0] chipId;

    reg [2:0] cnt_fc;
    reg [7:0] fc_byte;
    always@(posedge clk320) begin
        cnt_fc <= cnt_fc + 1;
        if(cnt_fc==3'd0)
            fc_byte <= fc_IDLE; //sampling parallel 8 bits fc at 320 MHz clock
        else
            fc_byte[7:0] <= {fc_byte[6:0],fc_byte[7]}; //shift 8-bit parallel fc in 40 MHz period, serializing to 1 bit
    end

    wire fc_fc;
    assign fc_fc = fc_byte[7];

    reg initReset;
    reg reset2;
    reg dnReset;
    reg [1:0] rateRight; //serializer speed
    reg [1:0] rateLeft; //serializer speed
    reg [6:0] ocp;
    reg disSCR;
    reg mergeTrigData;
    reg singlePort;

    reg [2:0] triggerGranularity;
    wire soutRight;
    wire soutLeft;
    wire ws_start;
    wire ws_stop;
 //   wire chargeInjection;
    wire [3:0] fc_state_bitAlign;
    wire [3:0] fc_ed;
    // wire readoutClock;

ETROCEmulatorTop #(.L1ADDRWIDTH(7),.BCSTWIDTH(27), .PIXELROW(3)) ETROCEmulatorTopInst
(
	.clk(clk),          
    .clk1280(clk1280),  
    .clk320(clk320),
    .clkPolarity(1'b0),
    .reset(dnReset),       
    .chipId(chipId),
    // .readoutClockDelayPixel(5'd0),
    // .readoutClockWidthPixel(5'd16),
    // .readoutClock(readoutClock),
    .linkResetSlowControl(1'b0),
    .linkResetTestPattern(1'b1),
    .linkResetFixedPattern(32'h3C5C3C5A),                  
    .emptySlotBCID(12'd1177),       
    .triggerGranularity(triggerGranularity),   
    .disScrambler(disSCR),
    .mergeTriggerData(mergeTrigData),
    .singlePort(singlePort),      
//	.fcAlignStart(fccAlign),		
//	.fcBitAlignError(fc_bitError),	
//	.fcBitAlignStatus(fc_ed),			
	.fcData(fc_fc),
//	.fcAlignFinalState(fc_state_bitAlign),
	// .chargeInjection(chargeInjection),
    // .chargeInjectionDelay(5'd24),
    .wsStart(ws_start),
    .wsStop(ws_stop),
	.ocp(ocp),  
    .addrOffset(1'b1),
    .soutRight(soutRight),  //serializer output
    .soutLeft(soutLeft)  //serializer output
);


    wire [4:0] trigDataSizeRight;
    wire [4:0] trigDataSizeLeft;
    wire [4:0] trigDataSize;
    assign trigDataSize =    triggerGranularity == 3'd1 ? 5'd1 : 
                            (triggerGranularity == 3'd2 ? 5'd2 :
                            (triggerGranularity == 3'd3 ? 5'd4 :
                            (triggerGranularity == 3'd4 ? 5'd8 :
                            (triggerGranularity == 3'd5 ? 5'd16 : 5'd0))));
    assign trigDataSizeRight =  singlePort ? trigDataSize : 
                                (mergeTrigData ? trigDataSize >> 1 : 5'd0); 
    assign trigDataSizeLeft =  singlePort ? 5'd0 : 
                                (mergeTrigData ? trigDataSize >> 1 : trigDataSize);      


    wire right_aligned;
    wire [9:0] RC_right_goodEventRate;
    wire [1:0] RC_right_dataType;
    wire [19:0] RC_right_BCIDErrorCount;
    wire [19:0] RC_right_nullEventCount;
    wire [19:0] RC_right_goodEventCount;
    wire [19:0] RC_right_notHitEventCount;
    wire [19:0] RC_right_L1OverlfowEventCount;
    wire [19:0] RC_right_totalHitsCount;
    wire [19:0] RC_right_dataErrorCount;
    wire [19:0] RC_right_missedHitsCount;
    wire [8:0]  RC_right_hittedPixelCount;
    wire [19:0] RC_right_frameErrorCount;
    wire [19:0] RC_right_mismatchBCIDCount;
    wire [19:0] RC_right_L1FullEventCount;
    wire [19:0] RC_right_L1HalfFullEventCount;
    wire [19:0] RC_right_SEUEventCount;
    wire [19:0] RC_right_hitCountMismatchEventCount;
    wire [15:0] RC_right_triggerDataOut;

    fullChainDataCheck fullChainDataCheckInstRight
    (
        .clk320(clk320),
        .clk640(clk640),
        .clk1280(clk1280),
        .dataRate(rateRight),
        .chipId(chipId),
        .triggerDataSize(trigDataSizeRight),
        .reset(dnReset),
        .reset2(reset2),
        .aligned(right_aligned),
        .disSCR(disSCR),
        .serialIn(soutRight),
        .RC_BCIDErrorCount(RC_right_BCIDErrorCount),
        .RC_dataType(RC_right_dataType),
        .RC_nullEventCount(RC_right_nullEventCount),
        .RC_goodEventCount(RC_right_goodEventCount),
        .RC_notHitEventCount(RC_right_notHitEventCount),
        .RC_L1OverlfowEventCount(RC_right_L1OverlfowEventCount),
        .RC_totalHitsCount(RC_right_totalHitsCount),
        .RC_dataErrorCount(RC_right_dataErrorCount),
        .RC_missedHitsCount(RC_right_missedHitsCount),
        .RC_hittedPixelCount(RC_right_hittedPixelCount),
        .RC_frameErrorCount(RC_right_frameErrorCount),
        .RC_L1FullEventCount(RC_right_L1FullEventCount),
        .RC_L1HalfFullEventCount(RC_right_L1HalfFullEventCount),
        .RC_SEUEventCount(RC_right_SEUEventCount),
        .RC_goodEventRate(RC_right_goodEventRate),
        .RC_hitCountMismatchEventCount(RC_right_hitCountMismatchEventCount),
        .RC_mismatchBCIDCount(RC_right_mismatchBCIDCount),
        .RC_triggerDataOut(RC_right_triggerDataOut)
    );

    wire left_aligned;
    wire [9:0] RC_left_goodEventRate;
    wire [1:0] RC_left_dataType;
    wire [19:0] RC_left_BCIDErrorCount;
    wire [19:0] RC_left_nullEventCount;
    wire [19:0] RC_left_goodEventCount;
    wire [19:0] RC_left_notHitEventCount;
    wire [19:0] RC_left_L1OverlfowEventCount;
    wire [19:0] RC_left_totalHitsCount;
    wire [19:0] RC_left_dataErrorCount;
    wire [19:0] RC_left_missedHitsCount;
    wire [8:0]  RC_left_hittedPixelCount;
    wire [19:0] RC_left_frameErrorCount;
    wire [19:0] RC_left_mismatchBCIDCount;
    wire [19:0] RC_left_L1FullEventCount;
    wire [19:0] RC_left_L1HalfFullEventCount;
    wire [19:0] RC_left_SEUEventCount;
    wire [19:0] RC_left_hitCountMismatchEventCount;
    wire [15:0] RC_left_triggerDataOut;

    fullChainDataCheck fullChainDataCheckInstLeft
    (
        .clk320(clk320),
        .clk640(clk640),
        .clk1280(clk1280),
        .dataRate(rateLeft),
        .chipId(chipId),
        .triggerDataSize(trigDataSizeLeft),
        .reset(dnReset),
        .reset2(reset2),
        .aligned(left_aligned),
        .disSCR(disSCR),
        .serialIn(soutLeft),
        .RC_BCIDErrorCount(RC_left_BCIDErrorCount),
        .RC_dataType(RC_left_dataType),
        .RC_nullEventCount(RC_left_nullEventCount),
        .RC_goodEventCount(RC_left_goodEventCount),
        .RC_notHitEventCount(RC_left_notHitEventCount),
        .RC_L1OverlfowEventCount(RC_left_L1OverlfowEventCount),
        .RC_totalHitsCount(RC_left_totalHitsCount),
        .RC_dataErrorCount(RC_left_dataErrorCount),
        .RC_missedHitsCount(RC_left_missedHitsCount),
        .RC_hittedPixelCount(RC_left_hittedPixelCount),
        .RC_frameErrorCount(RC_left_frameErrorCount),
        .RC_L1FullEventCount(RC_left_L1FullEventCount),
        .RC_L1HalfFullEventCount(RC_left_L1HalfFullEventCount),
        .RC_SEUEventCount(RC_left_SEUEventCount),
        .RC_goodEventRate(RC_left_goodEventRate),
        .RC_hitCountMismatchEventCount(RC_left_hitCountMismatchEventCount),
        .RC_mismatchBCIDCount(RC_left_mismatchBCIDCount),
        .RC_triggerDataOut(RC_left_triggerDataOut)
    );


    initial begin
        clk = 0;
        clk1280 = 1'b0;
        clk640 = 0;
        clk320 = 0;
        cnt_fc = 3'd0;
        chipId = 17'H1ABCD;
	    fccAlign = 0;
        initReset = 1;
        reset2 = 1;
        ocp = 7'h01;  //2%
        disSCR = 1'b1;
        mergeTrigData = 1'b1;
        singlePort = 1'b0;
        triggerGranularity = 3'd1;
// `ifdef TEST_SERRIALIZER_1280
//         rateRight = 2'b11; //high speed readout
//         rateLeft = 2'b11; //high speed readout
// `elsif TEST_SERRIALIZER_640
//         rateRight = 2'b01; 
//         rateLeft = 2'b01; 
// `elsif TEST_SERRIALIZER_320
//         rateRight = 2'b00; 
//         rateLeft = 2'b00; 
// `endif

        rateRight = 2'b00; 
        rateLeft  = 2'b00; 

        #25 initReset = 1'b0;
        #100 initReset = 1'b1;
        #10000 reset2 = 1'b0;
	    #10100 fccAlign = 1'b1;
        #10200 reset2 = 1'b1;
        #10400 fccAlign = 1'b0;
`ifdef TEST_SCRAMBLER
        #5000 disSCR = 1'b0;
`else
        #5000 disSCR = 1'b1; 
`endif
        #40000 ocp = 7'h01;
        #40000 ocp = 7'h01;
        #40000 ocp = 7'h01;
        #40000 ocp = 7'h01; 
        #1000000
        $stop;
    end
    always 
        #12.48 clk = ~clk; //25 ns clock period
    
    always
//        #0.390625 clk1280 = ~clk1280;
        #0.390 clk1280 = ~clk1280;

    always
//        #0.78125  clk640 = ~clk640;
        #0.780  clk640 = ~clk640;

    always
//        #1.5625   clk320 = ~clk320;
        #1.56   clk320 = ~clk320;

   always @(posedge clk) 
   begin
        dnReset <= initReset;        
   end
endmodule
