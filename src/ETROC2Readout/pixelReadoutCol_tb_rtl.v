
//all verilog files needed for pixelReadoutCol_tb.v
`include "pixelReadoutCol_tb.v"
`include "pixelReadoutWithSWCell_rtl.v"
`include "BCIDBuffer.v"
`include "BCIDCounter.v"
`include "memBlock/BCID_mem_rtl.v"
`include "CircularBufferAddr.v"
`include "FIFOWRCtrlerDDR.v"
`include "L1BufferAddr.v"
`include "PRBS31.v"
`include "TDCTestPatternGen.v"
`include "TestL1Generator.v"
`include "memBlock/cb_data_mem_decoder.v"
`include "commonDefinition.v"
`include "globalReadoutForPixelTest.v"
`include "globalReadoutController.v"
`include "pixelReadoutCol.v"


