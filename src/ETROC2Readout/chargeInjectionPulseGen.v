`timescale 1ns / 10ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Wed Nov  3 23:03:57 CDT 2021
// Module Name: chargeInjectionPulseGen
// Project Name: ETROC2 readout
// Description: 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created

// 
//////////////////////////////////////////////////////////////////////////////////
module chargeInjectionPulseGen 
(
	input           clk40,              //40MHz
    input           clk1280,
    input           reset,
    input           chargeInjectionCmd,
    input [4:0]     delay,              //phase from clk40 rising edge to rising edge
	output          pulse               //
);
// tmrg default triplicate

    reg chargeInjectionCmdDelay;
    reg [1:0] sessionCount;
    wire [1:0] nextsessionCount = sessionCount + 1;
    wire [1:0] nextsessionCountVoted = nextsessionCount;
    reg endSession;
    wire startSession = ~chargeInjectionCmdDelay&chargeInjectionCmd; //only start at rising edge
    reg startReg;
    always @(posedge clk40) 
    begin
        startReg <= startSession;
        chargeInjectionCmdDelay <= chargeInjectionCmd;
        if(~reset)
        begin
            endSession <= 1'b1;
            sessionCount <= 2'd0;
        end
        else if(startSession)
        begin
            endSession <= 1'b0;
            sessionCount <= 2'd0;
        end
        else if (~endSession) begin
            sessionCount <= nextsessionCountVoted;
            if(sessionCount == 2'd3)
            begin
                endSession <= 1'b1;
                sessionCount <= 2'd0;
            end
        end
    end

    reg syncClk40;
    always @(negedge clk1280) 
    begin
        if(~endSession)begin
            syncClk40 <= clk40;
        end
    end

    reg [6:0] phaseCounter;
    reg clk40D1;                     //delay clk40
    reg clk40D2;                     //delay clk40
    wire risingClk40;
    assign risingClk40 = ~clk40D2 & clk40D1;
    wire [6:0] nextCount = phaseCounter + 1;
    wire [6:0] nextCountVoted = nextCount;

    always @(posedge clk1280) 
    begin
        if(~endSession)begin
            clk40D1 <= syncClk40;
            clk40D2 <= clk40D1;
            if(risingClk40 & startReg)
            begin
                phaseCounter <= 7'd3;   //align phasECounter == 0 to rising edge of clk40
            end
            else 
            begin
                phaseCounter <= nextCountVoted;           
            end
        end
    end

    reg pulseReg;
    wire [6:0] risingAt     = {2'b01 ,delay};
    wire [6:0] fallingAt    = risingAt + 7'd64;  //last 50 ns
    always @(posedge clk1280) 
    begin
        if(~endSession)begin
            if(phaseCounter >= risingAt && phaseCounter < fallingAt)
            begin
                pulseReg <= 1'b1;              
            end
            else
            begin
                pulseReg <= 1'b0;              
            end
        end
        else
        begin
                pulseReg <= 1'b0;                          
        end
    end

    assign pulse = pulseReg;

endmodule
