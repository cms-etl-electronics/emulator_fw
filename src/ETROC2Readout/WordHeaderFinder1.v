`timescale 10ps / 1ps
//////////////////////////////////////////////////////////////////////////////////
// 
// Create Date: 2022-04-18
// Module Name: WordHeaderFinder1
// Project Name: ETROC2 readout
// Description: 
// Dependencies: 
// 


//////////////////////////////////////////////////////////////////////////////////


module WordHeaderFinder1   (
    input                   bitCK,
	 input clk40RB,//2022
    input [7:0]       RawWord,
	output  BadSync6sec,    // The Sync become bad for a few seconds 
	output  Trig1b,    // Tigger bit 
	output  DV3p,    // data valid of the output data 
	output [39:0]  dout         //aligned word
);

		assign wordCK=clk40RB;//2022

	reg [2:0]  BitSkipIndex;
	reg [5:0] BitCnt1x;


	//create a pulse tkP2Q with the 40 MHz clock edge
	// 
	// CK40 tick:   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
	// RawWord:    X ------------- X ------------- X
	// Flip1Q:     /~~~~~~~~~~~~~~~\_______________/
	// tkP2Q:       0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0
	// ClkCnt3b:    6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
    reg Flip1Q;
    reg Flip1QQ;
    reg tkP2Q;
    always @(posedge wordCK) 
    begin        
        Flip1Q <= !Flip1Q;       
    end
	 
    always @(posedge bitCK) 
    begin
        Flip1QQ <= Flip1Q;
        tkP2Q <= (!Flip1Q) & Flip1QQ;
    end

	//Create a counter to count the bit clock (0-7)
	reg [2:0] ClkCnt3b;
	always @(posedge bitCK)
	begin
		if (tkP2Q == 1'b1)
		begin
			ClkCnt3b<=3'b000;
		end
		else
		begin
			ClkCnt3b <= ClkCnt3b + 1;
		end
	end

	// Turn the RawWord back into bit stream 
	// Use RawShiftReg8x[0] to feed later stage
	reg [7:0] RawShiftReg8x;
	always @(posedge bitCK)
	begin
		if (ClkCnt3b == 3'b111)
		begin
			RawShiftReg8x<=RawWord;
		end
		else
		begin
			RawShiftReg8x<={1'b0, RawShiftReg8x[7:1]};
		end
	end
	 
	// If ClkCnt3b == BitSkipIndex, then skip the trigger bit at next clock cycle.
	// SkipBitP = 1 when ClkCnt3b == BitSkipIndex+1
	reg SkipBitP;
	always @(posedge bitCK)
	begin
		if (ClkCnt3b == BitSkipIndex)
		begin
			SkipBitP <= 1'b1;
		end
		else
		begin
			SkipBitP <= 1'b0;
		end
	end

	// If SkipBitP == 1, then skip the trigger bit.
	// Otherwise, the RawShiftReg8x[0] is fed into the ChkWord41b, insert LSB first
	reg Trig1x;
	reg [160:0] ChkWord41b;
	always @(posedge bitCK)
	begin
		if (SkipBitP == 1'b0)
		begin
			ChkWord41b<={RawShiftReg8x[0], ChkWord41b[160:1]};
			Trig1x <= Trig1x;
		end
		else
		begin
			ChkWord41b<=ChkWord41b;
			Trig1x <= RawShiftReg8x[0];
		end
	end

	// Generate a pulse when the higher 16 bits of ChkWord41b is 0x3C5C
	// The pulse width is single clock cycle
	// (Since we skip trigger bit, the 3C5C coundition could be two cycles long)
	// When p3C5Cx1Q = 1, ChkWord41b[39:24] = 3C5C, since the bit pattern shifted by 1
	reg p3C5Cx1Q;
	always @(posedge bitCK)
	begin
		if (SkipBitP == 1'b0)
		begin
			if ((ChkWord41b[160:145] == 16'h3C5C)&&(ChkWord41b[120:105] == 16'h3C5C)&&(ChkWord41b[80:65] == 16'h3C5C)&&(ChkWord41b[40:25] == 16'h3C5C))
			//if ((ChkWord41b[40:25] == 16'h3C5C))
			begin
				p3C5Cx1Q <= 1'b1;
			end
			else
			begin
				p3C5Cx1Q <= 1'b0;
			end
		end
		else
		begin
			p3C5Cx1Q <= p3C5Cx1Q;
		end
	end
	 
	reg [5:0] BitCntTemp;
	always @(posedge bitCK)
	begin
		if (SkipBitP == 1'b0)
		begin
			if ((p3C5Cx1Q == 1'b1)||(BitCntTemp==6'd39))
			begin
				BitCntTemp<=6'd0;
			end
			else
			begin
				BitCntTemp<=BitCntTemp+6'd1;
			end
		end
		else
		begin
			BitCntTemp<=BitCntTemp;
		end

	end


	// Bring ChkWord41b to OutWord40b
	reg [39:0] OutWord40b;
	always @(posedge bitCK)
	begin
		if ((BitCnt1x == 6'd39)&&(SkipBitP == 1'b0))
		begin
			OutWord40b<=ChkWord41b[39:0];
		end
		else
		begin
			OutWord40b<=OutWord40b;
		end
	end



	// Good filler (or header) count
	// When a 3C5C is seen but mismatches the temp bit count, (p3C5Cx1Q == 1'b1)&&(BitCntTemp!=6'd39) clear the counter
	// Also when (NoFillerCnt[11]== 1'b1), there are 2048 40-bit words without good filler, clear the counter
	// When a 3C5C is seen and matches the temp bit count, (p3C5Cx1Q == 1'b1)&&(BitCntTemp==6'd39) it is a good filler or header
	// The GoodFillerCnt is capped if the top bit is 1.
	// 
	reg [11:0] NoFillerCnt;
	reg [3:0] GoodFillerCnt;
	always @(posedge bitCK)
	begin
		if (SkipBitP == 1'b0)
		begin
			if (((p3C5Cx1Q == 1'b1)&&(BitCntTemp!=6'd39))||(NoFillerCnt[11]== 1'b1))
			begin
				GoodFillerCnt <= 4'b0000;
			end
			else if((p3C5Cx1Q == 1'b1)&&(BitCntTemp==6'd39)&&(GoodFillerCnt[3]!= 1'b1))
			begin
				GoodFillerCnt <= GoodFillerCnt + 4'b0001;
			end
			else 
			begin
				GoodFillerCnt<=GoodFillerCnt;
			end
		end
		else 
		begin
			GoodFillerCnt<=GoodFillerCnt;
		end
	end

	// No filler (or header) count
	// When a 3C5C is seen and matches the temp bit count, (p3C5Cx1Q == 1'b1)&&(BitCntTemp==6'd39) it is a good filler or header, clear the counter
	// Also when (NoFillerCnt[11]== 1'b1), there are 2048 40-bit words without good filler, clear the counter
	// This will let NoFillerCnt[11] becomes 1 for one cycle.
	// (BitCnt1x == 6'd39) serves as a timing tick to increase the counter.
	//
	//reg [11:0] NoFillerCnt;
	always @(posedge bitCK)
	begin
		if (SkipBitP == 1'b0)
		begin
			if (((p3C5Cx1Q == 1'b1)&&(BitCntTemp==6'd39))||(NoFillerCnt[11]== 1'b1))
			begin
				NoFillerCnt <= 12'h0000;
			end
			else if((BitCnt1x == 6'd39))
			begin
				NoFillerCnt <= NoFillerCnt + 12'h0001;
			end
			else 
			begin
				NoFillerCnt<=NoFillerCnt;
			end
		end
		else 
		begin
			NoFillerCnt<=NoFillerCnt;
		end
	end

	// If found more than 8 good fillers, sync the BitCnt1x 
	//
	//reg [5:0] BitCnt1x;
	always @(posedge bitCK)
	begin
		if (SkipBitP == 1'b0)
		begin
			if (((p3C5Cx1Q == 1'b1)&&(BitCntTemp==6'd39)&&(GoodFillerCnt[3]== 1'b1))||(BitCnt1x==6'd39))
			begin
				BitCnt1x <= 6'b000000;
			end
			else 
			begin
				BitCnt1x <= BitCnt1x+6'b000001;
			end
		end
		else 
		begin
			BitCnt1x <= BitCnt1x;
		end
	end


	// step forward the BitSkipIndex
	// (NoFillerCnt[11]== 1'b1) means there are 2048 words without good filler
	always @(posedge bitCK)
	begin
		if (SkipBitP == 1'b0)
		begin
			if (NoFillerCnt[11]== 1'b1)
			begin
				BitSkipIndex<=BitSkipIndex - 3'b001;
			end
			else 
			begin
				BitSkipIndex<=BitSkipIndex;
			end
		end
		else 
		begin
			BitSkipIndex <= BitSkipIndex;
		end
	end

	// Data Valid signal
	// It can be used to push data into FIFO in the later stage
	// The data at OutWord40b1Q is for sure stable during BitCnt1x = 0-15
	reg DV1q;
	always @(posedge wordCK) 
	begin        
		if ((BitCnt1x[5:4] == 2'b00))
		begin
			DV1q <= 1'b1;
		end
		else
		begin
			DV1q <= 1'b0;
		end
	end
	
	// Generate a pulse with single cycle width
	reg DV1qq;
	reg DV2q;
	reg DV3q;
	always @(posedge wordCK) 
	begin        
		DV1qq <= DV1q;
		DV2q <= (!DV1qq) & DV1q;
		DV3q <= DV2q;
	end

	reg Trig1y;
	reg [39:0] OutWord40b1Q;
	always @(posedge wordCK) 
	begin        
		OutWord40b1Q <= OutWord40b;
		Trig1y <= Trig1x;
	end

	// When the NoFillerCnt[10]==1, the Sync is almost lost,
	// In this case, turn on BadSync6sec for 250M/40MHz = 6.4 sec
	reg [28:0] BadSyncCnt1x;
	always @(posedge wordCK)
	begin
		if (NoFillerCnt[10]== 1'b1)
			begin
				BadSyncCnt1x <= 29'h10000000;
			end
			else if((BadSyncCnt1x[28] == 1'b1))
			begin
				BadSyncCnt1x <= BadSyncCnt1x + 29'h00000001;
			end
			else 
			begin
				BadSyncCnt1x <= BadSyncCnt1x;
			end
	end


    assign BadSync6sec = BadSyncCnt1x[28];

    assign Trig1b = Trig1y;
	 
    assign DV3p = DV3q;
	
    assign dout = OutWord40b1Q;

endmodule