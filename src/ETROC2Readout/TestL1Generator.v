`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Sat Jan 23 12:36:50 CST 2021
// Module Name: TestL1Generator
// Project Name: ETROC2 readout
// Description: 
// Dependencies: No
// 
// Revision:
// Revision 0.01 - File Created

// 
//////////////////////////////////////////////////////////////////////////////////
`include "commonDefinition.v"
// `ifndef TESTL1GENERATOR
// `define TESTL1GENERATOR
module TestL1Generator #(parameter WORDWIDTH = 15, FORWARDSTEPS = 0)
(
	input clk,            //40MHz
    input dis,
    input reset,      
    input mode,     //0 is periodic trigger, 1 is random trigger
    output L1A    //emulate L1A
);
// tmrg default triplicate

    wire [WORDWIDTH-1:0] prbs;
    wire disPRBS;
    wire disCount;
    assign disPRBS = dis | ~mode;
    assign disCount = dis | mode;
// `ifdef SIMULATION_RANDOM
//     reg [WORDWIDTH-1:0] prbsReg;
//     always@(posedge clk) begin
//         prbsReg <= $urandom%(16'H8000);
//     end
//     assign prbs = prbsReg;
// `else
`ifndef NOPIXEL_MEM
  PRBS31 #(.WORDWIDTH(WORDWIDTH),
    .FORWARDSTEPS(FORWARDSTEPS))
     prbs_inst(
		.clk(clk),
		.reset(reset),
		.dis(disPRBS),
        //.seed(15'h5AAA),
        .seed(31'h2AAAAAAA),
		.prbs(prbs)
	);
`else
  PRBS31 #(.WORDWIDTH(WORDWIDTH))
     prbs_inst(
		.clk(clk),
		.reset(reset),
		.dis(disPRBS),
        //.seed(15'h5AAA),
        .seed(31'h2AAAAAAA),
		.prbs(prbs)
	);
`endif

// `endif

    reg startTMR;
    reg [9:0] countTMR;
    wire [9:0] nextCount = countTMR + 10'd1;
    wire [9:0] nextCountVoted = nextCount;
    always @(posedge clk) 
    begin
        if(!reset)
        begin
            countTMR <= 10'h000;
            startTMR <= 1'b0;
        end
        else
        begin
            if(startTMR == 1'b0)
            begin
                countTMR <= nextCountVoted;
                if(countTMR == 10'd512)
                begin
                    startTMR <= 1'b1;
                end     
            end
        end
    end

    //fixed periodic is 41
    reg [7:0] triggerCountTMR;
    wire [7:0] nextTriggerCount = triggerCountTMR + 8'd1;
    wire [7:0] nextTriggerCountVoted = nextTriggerCount;
    always @(posedge clk) 
    if(!reset)
    begin
        triggerCountTMR <= 8'h00;
    end
    else if(!disCount)
    begin
        if(triggerCountTMR == 8'd101)begin
            triggerCountTMR <= 8'h00;
        end
        else begin
            triggerCountTMR <= nextTriggerCountVoted;            
        end
    end
    //do not read garbage in circular buffer in the first 512 clock periods.
    assign L1A = !startTMR ? 1'b0 : (mode ? (prbs < `L1A_THRESHOLD) : triggerCountTMR == 8'd101); //periodic about 1/102 2022-03-24

endmodule
// `endif 