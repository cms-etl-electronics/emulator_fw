`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Thu Feb 11 12:58:07 CST 2021
// Module Name: serializer
// Project Name: ETROC2 readout
// Description: 
// Dependencies: 
// 
// LSB first serializer


//////////////////////////////////////////////////////////////////////////////////


module serializer(
    input           link_reset,
    input           dis,
    input           testPatternSel,     //0: PRBS7, 1: fixed pattern specified by user
    input [31:0]    fixedTestPattern,   //user specified test pattern
    input           clk1280,            //1280 MHz
	 input            clk40,
    input           clk40syn,           //40M clock for sync
    input [1:0]     rate,               //00: 320 Mbps, 01: 640Mbps, 10/11: 1280 Mbps
	input [31:0]    din,                //input data
	output      sout                //output serial data
);
// tmrg default triplicate
// tmrg do_not_triplicate sout

    localparam rdClkPhase = 5'b10000;

   // wire clk40;
    wire clk320;
    wire clk640;
    wire load40; //load signal for first stage serializers
    wire load640; //load signal for second serializers
    wire load1280; //load signal for last stage serializer
    wire reset;         //periodic reset
    reg [4:0] counter;
    reg clk40syncLater;
    reg clk40syncLater1D;
    assign reset = ~(~clk40syncLater & clk40syncLater1D);  //low active 
    wire [4:0] nextCounter = counter + 1;
    wire [4:0] nextCounterVoted = nextCounter;
	
	 // 2022 
          reg clk40_8_1=0;
          reg [2:0]clk40cnt=0;
           always @(posedge clk1280)
           begin
              clk40cnt<=clk40cnt+1;
              if (clk40cnt==0) clk40_8_1=1;
              else clk40_8_1=0;
           end
       /////////    
	
    always @(posedge clk1280) 
    begin
        if(~dis)
        begin
            clk40syncLater <=clk40syn;
            clk40syncLater1D <= clk40syncLater;
        end
        if(~reset | dis)
        begin
            counter <= rdClkPhase;
        end
        else
        begin
            counter <= nextCounterVoted;
        end
    end
    assign clk640 = counter[0];
    //assign clk320 = counter[1];
    assign clk320 = clk1280;// infact clk1280= 320MHz 202201
    //assign clk40  = counter[4];
    ////assign clk40 = counter[2];
	 
    //assign load40 = ~counter[4] & ~counter[3] & ~counter[2]; 
    assign load40= clk40_8_1;//2022 ~counter[2] & ~counter[1] & ~counter[0];
    assign load640  = ~clk320;
    assign load1280 = ~clk640; 

//    assign rdClk = clk40;       //read clock 

    wire [3:0] enSer320;
    wire [1:0] enSer640;
    wire enSer1280;
    assign enSer320     = (rate == 2'b00) ? 4'h1  : ((rate == 2'b01) ? 4'h3  : 4'hF);
    assign enSer640     = (rate == 2'b00) ? 2'b00 : ((rate == 2'b01) ? 2'b01 : 2'b11);
    assign enSer1280    = (rate == 1'b0)  ? 1'b0  : ((rate == 1'b01) ? 1'b0  : 1'b1);

    wire disPRBS8;
    wire disPRBS16;
    wire disPRBS32;
    assign disPRBS8     = ~link_reset | testPatternSel | (rate != 2'b00);
    assign disPRBS16    = ~link_reset | testPatternSel | (rate != 2'b01);
    assign disPRBS32    = ~link_reset | testPatternSel | (rate[1] != 1'b1);

    wire [7:0] PRBSData8;
    wire [15:0] PRBSData16;
    wire [31:0] PRBSData32;

    wire resetPRBS;
    reg link_reset1D;
    always @(posedge clk40) 
    begin
        link_reset1D <= link_reset;        
    end
    assign resetPRBS = ~(link_reset & ~link_reset1D); //reset PRBS for the first link_reset

    PRBS7 #(.WORDWIDTH(8)) prbs7_8 (
        .clk(clk40),
        .reset(resetPRBS),
        .dis(disPRBS8),
        .seed(7'H3F),
        .prbs(PRBSData8)
    );

    PRBS7 #(.WORDWIDTH(16)) prbs7_16 (
        .clk(clk40),
        .reset(resetPRBS),
        .dis(disPRBS16),
        .seed(7'H3F),
        .prbs(PRBSData16)
    );

    PRBS7 #(.WORDWIDTH(32)) prbs7_32 (
        .clk(clk40),
        .reset(resetPRBS),
        .dis(disPRBS32),
        .seed(7'H3F),
        .prbs(PRBSData32)
    );

    wire [31:0] PRBSDataCombine;
    assign PRBSDataCombine = (rate == 2'b00) ? {24'd0,PRBSData8} : 
                            ((rate == 2'b01) ? {16'd0,PRBSData16} : PRBSData32);
    wire [31:0] testPattern;
    assign testPattern = testPatternSel ? fixedTestPattern : PRBSDataCombine;
    wire [31:0] source = link_reset ? testPattern : din;
    wire [3:0] s320;
    wire [1:0] s640;
    wire s1280;
    wire data40[31:0];
    genvar n;
    generate
        for(n = 0 ; n < 8; n = n+1)
        begin
            assign data40 [n]      = (rate == 2'b00) ? source[n]    : (rate == 2'b01 ? source[n*2]      : source[n*4]);
            assign data40 [n+8]    = (rate == 2'b00) ? 1'b0         : (rate == 2'b01 ? source[n*2+1]    : source[n*4+2]);
            assign data40 [n+16]   = (rate == 2'b00) ? 1'b0         : (rate == 2'b01 ? 1'b0             : source[n*4+1]);
            assign data40 [n+24]   = (rate == 2'b00) ? 1'b0         : (rate == 2'b01 ? 1'b0             : source[n*4+3]);
        end
    endgenerate
//2022
//    genvar i;
//    wire [7:0] data8In [3:0];
//    generate
//        //for(i = 0 ; i < 4; i = i+1)
//		   for (i=0;i<1;i=i+1)// 20220128 for only 320Mbps , not others rate 
//        begin:loop_s1
//            assign data8In[i] = {data40[7+8*i],data40[6+8*i],data40[5+8*i],data40[4+8*i],
//                      data40[3+8*i],data40[2+8*i],data40[1+8*i],data40[8*i]};
//            serializerBlock #(.WORDWIDTH(8))s320Inst (
//                .enable(enSer320[i]),
//                .load(load40),
//                .bitCK(clk320),
//                .din(data8In[i]),
//                .sout(s320[i])
//            );
//        end      
//    endgenerate  2022
// 2022 	 
	wire [7:0] din8;
assign din8={data40[7],data40[6],data40[5],data40[4],data40[3],data40[2],data40[1],data40[0]};	
	 serializerBlock #(.WORDWIDTH(8))s320Inst (
                .enable(enSer320[0]),
                .load(load40),
                .bitCK(clk320),
                .din(din8),//.din(data8In[i]),
                .sout(sout)//(s320[0]) 2022 datao confirmed
            );
 // 2022  end  
 
//    genvar j;
//    wire [1:0] data2In [1:0];
//    generate
//        for(j = 0 ; j < 2; j = j+1)
//        begin:loop_s2
//        assign data2In[j] = {s320[2*j+1],s320[2*j]};
//        serializerBlock #(.WORDWIDTH(2))s640Inst(
//            .enable(enSer640[j]),
//            .load(load640),
//            .bitCK(clk640),
//            .din(data2In[j]),
//            .sout(s640[j])
//        );
//        end
//    endgenerate
//
//  //  wire inv_clk;
//  //  assign inv_clk = !clk;
// //   wire usedClk;
//  //  assign usedClk = clkPolarity ? inv_clk:clk;
//    serializerBlock #(.WORDWIDTH(2))s128Inst(
//        .enable(enSer1280),
//        .load(load1280),
//        .bitCK(clk1280),
//        .din(s640),
//        .sout(s1280)
//    );
//
    wire soutVoted = dis? 1'b0 : ((rate[1] == 1'b1) ? s1280 : (rate == 2'b01 ? s640[0] : s320[0]));

    //always @(negedge clk1280) // 20220128
//	 always @(posedge clk1280)
//    begin
//       // sout <= soutVoted;
//       sout<=s320[0]; // 202201
//    end
//assign sout=s320[0];

endmodule