`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Southern Methodist University
// Author: Datao Gong 
// 
// Create Date: Wed Feb  3 10:14:24 CST 2021
// Module Name: fullChainDataCheck
// Project Name: ETROC2 readout
// Description: 
// Dependencies: PRBS9
// 
// Revision:
// Revision 0.01 - File Created

// 
//////////////////////////////////////////////////////////////////////////////////
`include "commonDefinition.v"

module fullChainDataCheck
(
	input  			    clk320,
	input  			    clk640,
	input  			    clk1280,
	input  clk8RB,//2022
	input  clk40RB,//2022
    input [16:0]        chipId,
    input [1:0]         dataRate,
    input [4:0]         triggerDataSize,
	input  			    reset,    
	input  			    reset2, 
    input               disSCR,             
    input               serialIn,
	 
 	output  BadSync6sec,    // The Sync become bad for a few seconds 
 	output  Trig1b,    // Tigger bit 
	output  DV3p,    // data valid of the output data 
	output  DataChk1b,    // 1 bit of the output data for checking on scope 

    output              aligned,
    output  [19:0]   RC_BCIDErrorCount,
    output  [1:0]    RC_dataType,
    output  [19:0]   RC_nullEventCount, //idle count
    output  [19:0]   RC_goodEventCount, 
    output  [19:0]   RC_notHitEventCount, 
    output  [19:0]   RC_L1OverlfowEventCount,
    output  [19:0]   RC_L1FullEventCount,
    output  [19:0]   RC_L1HalfFullEventCount,
    output  [19:0]   RC_SEUEventCount,
    output  [19:0]   RC_hitCountMismatchEventCount,
    output [19:0]       RC_totalHitsCount,
    output [19:0]       RC_dataErrorCount,
    output [19:0]       RC_missedHitsCount,
    output [8:0]        RC_hittedPixelCount,
    output [19:0]       RC_frameErrorCount,
    output [9:0]        RC_goodEventRate, //CRCError for every 64 frame
    output [19:0]       RC_mismatchBCIDCount,
    output [15:0]       RC_triggerDataOut
	 
	//2022  input [39:0] dataframe_debug_right
);
// tmrg default do_not_triplicate

    wire wordCK;
    wire [39:0] word40b;
    wire deserClk;

    assign deserClk = (dataRate == 2'b00) ? clk320 : 
                      ((dataRate == 2'b01) ? clk640 : clk1280);

    wire dlSout;
    delayLine dlInst(
        .bitClk(deserClk),
        .delay(6'd1),
        .sin(serialIn),
        .sout(dlSout)
    );
	 
	 

    wire [4:0] wordAddr;
    wire wordTrigClk;
    //wire Trig1b;
    //wire DV3p;
    DeserializerWithTriggerData2 dser40T(
        .bitCK(deserClk),
		  .clk8(clk8RB),//2022
		  .clk40RB(clk40RB),//2022
        .reset(reset),
        .disSCR(disSCR),
        .rate(dataRate),
        .delay(wordAddr),
      // .delay(6'h02), 
        .sin(dlSout),
        .trigData(RC_triggerDataOut),
        .trigDataSize(triggerDataSize),
        .wordTrigClk(wordTrigClk),
        .word40CK(wordCK),
        .BadSync6sec(BadSync6sec),
        .Trig1b(Trig1b),
        .DV3p(DV3p),
        .dout(word40b)
    );

	 assign DataChk1b = word40b[2];


    wire [39:0] receivedDataFrame;
	   //2022  wire [39:0] receivedDataFrame_debug_right;
    //2022assign receivedDataFrame_debug_right=dataframe_debug_right;
    dataExtract dataExtractInst
    (
        .clk(clk8RB),//2022(wordCK),
        .reset(reset2),
        .chipId(chipId),
        .wordAddr(wordAddr),
        .din(word40b),
        .aligned(aligned),
        .goodEventRate(RC_goodEventRate),
        .dout(receivedDataFrame)
    );

    wire noError;
    dataStreamCheck dsk
    (
        .clk(clk8RB),//2022(wordCK),
        .reset(reset2),
        .chipId(chipId),
        .din(receivedDataFrame),//2022(receivedDataFrame_debug_right),
        .noError(noError)
    );

    dataRecordCheck dataRecordCheckReceiver
    (
        .clk(clk8RB),//2022(wordCK),
        .reset(reset2),
        .dataRecord(receivedDataFrame),//2022 (receivedDataFrame_debug_right),
        .BCIDErrorCount(RC_BCIDErrorCount),
        .dataType(RC_dataType),
        .nullEventCount(RC_nullEventCount),
        .goodEventCount(RC_goodEventCount),
        .notHitEventCount(RC_notHitEventCount),
        .L1OverlfowEventCount(RC_L1OverlfowEventCount),
        .totalHitsCount(RC_totalHitsCount),
        .dataErrorCount(RC_dataErrorCount),
        .missedHitsCount(RC_missedHitsCount),
        .hittedPixelCount(RC_hittedPixelCount),
        .frameErrorCount(RC_frameErrorCount),
        .L1FullEventCount(RC_L1FullEventCount),
        .L1HalfFullEventCount(RC_L1HalfFullEventCount),
        .SEUEventCount(RC_SEUEventCount),
        .goodEventRate(RC_goodEventRate),
        .hitCountMismatchEventCount(RC_hitCountMismatchEventCount),
        .mismatchBCIDCount(RC_mismatchBCIDCount)
    );


endmodule
