/****************************************************************************************************
 *                          ! THIS FILE WAS AUTO-GENERATED BY TMRG TOOL !                           *
 *                                   ! DO NOT EDIT IT MANUALLY !                                    *
 *                                                                                                  *
 * file    : ./globalTMR/globalReadoutTMR.v                                                         *
 *                                                                                                  *
 * user    : dataogong                                                                              *
 * host    : 16-11005.local                                                                         *
 * date    : 12/12/2021 11:45:26                                                                    *
 *                                                                                                  *
 * workdir : /Users/dataogong/Documents/Gitlab/ETROCI2C/etroc2_swreadout/rtl/ETROC2Readout          *
 * cmd     : /Users/dataogong/Documents/ActiveDocument/Gitlab/tmrg/bin/tmrg -c tmrgGlobal.cnf       *
 * tmrg rev: e2867c77ccb74667bff8dd6a91891cdefaaa0580                                               *
 *                                                                                                  *
 * src file: globalReadout.v                                                                        *
 *           File is NOT under version control!                                                     *
 *           Modification time : 2021-12-10 18:30:57.269095                                         *
 *           File Size         : 12198                                                              *
 *           MD5 hash          : 89440946f00e1f20e3c3c2f69851e9ee                                   *
 *                                                                                                  *
 ****************************************************************************************************/

`timescale  1ns / 10ps
`include  "commonDefinition.v"
module globalReadoutTMR #(
  parameter L1ADDRWIDTH=7,
  parameter BCSTWIDTH=27
)(
     input  clkA,
     input  clkB,
     input  clkC,
     input  clk1280A,
     input  clk1280B,
     input  clk1280C,
     input [1:0] serRateA,
     input [1:0] serRateB,
     input [1:0] serRateC,
     input [16:0] chipIdA,
     input [16:0] chipIdB,
     input [16:0] chipIdC,
     input  disA,
     input  disB,
     input  disC,
     input  link_reset_fastCommandA,
     input  link_reset_fastCommandB,
     input  link_reset_fastCommandC,
     input  link_reset_slowControlA,
     input  link_reset_slowControlB,
     input  link_reset_slowControlC,
     input  link_reset_testPatternSelA,
     input  link_reset_testPatternSelB,
     input  link_reset_testPatternSelC,
     input [31:0] link_reset_fixedTestPatternA,
     input [31:0] link_reset_fixedTestPatternB,
     input [31:0] link_reset_fixedTestPatternC,
     input  L1A_RstA,
     input  L1A_RstB,
     input  L1A_RstC,
     input [15:0] trigHitsA,
     input [15:0] trigHitsB,
     input [15:0] trigHitsC,
     input [4:0] trigDataSizeA,
     input [4:0] trigDataSizeB,
     input [4:0] trigDataSizeC,
     input [11:0] emptySlotBCIDA,
     input [11:0] emptySlotBCIDB,
     input [11:0] emptySlotBCIDC,
     input  resetA,
     input  resetB,
     input  resetC,
     input [1:0] onChipL1AConfA,
     input [1:0] onChipL1AConfB,
     input [1:0] onChipL1AConfC,
     input  disSCRA,
     input  disSCRB,
     input  disSCRC,
     input [11:0] BCIDoffsetA,
     input [11:0] BCIDoffsetB,
     input [11:0] BCIDoffsetC,
     input  BCIDRstA,
     input  BCIDRstB,
     input  BCIDRstC,
     input  inL1AA,
     input  inL1AB,
     input  inL1AC,
     input [45:0] dnDataA,
     input [45:0] dnDataB,
     input [45:0] dnDataC,
     input  dnUnreadHitA,
     input  dnUnreadHitB,
     input  dnUnreadHitC,
     output  dnReadA,
     output  dnReadB,
     output  dnReadC,
     output [BCSTWIDTH-1:0] dnBCSTA,
     output [BCSTWIDTH-1:0] dnBCSTB,
     output [BCSTWIDTH-1:0] dnBCSTC,
     output  sout
);
wor nextL1CountTmrErrorC;
wire [7:0] nextL1CountVotedC;
wor nextL1CountTmrErrorB;
wire [7:0] nextL1CountVotedB;
wor nextL1CountTmrErrorA;
wire [7:0] nextL1CountVotedA;
wire trigScrOnA;
wire trigScrOnB;
wire trigScrOnC;
assign trigScrOnA =  ~disSCRA;
assign trigScrOnB =  ~disSCRB;
assign trigScrOnC =  ~disSCRC;
wire emulatorL1AA;
wire emulatorL1AB;
wire emulatorL1AC;
wire dnL1AA;
wire dnL1AB;
wire dnL1AC;
wire dnResetA;
wire dnResetB;
wire dnResetC;
wire onChipL1AEnA =  onChipL1AConfA[1] ;
wire onChipL1AEnB =  onChipL1AConfB[1] ;
wire onChipL1AEnC =  onChipL1AConfC[1] ;
wire disL1GenA =  ~onChipL1AEnA|disA;
wire disL1GenB =  ~onChipL1AEnB|disB;
wire disL1GenC =  ~onChipL1AEnC|disC;
wire L1ModeA =  onChipL1AConfA[0] ;
wire L1ModeB =  onChipL1AConfB[0] ;
wire L1ModeC =  onChipL1AConfC[0] ;

TestL1GeneratorTMR TestL1GeneratorInst (
          .clkA(clkA),
          .clkB(clkB),
          .clkC(clkC),
          .disA(disL1GenA),
          .disB(disL1GenB),
          .disC(disL1GenC),
          .modeA(L1ModeA),
          .modeB(L1ModeB),
          .modeC(L1ModeC),
          .resetA(dnResetA),
          .resetB(dnResetB),
          .resetC(dnResetC),
          .L1AA(emulatorL1AA),
          .L1AB(emulatorL1AB),
          .L1AC(emulatorL1AC)
          );
wire actualL1AA;
wire actualL1AB;
wire actualL1AC;
assign actualL1AA =  ~disA&(onChipL1AEnA ? emulatorL1AA : inL1AA);
assign actualL1AB =  ~disB&(onChipL1AEnB ? emulatorL1AB : inL1AB);
assign actualL1AC =  ~disC&(onChipL1AEnC ? emulatorL1AC : inL1AC);
wire [11:0] genBCIDA;
wire [11:0] genBCIDB;
wire [11:0] genBCIDC;
wire [11:0] actualOffsetA;
wire [11:0] actualOffsetB;
wire [11:0] actualOffsetC;
assign actualOffsetA =  onChipL1AEnA ? 12'H000 : BCIDoffsetA;
assign actualOffsetB =  onChipL1AEnB ? 12'H000 : BCIDoffsetB;
assign actualOffsetC =  onChipL1AEnC ? 12'H000 : BCIDoffsetC;

BCIDCounterTMR BC (
          .clkA(clkA),
          .clkB(clkB),
          .clkC(clkC),
          .disA(disA),
          .disB(disB),
          .disC(disC),
          .resetA(dnResetA),
          .resetB(dnResetB),
          .resetC(dnResetC),
          .rstBCIDA(BCIDRstA),
          .rstBCIDB(BCIDRstB),
          .rstBCIDC(BCIDRstC),
          .offsetA(actualOffsetA),
          .offsetB(actualOffsetB),
          .offsetC(actualOffsetC),
          .BCIDA(genBCIDA),
          .BCIDB(genBCIDB),
          .BCIDC(genBCIDC)
          );
wire dnLoadA;
wire dnLoadB;
wire dnLoadC;
wire [11:0] gbrcBCIDA;
wire [11:0] gbrcBCIDB;
wire [11:0] gbrcBCIDC;
wire [L1ADDRWIDTH-1:0] wrAddrA;
wire [L1ADDRWIDTH-1:0] wrAddrB;
wire [L1ADDRWIDTH-1:0] wrAddrC;
wire [L1ADDRWIDTH-1:0] rdAddrA;
wire [L1ADDRWIDTH-1:0] rdAddrB;
wire [L1ADDRWIDTH-1:0] rdAddrC;
assign wrAddrA =  dnBCSTA[L1ADDRWIDTH*2+2:L1ADDRWIDTH+3] ;
assign wrAddrB =  dnBCSTB[L1ADDRWIDTH*2+2:L1ADDRWIDTH+3] ;
assign wrAddrC =  dnBCSTC[L1ADDRWIDTH*2+2:L1ADDRWIDTH+3] ;
assign rdAddrA =  dnBCSTA[L1ADDRWIDTH+2:3] ;
assign rdAddrB =  dnBCSTB[L1ADDRWIDTH+2:3] ;
assign rdAddrC =  dnBCSTC[L1ADDRWIDTH+2:3] ;
assign dnL1AA =  dnBCSTA[1] ;
assign dnL1AB =  dnBCSTB[1] ;
assign dnL1AC =  dnBCSTC[1] ;
assign dnLoadA =  dnBCSTA[2] ;
assign dnLoadB =  dnBCSTB[2] ;
assign dnLoadC =  dnBCSTC[2] ;
assign dnResetA =  dnBCSTA[0] ;
assign dnResetB =  dnBCSTB[0] ;
assign dnResetC =  dnBCSTC[0] ;
wire BCBE1AA;
wire BCBE1AB;
wire BCBE1AC;
wire BCBE2AA;
wire BCBE2AB;
wire BCBE2AC;
wire resetBCIDBufferA =  dnResetA&~disA;
wire resetBCIDBufferB =  dnResetB&~disB;
wire resetBCIDBufferC =  dnResetC&~disC;

BCIDBufferTMR #(.ADDRWIDTH(L1ADDRWIDTH)) BCB (
          .clkA(clkA),
          .clkB(clkB),
          .clkC(clkC),
          .resetA(resetBCIDBufferA),
          .resetB(resetBCIDBufferB),
          .resetC(resetBCIDBufferC),
          .inBCIDA(genBCIDA),
          .inBCIDB(genBCIDB),
          .inBCIDC(genBCIDC),
          .rdEnA(dnLoadA),
          .rdEnB(dnLoadB),
          .rdEnC(dnLoadC),
          .L1AA(dnL1AA),
          .L1AB(dnL1AB),
          .L1AC(dnL1AC),
          .wrAddrA(wrAddrA),
          .wrAddrB(wrAddrB),
          .wrAddrC(wrAddrC),
          .rdAddrA(rdAddrA),
          .rdAddrB(rdAddrB),
          .rdAddrC(rdAddrC),
          .E1AA(BCBE1AA),
          .E1AB(BCBE1AB),
          .E1AC(BCBE1AC),
          .E2AA(BCBE2AA),
          .E2AB(BCBE2AB),
          .E2AC(BCBE2AC),
          .outBCIDA(gbrcBCIDA),
          .outBCIDB(gbrcBCIDB),
          .outBCIDC(gbrcBCIDC)
          );
wire [L1ADDRWIDTH-1:0] wordCountA;
wire [L1ADDRWIDTH-1:0] wordCountB;
wire [L1ADDRWIDTH-1:0] wordCountC;
wire eventStartA;
wire eventStartB;
wire eventStartC;
wire hitA;
wire hitB;
wire hitC;
wire [28:0] outTDCDataA;
wire [28:0] outTDCDataB;
wire [28:0] outTDCDataC;
wire [7:0] outPixelIDA;
wire [7:0] outPixelIDB;
wire [7:0] outPixelIDC;
wire [11:0] outBCIDA;
wire [11:0] outBCIDB;
wire [11:0] outBCIDC;
wire [1:0] EAA;
wire [1:0] EAB;
wire [1:0] EAC;
wire outL1BufFullA;
wire outL1BufFullB;
wire outL1BufFullC;
wire outL1BufHalfFullA;
wire outL1BufHalfFullB;
wire outL1BufHalfFullC;
wire outL1BufOverflowA;
wire outL1BufOverflowB;
wire outL1BufOverflowC;
wire streamBufAlmostFullA;
wire streamBufAlmostFullB;
wire streamBufAlmostFullC;
wire resetControllerA =  resetA&~disA;
wire resetControllerB =  resetB&~disB;
wire resetControllerC =  resetC&~disC;

globalReadoutControllerTMR #(.L1ADDRWIDTH(L1ADDRWIDTH), .BCSTWIDTH(BCSTWIDTH)) gbrcInst (
          .clkA(clkA),
          .clkB(clkB),
          .clkC(clkC),
          .resetA(resetControllerA),
          .resetB(resetControllerB),
          .resetC(resetControllerC),
          .L1AA(actualL1AA),
          .L1AB(actualL1AB),
          .L1AC(actualL1AC),
          .dnDataA(dnDataA),
          .dnDataB(dnDataB),
          .dnDataC(dnDataC),
          .dnUnreadHitA(dnUnreadHitA),
          .dnUnreadHitB(dnUnreadHitB),
          .dnUnreadHitC(dnUnreadHitC),
          .dnReadA(dnReadA),
          .dnReadB(dnReadB),
          .dnReadC(dnReadC),
          .dnBCSTA(dnBCSTA),
          .dnBCSTB(dnBCSTB),
          .dnBCSTC(dnBCSTC),
          .inBCIDA(gbrcBCIDA),
          .inBCIDB(gbrcBCIDB),
          .inBCIDC(gbrcBCIDC),
          .BCBE1AA(BCBE1AA),
          .BCBE1AB(BCBE1AB),
          .BCBE1AC(BCBE1AC),
          .BCBE2AA(BCBE2AA),
          .BCBE2AB(BCBE2AB),
          .BCBE2AC(BCBE2AC),
          .wordCountA(wordCountA),
          .wordCountB(wordCountB),
          .wordCountC(wordCountC),
          .streamBufAlmostFullA(streamBufAlmostFullA),
          .streamBufAlmostFullB(streamBufAlmostFullB),
          .streamBufAlmostFullC(streamBufAlmostFullC),
          .hitA(hitA),
          .hitB(hitB),
          .hitC(hitC),
          .eventStartA(eventStartA),
          .eventStartB(eventStartB),
          .eventStartC(eventStartC),
          .outTDCDataA(outTDCDataA),
          .outTDCDataB(outTDCDataB),
          .outTDCDataC(outTDCDataC),
          .outPixelIDA(outPixelIDA),
          .outPixelIDB(outPixelIDB),
          .outPixelIDC(outPixelIDC),
          .outBCIDA(outBCIDA),
          .outBCIDB(outBCIDB),
          .outBCIDC(outBCIDC),
          .outEAA(EAA),
          .outEAB(EAB),
          .outEAC(EAC),
          .outL1BufFullA(outL1BufFullA),
          .outL1BufFullB(outL1BufFullB),
          .outL1BufFullC(outL1BufFullC),
          .outL1BufOverflowA(outL1BufOverflowA),
          .outL1BufOverflowB(outL1BufOverflowB),
          .outL1BufOverflowC(outL1BufOverflowC),
          .outL1BufHalfFullA(outL1BufHalfFullA),
          .outL1BufHalfFullB(outL1BufHalfFullB),
          .outL1BufHalfFullC(outL1BufHalfFullC)
          );
wire [1:0] DBSA;
wire [1:0] DBSB;
wire [1:0] DBSC;
assign DBSA =  {outL1BufOverflowA,outL1BufHalfFullA};
assign DBSB =  {outL1BufOverflowB,outL1BufHalfFullB};
assign DBSC =  {outL1BufOverflowC,outL1BufHalfFullC};
reg  [7:0] L1CounterA;
reg  [7:0] L1CounterB;
reg  [7:0] L1CounterC;
wire [7:0] nextL1CountA =  L1CounterA+1;
wire [7:0] nextL1CountB =  L1CounterB+1;
wire [7:0] nextL1CountC =  L1CounterC+1;

always @( posedge clkA )
     begin
          if (~resetA|L1A_RstA)
               begin
                    L1CounterA <= 8'd0;
               end
          else
               if (actualL1AA)
                    begin
                         L1CounterA <= nextL1CountVotedA;
                    end
     end

always @( posedge clkB )
     begin
          if (~resetB|L1A_RstB)
               begin
                    L1CounterB <= 8'd0;
               end
          else
               if (actualL1AB)
                    begin
                         L1CounterB <= nextL1CountVotedB;
                    end
     end

always @( posedge clkC )
     begin
          if (~resetC|L1A_RstC)
               begin
                    L1CounterC <= 8'd0;
               end
          else
               if (actualL1AC)
                    begin
                         L1CounterC <= nextL1CountVotedC;
                    end
     end
wire [39:0] dataFrameA;
wire [39:0] dataFrameB;
wire [39:0] dataFrameC;
wire [1:0] frameTypeA;
wire [1:0] frameTypeB;
wire [1:0] frameTypeC;
assign frameTypeA =  2'b00;
assign frameTypeB =  2'b00;
assign frameTypeC =  2'b00;
wire holdFrameBuilderA =  streamBufAlmostFullA|disA;
wire holdFrameBuilderB =  streamBufAlmostFullB|disB;
wire holdFrameBuilderC =  streamBufAlmostFullC|disC;

frameBuilderTMR fbInst (
          .clkA(clkA),
          .clkB(clkB),
          .clkC(clkC),
          .resetA(dnResetA),
          .resetB(dnResetB),
          .resetC(dnResetC),
          .TDCDataA(outTDCDataA),
          .TDCDataB(outTDCDataB),
          .TDCDataC(outTDCDataC),
          .typeA(frameTypeA),
          .typeB(frameTypeB),
          .typeC(frameTypeC),
          .hitA(hitA),
          .hitB(hitB),
          .hitC(hitC),
          .chipIDA(chipIdA),
          .chipIDB(chipIdB),
          .chipIDC(chipIdC),
          .eventStartA(eventStartA),
          .eventStartB(eventStartB),
          .eventStartC(eventStartC),
          .pixelIDA(outPixelIDA),
          .pixelIDB(outPixelIDB),
          .pixelIDC(outPixelIDC),
          .EAA(EAA),
          .EAB(EAB),
          .EAC(EAC),
          .BCIDA(outBCIDA),
          .BCIDB(outBCIDB),
          .BCIDC(outBCIDC),
          .L1CounterA(L1CounterA),
          .L1CounterB(L1CounterB),
          .L1CounterC(L1CounterC),
          .L1BufFullA(outL1BufFullA),
          .L1BufFullB(outL1BufFullB),
          .L1BufFullC(outL1BufFullC),
          .L1BufHalfFullA(outL1BufHalfFullA),
          .L1BufHalfFullB(outL1BufHalfFullB),
          .L1BufHalfFullC(outL1BufHalfFullC),
          .L1BufOverflowA(outL1BufOverflowA),
          .L1BufOverflowB(outL1BufOverflowB),
          .L1BufOverflowC(outL1BufOverflowC),
          .streamBufAlmostFullA(holdFrameBuilderA),
          .streamBufAlmostFullB(holdFrameBuilderB),
          .streamBufAlmostFullC(holdFrameBuilderC),
          .dataFrameA(dataFrameA),
          .dataFrameB(dataFrameB),
          .dataFrameC(dataFrameC)
          );
wire [31:0] doutA;
wire [31:0] doutB;
wire [31:0] doutC;
wire [15:0] encTrigHitsA;
wire [15:0] encTrigHitsB;
wire [15:0] encTrigHitsC;
wire resetTriggerProcessA =  dnResetA&~disA;
wire resetTriggerProcessB =  dnResetB&~disB;
wire resetTriggerProcessC =  dnResetC&~disC;

triggerProcessorTMR TPInst (
          .clkA(clkA),
          .clkB(clkB),
          .clkC(clkC),
          .resetA(resetTriggerProcessA),
          .resetB(resetTriggerProcessB),
          .resetC(resetTriggerProcessC),
          .trigHitsA(trigHitsA),
          .trigHitsB(trigHitsB),
          .trigHitsC(trigHitsC),
          .BCIDA(genBCIDA),
          .BCIDB(genBCIDB),
          .BCIDC(genBCIDC),
          .emptySlotBCIDA(emptySlotBCIDA),
          .emptySlotBCIDB(emptySlotBCIDB),
          .emptySlotBCIDC(emptySlotBCIDC),
          .encTrigHitsA(encTrigHitsA),
          .encTrigHitsB(encTrigHitsB),
          .encTrigHitsC(encTrigHitsC)
          );
wire resetStreamBufferA =  dnResetA&~disA;
wire resetStreamBufferB =  dnResetB&~disB;
wire resetStreamBufferC =  dnResetC&~disC;
wire [31:0] triggerDataA =  {16'd0,encTrigHitsA};
wire [31:0] triggerDataB =  {16'd0,encTrigHitsB};
wire [31:0] triggerDataC =  {16'd0,encTrigHitsC};

streamBufferTMR #(.FIFODEPTH(2)) streamBufferInst (
          .clkA(clkA),
          .clkB(clkB),
          .clkC(clkC),
          .resetA(resetStreamBufferA),
          .resetB(resetStreamBufferB),
          .resetC(resetStreamBufferC),
          .rateA(serRateA),
          .rateB(serRateB),
          .rateC(serRateC),
          .triggerDataSizeA(trigDataSizeA),
          .triggerDataSizeB(trigDataSizeB),
          .triggerDataSizeC(trigDataSizeC),
          .triggerDataA(triggerDataA),
          .triggerDataB(triggerDataB),
          .triggerDataC(triggerDataC),
          .dataFrameA(dataFrameA),
          .dataFrameB(dataFrameB),
          .dataFrameC(dataFrameC),
          .RT_BCIDA(genBCIDA),
          .RT_BCIDB(genBCIDB),
          .RT_BCIDC(genBCIDC),
          .DBSA(DBSA),
          .DBSB(DBSB),
          .DBSC(DBSC),
          .RT_L1CounterA(L1CounterA),
          .RT_L1CounterB(L1CounterB),
          .RT_L1CounterC(L1CounterC),
          .almostFullA(streamBufAlmostFullA),
          .almostFullB(streamBufAlmostFullB),
          .almostFullC(streamBufAlmostFullC),
          .doutA(doutA),
          .doutB(doutB),
          .doutC(doutC)
          );
wire [1:0] dataWidthA;
wire [1:0] dataWidthB;
wire [1:0] dataWidthC;
assign dataWidthA =  serRateA;
assign dataWidthB =  serRateB;
assign dataWidthC =  serRateC;
wire [31:0] scrA;
wire [31:0] scrB;
wire [31:0] scrC;
wire resetScramblerA =  dnResetA&~disA;
wire resetScramblerB =  dnResetB&~disB;
wire resetScramblerC =  dnResetC&~disC;

ScramblerTMR scrInst (
          .clkA(clkA),
          .clkB(clkB),
          .clkC(clkC),
          .resetA(resetScramblerA),
          .resetB(resetScramblerB),
          .resetC(resetScramblerC),
          .dataWidthA(dataWidthA),
          .dataWidthB(dataWidthB),
          .dataWidthC(dataWidthC),
          .dinA(doutA),
          .dinB(doutB),
          .dinC(doutC),
          .bypassA(disSCRA),
          .bypassB(disSCRB),
          .bypassC(disSCRC),
          .doutA(scrA),
          .doutB(scrB),
          .doutC(scrC)
          );
reg  [1:0] slow_ctrl_link_reset_delayA;
reg  [1:0] slow_ctrl_link_reset_delayB;
reg  [1:0] slow_ctrl_link_reset_delayC;
reg  link_reset_regA;
reg  link_reset_regB;
reg  link_reset_regC;

always @( negedge clkA )
     begin
          if (~disA)
               begin
                    slow_ctrl_link_reset_delayA <= {slow_ctrl_link_reset_delayA[0] ,link_reset_slowControlA};
                    link_reset_regA <= slow_ctrl_link_reset_delayA[1] |link_reset_fastCommandA;
               end
     end

always @( negedge clkB )
     begin
          if (~disB)
               begin
                    slow_ctrl_link_reset_delayB <= {slow_ctrl_link_reset_delayB[0] ,link_reset_slowControlB};
                    link_reset_regB <= slow_ctrl_link_reset_delayB[1] |link_reset_fastCommandB;
               end
     end

always @( negedge clkC )
     begin
          if (~disC)
               begin
                    slow_ctrl_link_reset_delayC <= {slow_ctrl_link_reset_delayC[0] ,link_reset_slowControlC};
                    link_reset_regC <= slow_ctrl_link_reset_delayC[1] |link_reset_fastCommandC;
               end
     end

serializerTMR serInst (
          .link_resetA(link_reset_regA),
          .link_resetB(link_reset_regB),
          .link_resetC(link_reset_regC),
          .disA(disA),
          .disB(disB),
          .disC(disC),
          .testPatternSelA(link_reset_testPatternSelA),
          .testPatternSelB(link_reset_testPatternSelB),
          .testPatternSelC(link_reset_testPatternSelC),
          .fixedTestPatternA(link_reset_fixedTestPatternA),
          .fixedTestPatternB(link_reset_fixedTestPatternB),
          .fixedTestPatternC(link_reset_fixedTestPatternC),
          .clk1280A(clk1280A),
          .clk1280B(clk1280B),
          .clk1280C(clk1280C),
          .rateA(serRateA),
          .rateB(serRateB),
          .rateC(serRateC),
          .clk40synA(clkA),
          .clk40synB(clkB),
          .clk40synC(clkC),
          .dinA(scrA),
          .dinB(scrB),
          .dinC(scrC),
          .sout(sout)
          );

majorityVoter #(.WIDTH(8)) nextL1CountVoterA (
          .inA(nextL1CountA),
          .inB(nextL1CountB),
          .inC(nextL1CountC),
          .out(nextL1CountVotedA),
          .tmrErr(nextL1CountTmrErrorA)
          );

majorityVoter #(.WIDTH(8)) nextL1CountVoterB (
          .inA(nextL1CountA),
          .inB(nextL1CountB),
          .inC(nextL1CountC),
          .out(nextL1CountVotedB),
          .tmrErr(nextL1CountTmrErrorB)
          );

majorityVoter #(.WIDTH(8)) nextL1CountVoterC (
          .inA(nextL1CountA),
          .inB(nextL1CountB),
          .inC(nextL1CountC),
          .out(nextL1CountVotedC),
          .tmrErr(nextL1CountTmrErrorC)
          );
endmodule

