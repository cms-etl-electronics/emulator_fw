//
// 
// 2022.2.12
// fastCommandEncoderTop
//
//  dbw
/////////////////////////////////////////////////////
`include "ETROC2Readout/commonDefinition.v"
module fastCommandEncoderTop(
input clk40,
input clk320,
input dnReset,
output fc_fc

// for test
//output 

);
reg [7:0] fc_byte;


localparam fc_IDLE 		= 8'b1111_0000; //8'hF0 -->n_fc = 0
localparam fc_BCR 		= 8'b0101_1010; //8'H5A -->n_fc = 5
localparam fc_L1A       = 8'b1001_0110; //8'H96 -->n_fc = 8
localparam fc_L1A_BCR   = 8'b1001_1001; //8'H99 -->n_fc = 9

//wire L1Mode = onChipL1AConf[0];
wire L1A_signal;
    TestL1Generator TestL1GeneratorInst(
        .clk(clk40),
        .dis(1'b0),// 1:disable 0:no disable
        .mode(1'b0),//0 is periodic trigger, 1 is random trigger 2022-03-24 change to periodic 1'b0
        .reset(dnReset),
        .L1A(L1A_signal) //
    );
	 
	 reg [11:0] BCIDcnt;
	 
	 reg BCR_signal;
	 
	 always @(posedge clk40)
	 begin
			if (~dnReset)
			begin
				BCIDcnt<=12'h000;
				BCR_signal<=1'b0;
			
			end
			else
			begin
				if (BCIDcnt==`MAX_BCID_NUMBER)
				begin
					BCIDcnt <= 12'h000; //BCID from 0 to 3563
					BCR_signal<=1'b1;
				end
				else 
				begin
					BCIDcnt<=BCIDcnt+1'b1;
					BCR_signal<=1'b0;
				end
			end
	 end
	 
//	 reg L1A_signal_R;
//	 reg BCR_signal_R;
	 
//	 always @(posedge clk320) begin
//		L1A_signal_R<=L1A_signal;
//		BCR_signal_R<=BCR_signal;
//	 
//	 end
	
	  reg [2:0] cnt_fc=3'b0;
    reg [7:0] fc_byte_o;
	 always @(posedge clk40)
	 begin
	 if (L1A_signal & BCR_signal) fc_byte_o<=fc_L1A_BCR;
				else if (L1A_signal) fc_byte_o<=fc_L1A;
				     else if (BCR_signal) fc_byte_o<=fc_BCR;
							 else fc_byte_o <= fc_IDLE; //sampling parallel 8 bits fc at 320 MHz clock
	 end
	 
	          
	 
    always@(posedge clk320) begin
        cnt_fc <= cnt_fc + 1'b1;
        if(cnt_fc==3'd0)
		   begin
				fc_byte<=fc_byte_o;
			end
        else
            fc_byte[7:0] <= {fc_byte[6:0],fc_byte[7]}; //shift 8-bit parallel fc in 40 MHz period, serializing to 1 bit
    end

    //wire fc_fc;
    assign fc_fc = fc_byte[7];
	

endmodule
