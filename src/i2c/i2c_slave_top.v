`timescale	1ns/1ps

module i2c_slave_top(
	input reset_n,
	input sda,
	inout scl,
	input clock, 
	output [7:0] data_in_reg0,   
   output  [7:0]  data_in_reg1,
       // clock_signalTap,
	// for testbench interface add ,dengbw 20200323
	output sda_en,
   output sda_out,
	
	// for chipscope watching
	output [3:0] main_state,
	//	reg_addr_t_buf,
	output [7:0]	dat_out_regs,
	output [7:0]	dat_in_regs,
		//addr_in_state,
	//	data_in_state,
	//	data_out_state,
	//	reg_addr_state,
	output [7:0] regOut00,
  output [7:0] regOut01,
  output [7:0] regOut02,
  output [7:0] regOut03,
  output [7:0] regOut04,
  output [7:0] regOut05,
  output [7:0] regOut06,
  output [7:0] regOut07,
  output [7:0] regOut08,
  output [7:0] regOut09,
  output [7:0] regOut0A,
  output [7:0] regOut0B,
  output [7:0] regOut0C,
  output [7:0] regOut0D,
  output [7:0] regOut0E,
  output [7:0] regOut0F,
  output [7:0] regOut10,
  output [7:0] regOut11,
  output [7:0] regOut12,
  output [7:0] regOut13,
  output [7:0] regOut14,
  output [7:0] regOut15,
  output [7:0] regOut16,
  output [7:0] regOut17,
  output [7:0] regOut18,
  output [7:0] regOut19,
  output [7:0] regOut1A,
  output [7:0] regOut1B,
  output [7:0] regOut1C,
  output [7:0] regOut1D,
  output [7:0] regOut1E,
  output [7:0] regOut1F,
		
		
  output	wea,
  output [8:0]    addra,
  output   ena		
	
	);
	
//input		reset_n;	//extern signal
//input		scl;
//inout		sda;	
//input		clock;		//intern signal
//
//output	[7:0]	data_in_reg0;	//to define eight register 
//output	[7:0]	data_in_reg1;
//
//
//output 	sda_en;
//output   sda_out;
//
//
//// for chipscope watching
//output	[3:0]	main_state; 
//
//output [7:0] dat_out_regs;
//
//output  [7:0] dat_in_regs;
//output  wea;
//output [8:0]addra;
//output  ena;


wire sda_in;

    
assign  sda_in = (!sda_en) ? sda  : 1'bz;
assign	sda    = sda_en ? sda_out : 1'bz;

//i2c_slave	i2c_slave_inst(
			//.reset_n(reset_n),
//			.clock(clock),
//			.sda_out(sda_out),
//			.sda_in(sda_in),
//			.sda_en(sda_en),
//			.scl(scl),
//			.data_reg0(data_in_reg0),   
//			.data_reg1(data_in_reg1),
			
			// for chipscope 
//			.main_state(main_state),
			//	reg_addr_t_buf,
//			.dat_out_regs(dat_out_regs),
//			.dat_in_regs(dat_in_regs),
			//addr_in_state,
			//	data_in_state,
			//	data_out_state,
			//	reg_addr_state,
//			.wea(wea),
//			.addra(addra),
//			.ena(ena)
//                            );


wire sclout;
wire sclen;
wire [15:0] wbAdr;
wire wbWe;
wire [7:0] wbDataIn;
wire [7:0] wbDataOut;
wire active1;
wire start1;
wire stop1;

assign wbDataIn = wbDataOut;

//i2cSlave	i2cSlave_inst(
//			//.reset_n(reset_n),
//			//.clock(clock),
//			.SDAin(sda_in),
//			.SDAout(sda_out),
//			.SDAen(sda_en),
//			.SCLin(scl),
//			.SCLout(sclout),
//			.SCLen(sclen),
//			
//			.i2cAddr(7'b1110010),
//			.driveSDA(1'b1),
//			
//			.rst(1'b0),
//			.clk(clock),
//
//			.wbAdr(wbAdr),
//			.wbWe(wbWe),
//			.wbDataIn(wbDataOut),
//			.wbDataOut(wbDataOut),
//			
//			.active(active1),
//			.start(start1),
//			.stop(stop1)
//                            );
//
//************************************************
//

  wire [7:0] regOut00;
  wire [7:0] regOut01;
  wire [7:0] regOut02;
  wire [7:0] regOut03;
  wire [7:0] regOut04;
  wire [7:0] regOut05;
  wire [7:0] regOut06;
  wire [7:0] regOut07;
  wire [7:0] regOut08;
  wire [7:0] regOut09;
  wire [7:0] regOut0A;
  wire [7:0] regOut0B;
  wire [7:0] regOut0C;
  wire [7:0] regOut0D;
  wire [7:0] regOut0E;
  wire [7:0] regOut0F;
  wire [7:0] regOut10;
  wire [7:0] regOut11;
  wire [7:0] regOut12;
  wire [7:0] regOut13;
  wire [7:0] regOut14;
  wire [7:0] regOut15;
  wire [7:0] regOut16;
  wire [7:0] regOut17;
  wire [7:0] regOut18;
  wire [7:0] regOut19;
  wire [7:0] regOut1A;
  wire [7:0] regOut1B;
  wire [7:0] regOut1C;
  wire [7:0] regOut1D;
  wire [7:0] regOut1E;
  wire [7:0] regOut1F;
  
  wire [7:0] defVal00 = 8'h00;
  wire [7:0] defVal01 = 8'h01;
  wire [7:0] defVal02 = 8'h02;
  wire [7:0] defVal03 = 8'h03;
  wire [7:0] defVal04 = 8'h04;
  wire [7:0] defVal05 = 8'h05;
  wire [7:0] defVal06 = 8'h06;
  wire [7:0] defVal07 = 8'h07;
  wire [7:0] defVal08 = 8'h08;
  wire [7:0] defVal09 = 8'h09;
  wire [7:0] defVal0A = 8'h0A;
  wire [7:0] defVal0B = 8'h0B;
  wire [7:0] defVal0C = 8'h0C;
  wire [7:0] defVal0D = 8'h0D;
  wire [7:0] defVal0E = 8'h0E;
  wire [7:0] defVal0F = 8'h0F;
  wire [7:0] defVal10 = 8'h10;
  wire [7:0] defVal11 = 8'h11;
  wire [7:0] defVal12 = 8'h12;
  wire [7:0] defVal13 = 8'h13;
  wire [7:0] defVal14 = 8'h14;
  wire [7:0] defVal15 = 8'h15;
  wire [7:0] defVal16 = 8'h16;
  wire [7:0] defVal17 = 8'h17;
  wire [7:0] defVal18 = 8'h18;
  wire [7:0] defVal19 = 8'h19;
  wire [7:0] defVal1A = 8'h1A;
  wire [7:0] defVal1B = 8'h1B;
  wire [7:0] defVal1C = 8'h1C;
  wire [7:0] defVal1D = 8'h1D;
  wire [7:0] defVal1E = 8'h1E;
  wire [7:0] defVal1F = 8'h1F;

  wire [7:0] regIn100 = regOut00;
  wire [7:0] regIn101 = regOut01;
  wire [7:0] regIn102 = regOut02;
  wire [7:0] regIn103 = regOut03;
  wire [7:0] regIn104 = regOut04;
  wire [7:0] regIn105 = regOut05;
  wire [7:0] regIn106 = regOut06;
  wire [7:0] regIn107 = regOut07;
  wire [7:0] regIn108 = regOut08;
  wire [7:0] regIn109 = regOut09;
  wire [7:0] regIn10A = regOut0A;
  wire [7:0] regIn10B = regOut0B;
  wire [7:0] regIn10C = regOut0C;
  wire [7:0] regIn10D = regOut0D;
  wire [7:0] regIn10E = regOut0E;
  wire [7:0] regIn10F = regOut0F;

 //device address, set to 7'b1110010 = 7'h72
  wire [6:2] Ain = 5'b11100;
  wire A0ds;
  wire A0en;
  wire A0in = 1'b0;
  wire A0out;
  wire A0ud;
  wire A0pe;
  
  wire A1ds;
  wire A1en;
  wire A1in = 1'b1;
  wire A1out;
  wire A1ud;
  wire A1pe;

  //some outputs ???
  localparam HIGH_LOW_OUTS = 50;
  wire [HIGH_LOW_OUTS-1:0]  HIGH;
  wire [HIGH_LOW_OUTS-1:0]  LOW;

  wire RSTNds;
  wire RSTNen;
  wire RSTNin;
  wire RSTNout;
  wire RSTNud;
  wire RSTNpe;
  assign RSTNin = reset_n;

  wire SCLds;
  wire SCLen;
  wire SCLin;
  wire SCLout;
  wire SCLpe;
  wire SCLud;
  assign SCLin = scl;

  wire SDAds;
  wire SDAen;
  wire SDAin;
  wire SDAout;
  wire SDApe;
  wire SDAud;
  assign sda_en = SDAen;
  assign SDAin = sda_in;
  assign sda_out = SDAout;

  wire [15:0] busAddr; // tmrg triplicate busAddr
  wire        busClk; // tmrg triplicate busClk
  wire [7:0]  busDataMiso; // tmrg triplicate busDataMiso
  wire [7:0]  busDataMosi; // tmrg triplicate busDataMosi
  wire        busRe; // tmrg triplicate busRe
  wire        busRst; // tmrg triplicate busRst
  wire        busTmrError; // tmrg triplicate busTmrError
  wire        busWe; // tmrg triplicate busWe

  
  periphery2 periphery2_inst (
    .clk(clock),
    .A0ds(A0ds),
    .A0en(A0en),
    .A0in(A0in),
    .A0out(A0out),
    .A0pe(A0pe),
    .A0ud(A0ud),
    .A1ds(A1ds),
    .A1en(A1en),
    .A1in(A1in),
    .A1out(A1out),
    .A1pe(A1pe),
    .A1ud(A1ud),
    .Ain(Ain),
    .HIGH(HIGH),
    .LOW(LOW),
    .RSTNds(RSTNds),
    .RSTNen(RSTNen),
    .RSTNin(RSTNin),
    .RSTNout(RSTNout),
    .RSTNpe(RSTNpe),
    .RSTNud(RSTNud),
    .SCLds(SCLds),
    .SCLen(SCLen),
    .SCLin(SCLin),
    .SCLout(SCLout),
    .SCLpe(SCLpe),
    .SCLud(SCLud),
    .SDAds(SDAds),
    .SDAen(SDAen),
    .SDAin(SDAin),
    .SDAout(SDAout),
    .SDApe(SDApe),
    .SDAud(SDAud),
    .chipID(4'h3),
    .chipREV(4'b0),
    .matrixBusAddr(busAddr),
    .matrixBusClk(busClk),
    .matrixBusDataMiso(busDataMiso),
    .matrixBusDataMosi(busDataMosi),
    .matrixBusRe(busRe),
    .matrixBusRst(busRst),
    .matrixBusTmrError(busTmrError),
    .matrixBusWe(busWe),
    
    .defVal00(defVal00),
    .defVal01(defVal01),
    .defVal02(defVal02),
    .defVal03(defVal03),
    .defVal04(defVal04),
    .defVal05(defVal05),
    .defVal06(defVal06),
    .defVal07(defVal07),
    .defVal08(defVal08),
    .defVal09(defVal09),
    .defVal0A(defVal0A),
    .defVal0B(defVal0B),
    .defVal0C(defVal0C),
    .defVal0D(defVal0D),
    .defVal0E(defVal0E),
    .defVal0F(defVal0F),
    .defVal10(defVal10),
    .defVal11(defVal11),
    .defVal12(defVal12),
    .defVal13(defVal13),
    .defVal14(defVal14),
    .defVal15(defVal15),
    .defVal16(defVal16),
    .defVal17(defVal17),
    .defVal18(defVal18),
    .defVal19(defVal19),
    .defVal1A(defVal1A),
    .defVal1B(defVal1B),
    .defVal1C(defVal1C),
    .defVal1D(defVal1D),
    .defVal1E(defVal1E),
    .defVal1F(defVal1F),

    .regIn100(regIn100),
    .regIn101(regIn101),
    .regIn102(regIn102),
    .regIn103(regIn103),
    .regIn104(regIn104),
    .regIn105(regIn105),
    .regIn106(regIn106),
    .regIn107(regIn107),
    .regIn108(regIn108),
    .regIn109(regIn109),
    .regIn10A(regIn10A),
    .regIn10B(regIn10B),
    .regIn10C(regIn10C),
    .regIn10D(regIn10D),
    .regIn10E(regIn10E),
    .regIn10F(regIn10F),

    .regOut00(regOut00),
    .regOut01(regOut01),
    .regOut02(regOut02),
    .regOut03(regOut03),
    .regOut04(regOut04),
    .regOut05(regOut05),
    .regOut06(regOut06),
    .regOut07(regOut07),
    .regOut08(regOut08),
    .regOut09(regOut09),
    .regOut0A(regOut0A),
    .regOut0B(regOut0B),
    .regOut0C(regOut0C),
    .regOut0D(regOut0D),
    .regOut0E(regOut0E),
    .regOut0F(regOut0F),
    .regOut10(regOut10),
    .regOut11(regOut11),
    .regOut12(regOut12),
    .regOut13(regOut13),
    .regOut14(regOut14),
    .regOut15(regOut15),
    .regOut16(regOut16),
    .regOut17(regOut17),
    .regOut18(regOut18),
    .regOut19(regOut19),
    .regOut1A(regOut1A),
    .regOut1B(regOut1B),
    .regOut1C(regOut1C),
    .regOut1D(regOut1D),
    .regOut1E(regOut1E),
    .regOut1F(regOut1F)
  );


//
//************************************************
//

	
endmodule
