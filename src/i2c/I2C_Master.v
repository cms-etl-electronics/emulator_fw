// 2017.5.13 
// Author: dengbinwei
// 2020.3.20 modify based PO3100K I2c master 
// program for lpGBT I2c master 2020.3.20
//////////////////////////////////////////////////////////////////

`define MEM_LENGTH 10
module I2C_Master
(
     sysclk,reset,sda,scl,i2cclk,current_state,reg_ack, size,IIC_START,IIC_END, i2c_w_regaddr,i2c_r_regaddr,i2cdata,
	   multi_write_num,  multi_read_num,op_mode,
		// for test 
		readvalue1,
		readvalue2,
		readvalue3,
		readvalue4,
		readvalue5,
		readvalue6,
		readvalue7,
		readvalue8,
		readvalue9,
		readvalue10
);
  // output wire 		PO_STDBY;	// sensor standby 			
 //	output wire 		PO_RSTB;	// sensor reset (low active)	
   input sysclk,reset;
   inout sda;
	output scl; 
	
	 output i2cclk;
	 //reg sda;
	 reg scl;
	 output current_state;
	 //output Readvalue;
	 output reg_ack;
	 input size;
	 input IIC_START;
	 output IIC_END;
	 input i2c_w_regaddr;
	 input i2c_r_regaddr;
	 input i2cdata;
	 input multi_write_num;//
	 input multi_read_num;
	 input op_mode;
	 
		output readvalue1;
		output readvalue2;
		output readvalue3;
		output readvalue4;
		output readvalue5;
		output readvalue6;
		output readvalue7;
		output readvalue8;
		output readvalue9;
		output readvalue10;

wire [15:0]size;	 
	 /***********************************/
reg clock;
reg IIC_END;
wire [15:0] i2c_w_regaddr;
wire [15:0] i2c_r_regaddr;
wire [7:0] i2cdata;
wire [8:0] multi_write_num;
wire [8:0] multi_read_num;
wire [1:0] op_mode;// I2C Master operation mode:WO, RO, FWLR, FRLW
// for inout sim
wire sda_in;
reg sda_out;
reg sda_en_in;
assign sda=(!sda_en_in)?sda_out:1'bz;
assign sda_in=(sda_en_in)?sda:1'bz;


	parameter WO=     2'b00;// write only
	parameter RO=     2'b01;// read only
	parameter FWLR=   2'b10;// first WRITE, last read
	parameter FRLW=   2'b11;// first READ, last write

//assign PO_STDBY = 1'b0; // Sensor not standby
//assign PO_RSTB = 1'b1;  // Sensor resetb = 1
	 
	 //////////// FSM///////////////////////////////////////////
	 // PO3100K I2c status FSM,, lpGBT I2c Multiple write
	 // S S-addr  W  A   R-addr  A   DATA A  DATA A .... DATA A  P
	 // S s-addr W   A   R-addr  A    Sr S-addr R  A   DATA  A DATA A  .... DATA NA P

//
// 320 *16bit register  
reg [8:0] multi_write_cnt=9'b0;// 
reg [8:0] multi_read_cnt=9'b0;

	 parameter Prepare= 6'd30,
	           Start0=6'd31,
				  I2cPrepare=6'd32,
				  SadrM=6'd34,
				 // Write0M=35,
				  Ack0S=6'd36,
				  RadrM=6'd37,
				  Ack1S=6'd38,
				  DataM=6'd39,
				  Ack2S=6'd40,
				  Stop=6'd41,
				  PrepareR=6'd42,
				  StartR=6'd43,
				  SadrMR=6'd44,
				  Ack0SR=6'd45,
				  RadrMR=6'd46,
				  Ack1SR=6'd47,
				  Sr1=6'd48,
				  SadrMR1=6'd49,
				  Ack2SR=6'd50,
				  DataR=6'd51,
				  G_NAck=6'd52,
				  Stop1=6'd53,
				  Idle=6'd54,
				  G_Ack=6'd55;
				  
	 parameter slaveaddrw=8'hE4;	//lpGBT write
	 parameter slaveaddrr=8'hE5;   //lpGBT read

	 reg [5:0] current_state=Prepare;	 
	// parameter size = 26;

//	 reg [8:1] memreg [0:28-1];
//	 initial $readmemh("source/register.list", memreg);
	 reg [17:0] bmemreg;  //the 16-bit address register+2 ACK 2022-06-28
	 
//	 reg [8:1] memvalue [0:26-1];
//	 initial	 $readmemh("source/data.list", memvalue);
	 reg [8:1] bmemvalue;
	 
	
	
	 reg [4:0] cnt2=5'd17;
    reg [3:0] cnt1=4'd8;	 
	 reg [1:0] cnt=2'b0;
	 reg[5:0] count1=6'b0;
	 reg reg_ack;
	 
	 reg[20:0] count=21'b0;
	 reg [7:0] repnum=8'b0;
	 reg [8:1]Readvalue[`MEM_LENGTH:1];
	 reg [$clog2(`MEM_LENGTH+1)-1:0] Read_cnt=1;// Readvalue addr counter
	 
	wire [7:0] readvalue1;
	wire [7:0] readvalue2;
	wire [7:0] readvalue3;
	wire [7:0] readvalue4;
	wire [7:0] readvalue5;
	wire [7:0] readvalue6;
	wire [7:0] readvalue7;
	wire [7:0] readvalue8;
	wire [7:0] readvalue9;
	wire [7:0] readvalue10;
	
	assign readvalue1=Readvalue[1];
	assign readvalue2=Readvalue[2];
	assign readvalue3=Readvalue[3];
	assign readvalue4=Readvalue[4];
	assign readvalue5=Readvalue[5];
	assign readvalue6=Readvalue[6];
	assign readvalue7=Readvalue[7];
	assign readvalue8=Readvalue[8];
	assign readvalue9=Readvalue[9];
	assign readvalue10=Readvalue[10];
	
	 
	 always@(posedge sysclk)  //8MHz
		begin
			if (reset==1'b0) count<=21'b0;
			else 
				 begin
				 count<=count+1'b1;
				 if (count<3'd5)  clock<=1'b1;
				 else if (count<4'd9) clock<=1'b0;           
				 else count<=21'b0;            // clock frequency:800kHz 
				 end
		end 

	
	
	 assign i2cclk=clock;
	 
	 
   reg i2c_reset_r=1'b1;
	reg i2c_reset_d=1'b1;
	wire i2c_reset;
	always @(posedge i2cclk)
	begin
	  if (i2c_reset_r==1'b1)
	  begin
	     i2c_reset_r<=1'b0;
	  end
	  i2c_reset_d<=i2c_reset_r;
	end
	//assign i2c_reset= ~(i2c_reset_r ^i2c_reset_d);
	assign i2c_reset= 1'b1;
	 
/////////////////////////////////////	 
	integer i; 
	 always @( posedge i2cclk  )
	 if( i2c_reset==1'b0 )	
	 begin
	   current_state<=Prepare;
		cnt<=2'b0;
		cnt1=4'd8;
		cnt2=5'd17;
		count1=6'b0;
		
		//sda<=1;
		sda_en_in=1'b0;
		sda_out<=1'b1;
		///////////sda///////////////////////
		
		
	   scl<=1'b1;
		//wcnt<=0;
		for ( i=1; i<=10; i=i+1)
		Readvalue[i]<=8'haa;// becuase 8'haa don't exist in the value variant number.
	   repnum<=8'b0;
		multi_write_cnt<=9'b0;
		multi_read_cnt<=9'b000000001;
		IIC_END<=1'b1;
		Read_cnt<=1;
	 end
	 else
	 begin
	 case(current_state)
			Prepare:
				begin
				// for test 
				for ( i=1; i<=10; i=i+1) Readvalue[i]<=8'haa;// becuase 8'haa don't exist in the value variant number.
				
				
				 if (IIC_START== 1'b0)
				   begin 
						IIC_END<=1'b1;
						current_state<=Prepare;
					end 
				 else
				   begin
						case (op_mode)
							 WO:current_state<=I2cPrepare;
							 RO:current_state<=PrepareR;
							 FWLR:current_state<=I2cPrepare;
							 FRLW:current_state<=PrepareR;
							 default:current_state<=Prepare; 
						 
						 endcase
					
				     
					end
				end
				I2cPrepare:
					begin
						// sda<=1;
						sda_en_in=1'b0;
						sda_out<=1'b1;
						 scl<=1'b1;
						 cnt<=cnt+1'b1;       
						 if (cnt==2'b10) 
						 begin 
						 cnt<=2'b00;
						 current_state<=Start0;
						 end
						 else current_state<=I2cPrepare;
					end	
				
		   Start0:  // send start signal
				begin
				   IIC_END<=1'b0;
					count1=count1+1'b1;          
					case (count1)
						1 :begin//sda<=1;
						 sda_en_in=1'b0;
		             sda_out<=1'b1;
						 end
						2 :scl<=1'b1;
						3 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						4 :scl<=1'b0;
						5 :begin 
							count1=6'b0;current_state<=SadrM; 
							end
						default;
				   endcase
        
				end
			SadrM:
			   begin
				count1=count1+1'b1;
					case (count1)
						1 :begin//sda<=slaveaddrw[cnt1-1]; //
						 sda_en_in=1'b0;
		             sda_out<=slaveaddrw[cnt1-1'b1];
						 end
						2 :scl<=1'b1;
						3 :scl<=1'b0;
						4 :begin 
								count1=6'b0;
								cnt1=cnt1-1'b1; 
								if (cnt1 ==4'b0000) 
									begin
										cnt1=4'd8;
									   current_state<=Ack0S;
									end
								else 
                           current_state<=SadrM;
							end
						default;
					endcase
        ////     addrw(count1,slaveaddrw,sda,scl,cnt1, current_state, Ack0S, SadrM  );
				end
			Ack0S:
				begin
				   count1=count1+1'b1;
					case (count1)
						1 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						2 : scl<=1'b1;
						3: begin//reg_ack=sda;
						 sda_en_in=1'b1;
		             end
						4:reg_ack<=sda_in;
			 			5 :begin scl<=1'b0;repnum<=repnum+1'b1; end
						6 :begin 
                       count1=6'b0;current_state<=RadrM; 
							  //bmemreg=memreg[repnum-1];
							  bmemreg[17:0]={i2c_w_regaddr[7:0],1'b0,i2c_w_regaddr[15:8],1'b0};//20200327 2022-06-28 added ACK
							  end
						default;
					endcase
		////			ACK(count1, sda, scl, current_state, RadrM );
				end
		   RadrM:
				begin
				 count1=count1+1'b1;
				 case (count1)
						1 ://begin sda<=bmemreg[cnt1];end//groupaddrw[cnt1]; //else sda<=regaddrw1[cnt1];//
						begin//sda<=bmemreg[cnt1];
						 sda_en_in=1'b0;
		             sda_out<=bmemreg[cnt2];
						 end
						
						2 :scl<=1'b1;
						3 :scl<=1'b0;
						4 :begin 
								count1=6'b0;
								cnt2=cnt2-1'b1; 
								if (cnt2 ==5'b00000) 
									begin
										cnt2=5'd17;  //was 5'd16, it may be wrong, changed to 5'd17, 2022-06-24
									   current_state<=Ack1S;
									end
								else 
                           current_state<=RadrM;
							end
						default;
					endcase

//				 addrw(count1,groupaddrw,sda,scl,cnt1, current_state, Ack1S, RadrM  );
				end
			Ack1S:
				begin
				   count1=count1+1'b1;
					case (count1)
						1 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						2 :scl<=1'b1;
						3: 	//reg_ack=sda;
						 sda_en_in=1'b1;
		            4: reg_ack<=sda_in;
						 
			 			5 :scl<=1'b0;
						6 :begin 
                       count1=6'b000000;current_state<=DataM; 
							  //bmemvalue<=memvalue[repnum-1];
							  bmemvalue[8:1]=i2cdata[7:0];
							  end
						default;
					endcase
//					ACK(count1, sda, scl, current_state, DataM );
				end
	      DataM:
				begin
				  count1=count1+1'b1;
				  case (count1)
						1 : //begin sda<=bmemvalue[cnt1];end//groupA[cnt1]; //else sda<=value1[cnt1];//
						begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=bmemvalue[cnt1];
						 end
						2 :scl<=1'b1;
						3 :scl<=1'b0;
						4 :begin 
								count1=6'b0;
								cnt1=cnt1-1'b1; 
								if (cnt1 ==4'b0000) 
									begin
										cnt1=4'd8;
									   current_state<=Ack2S;
										multi_write_cnt<=multi_write_cnt+1'b1;// write data inc
									end
								else 
								begin
                           current_state<=DataM;
								end
							end
						default;
					endcase
//				  addrw(count1,groupA,sda,scl,cnt1, current_state, Ack2S, DataM  );

				end
			Ack2S:
				begin
				   count1=count1+1'b1;
					case (count1)
						1 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						2 :scl<=1'b1;
						3: //reg_ack=sda;
						//begin//reg_ack=sda;
						 sda_en_in=1'b1;
		            4: reg_ack<=sda_in;
						// end
			 			5 :scl<=1'b0;
						6 :begin 
						     
						     if (multi_write_cnt== multi_write_num)
							  begin
									count1=6'b0;current_state<=Stop; 
								end
								else
								begin
								   bmemvalue[8:1]=i2cdata[7:0]+multi_write_cnt[7:0];// test 
								   count1=6'b0;current_state<=DataM;
								end
							  end
						default;
					endcase
//					ACK(count1, sda, scl, current_state, Stop );
				end	
			Stop: 
			begin          //
			   multi_write_cnt<=9'b0;
            count1=count1+1'b1;
				case (count1)
					 1 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
					 3 :scl<=1'b1;
					 10 :begin//sda<=1;
						 sda_en_in=1'b0;
		             sda_out<=1'b1;
						 end
					 15 :begin 
						    count1=6'b0;
							 
							 if (op_mode==FWLR) current_state<=PrepareR;
							 else current_state<=Prepare;
						  end
					 
					 default;
				endcase
//			   STOP(count1, sda, scl, current_state, PrepareR );
			end 



			
			PrepareR:
			begin
				 cnt<=cnt+1'b1;       
             if (cnt==2'b10) 
             begin 
             cnt<=2'b0;
				 cnt1=4'd8;
				 cnt2=5'd17;
				 count1=6'b0;
             current_state<=StartR;
             end
             else current_state<=PrepareR;
			
			end
	     StartR:
		  begin
		  	count1=count1+1'b1;          
					case (count1)
						1 :begin//sda<=1;
						 sda_en_in=1'b0;
		             sda_out<=1'b1;
						 end
						2 :scl<=1'b1;
						3 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						4 :scl<=1'b0;
						5 :begin 
							count1=6'b0;current_state<=SadrMR; 
							end
						default;
				   endcase
		  
		  end
			SadrMR:
			   begin
				count1=count1+1'b1;
					case (count1)
						1 ://sda<=slaveaddrw[cnt1-1]; //
						begin//;
						 sda_en_in=1'b0;
		             sda_out<=slaveaddrw[cnt1-1'b1];
						 end
						2 :scl<=1'b1;
						3 :scl<=1'b0;
						4 :begin 
								count1=6'b0;
								cnt1=cnt1-1'b1; 
								if (cnt1 ==4'b0000) 
									begin
										cnt1=4'd8;
									   current_state<=Ack0SR;
									end
								else 
                           current_state<=SadrMR;
							end
						default;
					endcase
				end
				
				Ack0SR:
				begin
				   count1=count1+1'b1;
					case (count1)
						1 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						2 :scl<=1'b1;
						3: //reg_ack=sda;
					//	begin//
						 sda_en_in=1'b1;
		            4: reg_ack<=sda_in;
					//	 end
			 			5 :scl<=1'b0;
						6 :begin 
						     bmemreg[17:0]={i2c_r_regaddr[7:0],1'b0,i2c_r_regaddr[15:8],1'b0};
                        count1=6'b0;current_state<=RadrMR; 
							  end
						default;
					endcase
					
				end
			 RadrMR:
				begin
				 count1=count1+1'b1;
				 case (count1)
						1 ://sda<=bmemreg[cnt1];//groupaddrw[cnt1]; //
						begin//
						 sda_en_in=1'b0;
		             sda_out<=bmemreg[cnt2];
						 end
						2 :scl<=1'b1;
						3 :scl<=1'b0;
						4 :begin 
								count1=6'b0;
								cnt2=cnt2-1'b1; 
								if (cnt2 ==5'b00000) 
									begin
										cnt2=5'd17;
									   current_state<=Ack1SR;
									end
								else 
                           current_state<=RadrMR;
							end
						default;
					endcase
					
				end
				Ack1SR:
				begin
				   count1=count1+1'b1;
					case (count1)
						1 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						2 :scl<=1'b1;
						3: //reg_ack=sda;
						//begin//
						 sda_en_in=1'b1;
		            4: reg_ack<=sda_in;
						// end
			 			5 :scl<=1'b0;
						6 :begin 
                       count1=6'b0;current_state<=Sr1; 
							  end
						default;
					endcase
					
				end
				Sr1:
				begin
				count1=count1+1'b1;          
					case (count1)
						1 :begin//sda<=1;
						 sda_en_in=1'b0;
		             sda_out<=1'b1;
						 end
						2 :scl<=1'b1;
						3 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						4 :scl<=1'b0;
						5 :begin 
							count1=6'b0;current_state<=SadrMR1; 
							end
						default;
				   endcase
		  	  end
			  SadrMR1:
			  begin
			  count1=count1+1'b1;
					case (count1)
						1 :begin//slaveaddrr[cnt1-1]; 
						 sda_en_in=1'b0;
		             sda_out<=slaveaddrr[cnt1-1];
						 end
						2 :scl<=1'b1;
						3 :scl<=1'b0;
						4 :begin 
								count1=6'b0;
								cnt1=cnt1-1'b1; 
								if (cnt1 ==4'b0000) 
									begin
										cnt1=4'd8;
									   current_state<=Ack2SR;
									end
								else 
                           current_state<=SadrMR1;
							end
						default;
					endcase
			  end
			  
			  Ack2SR:
				begin
				   count1=count1+1'b1;
					case (count1)
						1 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						2 :scl<=1'b1;
						3: //reg_ack=sda;
						//begin//
						 sda_en_in=1'b1;
		            4: reg_ack<=sda_in;
						// end
			 			5 :scl<=1'b0;
						6 :begin 
                       count1=6'b0;current_state<=DataR; 
							  end
						default;
					endcase
				end
				DataR:
				begin
					count1=count1+1'b1;
					case (count1)
					 1 ://sda<=1'bz;
					 begin
						 sda_en_in=1'b1;
		            
						 end
					 4 :scl<=1'b1;
					 8 ://Readvalue[cnt1]<=sda;
					 begin
						 sda_en_in=1'b1;
		             Readvalue[Read_cnt][cnt1]<=sda_in;
						 end
					 10 :scl<=1'b0;
					 12 :
						begin 
							cnt1=cnt1-1'b1;
							count1=6'b0;
							if (cnt1==4'b0000) 
							begin
								cnt1=4'd8; 
								 if (multi_read_cnt== multi_read_num)
							    begin
								   Read_cnt<=1'b1;
									multi_read_cnt<=9'b000000001;
									count1=6'b0;current_state<=G_NAck; 
								 end
								else
								begin
								   //bmemvalue[8:1]=i2cdata[7:0]+multi_write_cnt;// test 
									Read_cnt<=Read_cnt+1'b1;
								   count1=6'b0;current_state<=G_Ack;
								end
							end
							else current_state<=DataR;
						end
					 default;
					endcase
				end
				
				G_Ack:
				begin
				count1=count1+1'b1;
					case (count1)
					 1 ://sda<=ack; // ack=0
					 begin
						 sda_en_in=1'b0;
						 sda_out<=1'b0;
		           end
					 3 :scl<=1'b1;
					 5 :scl<=1'b0;
					 6: begin
						 count1=6'b0;current_state<=DataR; 
						 multi_read_cnt<=multi_read_cnt+1'b1;
						end
					 default;
			   	endcase
				
				end
				
				
				G_NAck:
				begin
					count1=count1+1'b1;
					case (count1)
						1:  begin // sda<=nack; // nack=1
						     sda_en_in=1'b0;
						     sda_out<=1'b1;
		                end
						2 :scl<=1'b1;
						3 :scl<=1'b0;
						4 :begin 
								count1=6'b0;
								current_state<=Stop1;
							end
						default;
					
					endcase 
				end
				Stop1: 
				begin          //
					count1=count1+1'b1;
					case (count1)
						 1 :begin//sda<=0;
						 sda_en_in=1'b0;
		             sda_out<=1'b0;
						 end
						 3 :scl<=1'b1;
						 10 :begin//sda<=1;
						 sda_en_in=1'b0;
		             sda_out<=1'b1;
						 end
						 15 :begin 
							 count1=6'b0;
//							 if (repnum== (size>>1))
//							 begin
//							   IIC_END<=1'b1;
//								current_state<=Idle;
//								repnum <=0;
//							  end
//							 else
							 begin
					   		 if (op_mode==RO) current_state<=PrepareR;
								 else if (op_mode==FRLW) current_state<=I2cPrepare;
								      else current_state<=Prepare;
								 IIC_END<=1'b1;
							//	 current_state<=Prepare;
								end
							end
						 default;
					endcase
			  end 
			  Idle: current_state<=Prepare;//Idle;
			  default : current_state <= Idle;//Idle;
	 endcase 
	 end


endmodule
