`timescale 1ps/1ps

module periphery2 #(
  parameter COLS = 16,
  parameter HIGH_LOW_OUTS = 50
)(
  // I2C interface
  input             clk,
  input             SCLin,
  output            SCLen,
  output            SCLout,
  output            SCLds,
  output            SCLpe,
  output            SCLud,

  input             SDAin,
  output            SDAout,
  output            SDAen,
  output            SDAds,
  output            SDApe,
  output            SDAud,

  input             A0in,
  output            A0out,
  output            A0en,
  output            A0ds,
  output            A0pe,
  output            A0ud,

  input             A1in,
  output            A1out,
  output            A1en,
  output            A1ds,
  output            A1pe,
  output            A1ud,

  input             RSTNin,
  output            RSTNout,
  output            RSTNen,
  output            RSTNds,
  output            RSTNpe,
  output            RSTNud,

  // test output interface
  input             TOin,
  output            TOen,
  output            TOout,
  output            TOds,
  output            TOpe,
  output            TOud,

  // Static configuration pins
  // should be tie low or high (using HIGH/LOW signals)
  input [3:0]       chipID,
  input [3:0]       chipREV,
  input [6:2]       Ain,
  output [HIGH_LOW_OUTS-1:0] HIGH,
  output [HIGH_LOW_OUTS-1:0] LOW,

  // configuration registers (Read/Write)
  output [7:0] regOut00,
  input  [7:0] defVal00,
  output [7:0] regOut01,
  input  [7:0] defVal01,
  output [7:0] regOut02,
  input  [7:0] defVal02,
  output [7:0] regOut03,
  input  [7:0] defVal03,
  output [7:0] regOut04,
  input  [7:0] defVal04,
  output [7:0] regOut05,
  input  [7:0] defVal05,
  output [7:0] regOut06,
  input  [7:0] defVal06,
  output [7:0] regOut07,
  input  [7:0] defVal07,
  output [7:0] regOut08,
  input  [7:0] defVal08,
  output [7:0] regOut09,
  input  [7:0] defVal09,
  output [7:0] regOut0A,
  input  [7:0] defVal0A,
  output [7:0] regOut0B,
  input  [7:0] defVal0B,
  output [7:0] regOut0C,
  input  [7:0] defVal0C,
  output [7:0] regOut0D,
  input  [7:0] defVal0D,
  output [7:0] regOut0E,
  input  [7:0] defVal0E,
  output [7:0] regOut0F,
  input  [7:0] defVal0F,
  output [7:0] regOut10,
  input  [7:0] defVal10,
  output [7:0] regOut11,
  input  [7:0] defVal11,
  output [7:0] regOut12,
  input  [7:0] defVal12,
  output [7:0] regOut13,
  input  [7:0] defVal13,
  output [7:0] regOut14,
  input  [7:0] defVal14,
  output [7:0] regOut15,
  input  [7:0] defVal15,
  output [7:0] regOut16,
  input  [7:0] defVal16,
  output [7:0] regOut17,
  input  [7:0] defVal17,
  output [7:0] regOut18,
  input  [7:0] defVal18,
  output [7:0] regOut19,
  input  [7:0] defVal19,
  output [7:0] regOut1A,
  input  [7:0] defVal1A,
  output [7:0] regOut1B,
  input  [7:0] defVal1B,
  output [7:0] regOut1C,
  input  [7:0] defVal1C,
  output [7:0] regOut1D,
  input  [7:0] defVal1D,
  output [7:0] regOut1E,
  input  [7:0] defVal1E,
  output [7:0] regOut1F,
  input  [7:0] defVal1F,

  // status registers (Read only)
  input [7:0] regIn100,
  input [7:0] regIn101,
  input [7:0] regIn102,
  input [7:0] regIn103,
  input [7:0] regIn104,
  input [7:0] regIn105,
  input [7:0] regIn106,
  input [7:0] regIn107,
  input [7:0] regIn108,
  input [7:0] regIn109,
  input [7:0] regIn10A,
  input [7:0] regIn10B,
  input [7:0] regIn10C,
  input [7:0] regIn10D,
  input [7:0] regIn10E,
  input [7:0] regIn10F,
  
  // Matrix interface
  output            matrixBusRst,
  output            matrixBusWe,
  output            matrixBusRe,
  output            matrixBusClk,
  output [15:0]     matrixBusAddr,
  output [7:0]      matrixBusDataMosi,
  input [7:0]       matrixBusDataMiso,
  input             matrixBusTmrError
);

  //`include "/users/qsun/workarea/tsmc65nm/ETROC_PLL/digital_work/git1/etroc2/rtl/regMap.v"
  localparam [7:0] MAGIC_NUMBER = 8'b10010110;
  
  localparam [7:0] REG_SEU0 = 16'h0120;
  localparam [7:0] REG_SEU1 = 16'h0121;
  localparam [7:0] REG_SEU2 = 16'h0122;
  localparam [7:0] REG_SEU3 = 16'h0123;


  // tmrg default triplicate
  // tmrg do_not_triplicate SCLin   SCLout  SCLen   SCLds  SCLpe   SCLud
  // tmrg do_not_triplicate SDAin   SDAout  SDAen   SDAds  SDApe   SDAud
  // tmrg do_not_triplicate A0in    A0out   A0en    A0ds   A0pe    A0ud
  // tmrg do_not_triplicate A1in    A1out   A1en    A1ds   A1pe    A1ud
  // tmrg do_not_triplicate RSTNin  RSTNout RSTNen  RSTNds RSTNpe  RSTNud
  // tmrg do_not_triplicate TOin    TOout   TOen    TOds   TOpe    TOud
  // tmrg do_not_triplicate HIGH
  // tmrg do_not_triplicate LOW
  // tmrg do_not_triplicate chipID
  // tmrg do_not_triplicate chipREV
  // tmrg do_not_triplicate Ain

  // tmrg do_not_triplicate defVal00 defVal01 defVal02 defVal03 
  // tmrg do_not_triplicate defVal04 defVal05 defVal06 defVal07 
  // tmrg do_not_triplicate defVal08 defVal09 defVal0A defVal0B
  // tmrg do_not_triplicate defVal0C defVal0D defVal0E defVal0F
  // tmrg do_not_triplicate defVal10 defVal11 defVal12 defVal13
  // tmrg do_not_triplicate defVal14 defVal15 defVal16 defVal17
  // tmrg do_not_triplicate defVal18 defVal19 defVal1A defVal1B
  // tmrg do_not_triplicate defVal1C defVal1D defVal1E defVal1F


  // memory wires
  parameter MEMLEN_RW=32+4;
  parameter MEMLEN_ALL=512;
  wire [MEMLEN_RW*8-1:0] memoryValues;
  wire [MEMLEN_ALL*8-1:0] registerValues;

  // SEU error detection
  wire tmrError = 1'b0;

  // wishbone
  wire [15:0] wbAdr;
  wire [7:0]  wbDataM2S;
  wire        wbWe;
  wire [7:0]  wbDataS2MRegs;
  wire [7:0]  wbDataS2MMatrix;
  wire [7:0]  wbDataS2M = wbAdr[15] ? wbDataS2MMatrix : wbDataS2MRegs;

  // reset / power on reset
  wire por;         // power on reset
  wire porA = por;  // POR readback
  wire porB = por;
  wire porC = por;
  wire rstComb = por | !RSTNin;
  wire rst;         // main system reset
  wire rstVtd;

//  powerOnResetLong POR(.POR(por));
//
//  resetVoter PORV(.in(rstComb),.out(rstVtd));
//
//  glitchFilter rstGF(
//     .in(rstVtd),
//     .out(rst)
//  );
  
  assign rst =1'b0;

  // I2C signals
  wire [6:0] i2cAdrNoGlitches;
  wire i2cActive;
  wire i2cStart;
  wire i2cStop;
  wire driveSDA;
  wire SDAinNoGlitches;
  wire SCLinNoGlitches;


  assign SCLds =1'b0;
  assign SCLpe =1'b1;
  assign SCLud =1'b1; //pull up
  assign SDApe =1'b1;
  assign SDAud =1'b1; //pull up
  assign RSTNen =1'b0;
  assign RSTNout =1'b0;
  assign RSTNds =1'b0;
  assign RSTNpe =1'b1;
  assign RSTNud =1'b1; //pull up

//  glitchFilter SDAinGF(
//     .in(SDAin),
//     .out(SDAinNoGlitches)
//  );
  assign SDAinNoGlitches = SDAin;

//  glitchFilter SCLinGF(
//     .in(SCLin),
//     .out(SCLinNoGlitches)
//  );
  assign SCLinNoGlitches = SCLin;

  // clock generation / gating / clock masking
  //wire clk;
  wire clkEnableRegA;
  wire clkEnableRegB;
  wire clkEnableRegC;
  wire clkForceEnable;
  wire MBtmrError = 1'b0;
  wire errDetected = MBtmrError;
  wire busTmrErrorPresent;
  wire clkEnableGlithes = ( i2cActive | tmrError | busTmrErrorPresent | clkForceEnable | !SCLin | !SDAin) && !rst;
  wire clkEnable;
  wire clkMasked;
  wire gateA = clkEnableRegA;
  wire gateB = clkEnableRegB;
  wire gateC = clkEnableRegC;
  wire gate=gateA;
  reg gateReg;

//  clockGenerator RO(
//    .enable(clkEnable),
//    .clk(clk)
//  );
//
//  glitchFilter clkEnGF(
//     .in(clkEnableGlithes),
//     .out(clkEnable)
//  );

  // Normaly should be done with dedicated cell (like CKLNQD2)
  // but here we need async reset
  always @(negedge clk or posedge rst)
  begin
    if (rst)
      gateReg <= 1'b1;
    else
     gateReg <= gate;
  end
//  assign clkMasked=clk&gateReg;

  assign clkMasked=clk;


  i2cSlave I2C (
    .SDAout(SDAout),
    .SDAen(SDAen),
    .SDAin(SDAinNoGlitches),
    .SCLin(SCLinNoGlitches),
    .SCLout(SCLout),
    .SCLen(SCLen),
    .rst(rst),
    .clk(clkMasked),
    .i2cAddr(i2cAdrNoGlitches),
    .wbDataIn(wbDataS2M),
    .wbDataOut(wbDataM2S),
    .wbWe(wbWe),
    .wbAdr(wbAdr),
    .start(i2cStart),
    .stop(i2cStop),
    .active(i2cActive),
    .driveSDA(1'b1)
  );


  assign A0ds = 1'b1;
  assign A0pe = 1'b1;
  assign A0ud = 1'b0; // pull down
  assign A0en = 1'b0;
  assign A0out= 1'b0;

  assign A1ds = 1'b1;
  assign A1pe = 1'b1;
  assign A1ud = 1'b0; // pull down
  assign A1en = 1'b0;
  assign A1out= 1'b0;

//  glitchFilter AGF6(.in(Ain[6]), .out(i2cAdrNoGlitches[6]));
//  glitchFilter AGF5(.in(Ain[5]), .out(i2cAdrNoGlitches[5]));
//  glitchFilter AGF4(.in(Ain[4]), .out(i2cAdrNoGlitches[4]));
//  glitchFilter AGF3(.in(Ain[3]), .out(i2cAdrNoGlitches[3]));
//  glitchFilter AGF2(.in(Ain[2]), .out(i2cAdrNoGlitches[2]));
//  glitchFilter A1GF(.in(A1in),   .out(i2cAdrNoGlitches[1]));
//  glitchFilter A0GF(.in(A0in),   .out(i2cAdrNoGlitches[0]));
  assign i2cAdrNoGlitches[6]= Ain[6];
  assign i2cAdrNoGlitches[5]= Ain[5];
  assign i2cAdrNoGlitches[4]= Ain[4];
  assign i2cAdrNoGlitches[3]= Ain[3];
  assign i2cAdrNoGlitches[2]= Ain[2];
  assign i2cAdrNoGlitches[1]= A1in;
  assign i2cAdrNoGlitches[0]= A0in;

  wire [MEMLEN_RW*8-1:0] defVal;
  wire [MEMLEN_RW*8-1 : 0] regOut;

  registerFile #(.MEMLEN(MEMLEN_RW)) MB (
    .wbAdr(wbAdr),
    .wbDataIn(wbDataM2S),
    .wbWe(wbWe),
    .clk(clkMasked),
    .rst(rst),
    .defaultValue(defVal),
    .values(memoryValues)
  );

  wire [31:0] seuCounter;

  assign regOut00 = memoryValues[0*8 +: 8];
  assign regOut01 = memoryValues[1*8 +: 8];
  assign regOut02 = memoryValues[2*8 +: 8];
  assign regOut03 = memoryValues[3*8 +: 8];
  assign regOut04 = memoryValues[4*8 +: 8];
  assign regOut05 = memoryValues[5*8 +: 8];
  assign regOut06 = memoryValues[6*8 +: 8];
  assign regOut07 = memoryValues[7*8 +: 8];
  assign regOut08 = memoryValues[8*8 +: 8];
  assign regOut09 = memoryValues[9*8 +: 8];
  assign regOut0A = memoryValues[10*8 +: 8];
  assign regOut0B = memoryValues[11*8 +: 8];
  assign regOut0C = memoryValues[12*8 +: 8];
  assign regOut0D = memoryValues[13*8 +: 8];
  assign regOut0E = memoryValues[14*8 +: 8];
  assign regOut0F = memoryValues[15*8 +: 8];
  assign regOut10 = memoryValues[16*8 +: 8];
  assign regOut11 = memoryValues[17*8 +: 8];
  assign regOut12 = memoryValues[18*8 +: 8];
  assign regOut13 = memoryValues[19*8 +: 8];
  assign regOut14 = memoryValues[20*8 +: 8];
  assign regOut15 = memoryValues[21*8 +: 8];
  assign regOut16 = memoryValues[22*8 +: 8];
  assign regOut17 = memoryValues[23*8 +: 8];
  assign regOut18 = memoryValues[24*8 +: 8];
  assign regOut19 = memoryValues[25*8 +: 8];
  assign regOut1A = memoryValues[26*8 +: 8];
  assign regOut1B = memoryValues[27*8 +: 8];
  assign regOut1C = memoryValues[28*8 +: 8];
  assign regOut1D = memoryValues[29*8 +: 8];
  assign regOut1E = memoryValues[30*8 +: 8];
  assign regOut1F = memoryValues[31*8 +: 8];

  assign defVal[0*8 +: 8] = defVal00;
  assign defVal[1*8 +: 8] = defVal01;
  assign defVal[2*8 +: 8] = defVal02;
  assign defVal[3*8 +: 8] = defVal03;
  assign defVal[4*8 +: 8] = defVal04;
  assign defVal[5*8 +: 8] = defVal05;
  assign defVal[6*8 +: 8] = defVal06;
  assign defVal[7*8 +: 8] = defVal07;
  assign defVal[8*8 +: 8] = defVal08;
  assign defVal[9*8 +: 8] = defVal09;
  assign defVal[10*8 +: 8] = defVal0A;
  assign defVal[11*8 +: 8] = defVal0B;
  assign defVal[12*8 +: 8] = defVal0C;
  assign defVal[13*8 +: 8] = defVal0D;
  assign defVal[14*8 +: 8] = defVal0E;
  assign defVal[15*8 +: 8] = defVal0F;
  assign defVal[16*8 +: 8] = defVal10;
  assign defVal[17*8 +: 8] = defVal11;
  assign defVal[18*8 +: 8] = defVal12;
  assign defVal[19*8 +: 8] = defVal13;
  assign defVal[20*8 +: 8] = defVal14;
  assign defVal[21*8 +: 8] = defVal15;
  assign defVal[22*8 +: 8] = defVal16;
  assign defVal[23*8 +: 8] = defVal17;
  assign defVal[24*8 +: 8] = defVal18;
  assign defVal[25*8 +: 8] = defVal19;
  assign defVal[26*8 +: 8] = defVal1A;
  assign defVal[27*8 +: 8] = defVal1B;
  assign defVal[28*8 +: 8] = defVal1C;
  assign defVal[29*8 +: 8] = defVal1D;
  assign defVal[30*8 +: 8] = defVal1E;
  assign defVal[31*8 +: 8] = defVal1F;
  assign defVal[32*8 +: 8] = 8'b0;
  assign defVal[33*8 +: 8] = 8'b0;
  assign defVal[34*8 +: 8] = 8'b0;
  assign defVal[35*8 +: 8] = 8'b0;

  // assign values for read only registers
  assign registerValues[MEMLEN_RW*8-1 : 0] = memoryValues[MEMLEN_RW*8-1 : 0];  // [R/W] registers
  assign registerValues[36*8 +: (256-36)*8] = 'b0;

  assign registerValues[16'h100*8 +: 8] = regIn100;
  assign registerValues[16'h101*8 +: 8] = regIn101;
  assign registerValues[16'h102*8 +: 8] = regIn102;
  assign registerValues[16'h103*8 +: 8] = regIn103;
  assign registerValues[16'h104*8 +: 8] = regIn104;
  assign registerValues[16'h105*8 +: 8] = regIn105;
  assign registerValues[16'h106*8 +: 8] = regIn106;
  assign registerValues[16'h107*8 +: 8] = regIn107;
  assign registerValues[16'h108*8 +: 8] = regIn108;
  assign registerValues[16'h109*8 +: 8] = regIn109;
  assign registerValues[16'h10A*8 +: 8] = regIn10A;
  assign registerValues[16'h10B*8 +: 8] = regIn10B;
  assign registerValues[16'h10C*8 +: 8] = regIn10C;
  assign registerValues[16'h10D*8 +: 8] = regIn10D;
  assign registerValues[16'h10E*8 +: 8] = regIn10E;
  assign registerValues[16'h10F*8 +: 8] = regIn10F;
  assign registerValues[16'h120*8 +: 8] = seuCounter[0 +: 8];
  assign registerValues[16'h121*8 +: 8] = seuCounter[8 +: 8];
  assign registerValues[16'h122*8 +: 8] = seuCounter[16+: 8];
  assign registerValues[16'h123*8 +: 8] = seuCounter[24+: 8];
  assign registerValues[16'h124*8 +: (512-16'h124)*8] = 'b0;
  
  // data out multiplexing
  memoryOutputMux #(.MEMLEN(MEMLEN_ALL))  MOM (
    .wbAdr(wbAdr),
    .wbDataOut(wbDataS2MRegs),
    .values(registerValues)
  );

  wire        magicNumberOK;

  wire [7:0] reg20 = memoryValues[ 8'h20*8 +: 8 ];
  wire [7:0] reg21 = memoryValues[ 8'h21*8 +: 8 ];
  wire [7:0] reg22 = memoryValues[ 8'h22*8 +: 8 ];

  assign magicNumberOK = (reg20[7:0] == MAGIC_NUMBER);
  assign clkEnableRegA = reg21[0] | !magicNumberOK;
  assign clkEnableRegB = reg21[1] | !magicNumberOK;
  assign clkEnableRegC = reg21[2] | !magicNumberOK;
  assign clkForceEnable = reg21[3];
  assign SDAds = reg21[4];
  assign driveSDA = reg21[5];
  wire [4:0] testOutSelect = reg22[4:0];


  wire loadErrorCounter = (wbAdr == REG_SEU0 | wbAdr == REG_SEU1 | wbAdr == REG_SEU2 | wbAdr == REG_SEU3) & wbWe;


  memoryErrorCounter MEC (
    .rst(rst),
    .clk(clk),
    .errDetected(errDetected),
    .dataOut(seuCounter),
    .load(loadErrorCounter)
  );



  //tmrg do_not_triplicate TIELOW  TIEHIGH

//  genvar i;
//  generate
//  for (i=0;i<HIGH_LOW_OUTS;i=i+1)
//    begin : tie
//      TIEL TIELOW(.ZN(LOW[i]));
//      TIEH TIEHIGH(.Z(HIGH[i]));
//    end
//  endgenerate

  // Test Output section
  // (get access to triplicated signals)
  wire clkMaskedA=clkMasked;
  wire clkMaskedB=clkMasked;
  wire clkMaskedC=clkMasked;

  wire clkA=clk;
  wire clkB=clk;
  wire clkC=clk;

  wire clkEnableA=clkEnable;
  wire clkEnableB=clkEnable;
  wire clkEnableC=clkEnable;

  wire clkEnableGlithesA=clkEnableGlithes;
  wire clkEnableGlithesB=clkEnableGlithes;
  wire clkEnableGlithesC=clkEnableGlithes;

  wire errDetectedA=errDetected;
  wire errDetectedB=errDetected;
  wire errDetectedC=errDetected;

  wire sysError=tmrError;
  wire sysErrorA=sysError;
  wire sysErrorB=sysError;
  wire sysErrorC=sysError;

  // tmrg do_not_triplicate  i2cActiveV i2cStartV i2cStopV
  wire  i2cActiveV=i2cActive;
  wire  i2cStartV=i2cStart;
  wire  i2cStopV=i2cStop;
  
  //tmrg do_not_triplicate busTmrErrorPresentV
  wire busTmrErrorPresentV=busTmrErrorPresent;


  // tmrg do_not_triplicate testSignals
  wire [31:0] testSignals={
    busTmrErrorPresentV,
    i2cActiveV,
    i2cStartV,
    i2cStopV,
    clkEnableC,
    clkEnableB,
    clkEnableA,
    clkEnableGlithesC,
    clkEnableGlithesB,
    clkEnableGlithesA,
    errDetectedC,
    errDetectedB,
    errDetectedA,
    sysErrorC,
    sysErrorB,
    sysErrorA,
    clkMaskedC,
    clkMaskedB,
    clkMaskedA,
    clkC,
    clkB,
    clkA,
    1'b1,
    1'b0,
    1'b0
  };

//  testOutMux TOM(
//    .inputSignals(testSignals),
//    .testOutSelect(testOutSelect),
//    .magicNumberOK(magicNumberOK),
//    .testOutput(TOout),
//    .testOutputEn(TOen)
//  );
  assign TOds = 1'b1;
  assign TOpe = 1'b0;
  assign TOud =1'b0;


  // Matrix interface
//  busMatrixAdapter BMA(
//    .busAddr(matrixBusAddr),
//    .busClk(matrixBusClk),
//    .busDataMiso(matrixBusDataMiso),
//    .busDataMosi(matrixBusDataMosi),
//    .busRe(matrixBusRe),
//    .busRst(matrixBusRst),
//    .busTmrError(matrixBusTmrError),
//    .busWe(matrixBusWe),
//    .rst(rst),
//    .wbAdr(wbAdr),
//    .wbClk(clk),
//    .wbDataM2S(wbDataM2S),
//    .wbDataS2M(wbDataS2MMatrix),
//    .wbWe(wbWe),
//    .busTmrErrorPresent(busTmrErrorPresent)
//  );

endmodule
