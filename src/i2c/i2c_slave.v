//**********************************************************************
//
//            File: i2c_slave.v
//            Module:i2c_slave
//            
//            
//
// modify by bwdeng
//**********************************************************************
`timescale	1ns/1ps

`define ADDRESS 7'b1110010  // 7'h72 + -->last bit is R/W bit  lpGBT i2c chip address ,really test chip i2c address 
//`define REGADDRL 8'h4C
`define REGADDR 16'h4C01// because i2C master regaddress=[7:0] [15:8]
`define REGADDR1 16'h4D01//
`define REGADDR2 16'h4E01//
`define REGADDR3 16'h4F01//
`define REGADDR4 16'h5001//
`define REGADDR5 16'h5101//
`define REGADDR6 16'h5201//
`define REGADDR7 16'h5301//
`define REGADDR8 16'h5401//
`define REGADDR9 16'h5501//

module	i2c_slave(
		//reset_n,
		clock,
		sda_out,
		sda_in,
		scl,
		sda_en,
		data_reg0,   
		data_reg1,
		main_state,
		reg_addr_t_buf,
		dat_out_regs,
		dat_in_regs,
		addr_in_state,
		data_in_state,
		data_out_state,
		reg_addr_state,
		
		// for chipscope watching
		wea,
      addra,
      ena		
		);
		
input		clock;   
//input		reset_n;
input		sda_in;
input		scl;

output	[7:0]	data_reg0;  //to define eight register 
output	[7:0]	data_reg1;
output 		sda_en;
reg		sda_en;
output 		sda_out;
output	[3:0]	main_state; 

output [15:0] reg_addr_t_buf;
output [7:0] dat_out_regs;
//output [7:0] dat_reg_w;


output 	[2:0]	addr_in_state;
output 	[3:0]	data_in_state;
output 	[3:0]	data_out_state;
output 	[4:0]	reg_addr_state;

// for chipscope watching
output  [7:0] dat_in_regs;
output  wea;
output [8:0]addra;
output  ena;



reg 		reset_n1=1'b1;
reg 		reset_n2=1'b1;
reg 		scl_regi0; 
reg 		scl_regi;
reg 		sda_regi0;
reg 		sda_regi;
reg		start_bus_reg;
reg		stop_bus_reg; 
 
reg	[7:0] 	data_reg0;  
reg	[7:0]	   data_reg1; 
 
reg	[6:0]	addr_in_reg;// chip address
//reg	[7:0]	data_in_reg0;  
//reg [7:0] data_in_reg0_buf;// for lpGBT or PO3100k read , to buf data_in_reg0
reg [7:0] data_in_reg1_buf;
reg [7:0] data_in_reg2_buf;
reg [7:0] data_in_reg3_buf;
reg [7:0] data_in_reg4_buf;
reg [7:0] data_in_reg5_buf;
reg [7:0] data_in_reg6_buf;
reg [7:0] data_in_reg7_buf;
reg [7:0] data_in_reg8_buf;
reg [7:0] data_in_reg9_buf;

reg data_in_reg_bufFlag;

reg	[7:0]	data_in_reg1; 
reg	[7:0]	data_in_reg2;  
reg	[7:0]	data_in_reg3;  
reg	[7:0]	data_in_reg4;  
reg	[7:0]	data_in_reg5;  
reg	[7:0]	data_in_reg6;  
reg	[7:0]	data_in_reg7; 
reg	[7:0]	data_in_reg8;  
reg	[7:0]	data_in_reg9;   

//reg	[7:0] 	data_in_reg1;
reg	[15:0] 	reg_addr;
reg	[3:0]	main_state; 
reg	[2:0]	addr_in_state;
reg	[3:0]	data_in_state;
reg	[3:0]	data_out_state;
reg	[4:0]	reg_addr_state;

reg		sda_out1;		// ACK
reg 		sda_out2;		// data_to_master
 
reg		write_read;
reg	[1:0]	ack_state; 
 
reg		flag;

reg [8:0]reg_addr_inc;
reg [15:0] reg_addr_local;
reg [15:0] reg_addr_t;
(* keep="true" *)reg [15:0] reg_addr_t_buf;
reg first_in_data_write;// first entrance into data_write operation.this flag is used to set reg_addr_inc=0
reg first_in_data_read;// first entrance into data_read operation.this flag is used to set reg_addr_inc=0


 
assign sda_out = flag ? sda_out2 : sda_out1;

//////////r2c_reset///////////////////////////////////////////////
  reg i2c_reset_r=1;
	reg i2c_reset_d=1;
	wire reset_n;
	always @(posedge clock)
	begin
	  if (i2c_reset_r==1)
	  begin
	     i2c_reset_r<=1'b0;
	  end
	  i2c_reset_d<=i2c_reset_r;
	end
	assign reset_n= ~(i2c_reset_r ^i2c_reset_d);

/////////////////////RAM for data_in_reg////////////////////
reg [7:0]dat_reg_buf;
reg [7:0]dat_reg_b;

wire wren;
wire rden;
(* keep="true" *)wire [7:0] dat_in_regs;
(* keep="true" *)wire [7:0] dat_out_regs;
(* keep="true" *)wire [8:0] wraddr;
(* keep="true" *)wire [8:0] rdaddr;

           
reg wren_r=0;// i2c slave role to watch, the control signal of "i2c slave read sda to write ram"  wren_r=1'b1;
reg rden_r=0;// i2c slave role to watch, the control signal of "i2c slave read sda to write ram"   rden_r=1'b0;									
reg [7:0] dat_reg;
reg [8:0] addr_r;//the address of "i2c slave read sda line to write data to ram" 

reg wren_w=0;// i2c slave role to watch, the control signal of "i2c slave read ram to write to sda line"  wren_r=1'b0;
reg rden_w=0;// i2c slave role to watch, the control signal of "i2c slave read ram to write to sda line"  rden_r=1'b1;
//(* keep="true" *) reg [7:0] dat_reg_w;// i2c slave role to watch, i2c salve write sda line to send data to i2c master
(* keep="true" *)reg [8:0] addr_w;  // the address of "i2c slave read ram data to write data to sda" 
//reg wFlag=0;

assign wren= wren_r^wren_w;// ensure only write
assign rden= rden_r^rden_w;// ensure only read
assign dat_in_regs=dat_reg;
assign wraddr=addr_r;
assign rdaddr=addr_w;

					       
reg_mem	reg_mem_inst (
	.clock ( clock ),
	.data ( dat_in_regs ),
	.rdaddress ( rdaddr ),
	.rden ( rden),
	.wraddress ( wraddr ),
	.wren ( wren ),
	.q ( dat_out_regs )
	);
 
wire wea;
assign wea= wren | (~rden);
wire [8:0]addra;
assign addra=wea?wraddr:rdaddr;
wire ena;
assign ena= wren ^ rden;

// reg_mem reg_mem_inst(
//  .clka(clock),
//  .ena(ena),
//  .wea(wea),
//  .addra(addra),
//  .dina(dat_in_regs),
//  .douta(dat_out_regs)
//);

// ----------------------------------------------------------------
// reset_n, scl, sda_in -> two stages registered 
always@(posedge clock)
begin
	reset_n1 <= reset_n;
	reset_n2 <= reset_n1; 
end
  
always@(posedge clock or negedge reset_n2)
begin
      if(!reset_n2)
	begin
             scl_regi  <= 1'b0;
             sda_regi  <= 1'b0;
             scl_regi0 <= 1'b0;
             sda_regi0 <= 1'b0;
	end
      else
	begin
             scl_regi0 <= scl_regi;
             scl_regi  <= scl;
             sda_regi0 <= sda_regi;
             sda_regi  <= sda_in;
	end
end

// ----------------------------------------------------------------
// to test start condition: scl=1, sda_in=100

always@(posedge clock or negedge reset_n2)
 begin
  if(!reset_n2)
     start_bus_reg <= 1'b0;
  else
     begin
       if({sda_regi0,sda_regi,sda_in}==3'b100 && {scl_regi0,scl_regi,scl}==3'b111)
            start_bus_reg <= 1'b1;
       else
            start_bus_reg <= 1'b0;
     end
 end
 
// ----------------------------------------------------------------
// to test stop condition: scl=1, sda_in=011

always@(posedge clock or negedge reset_n2)
 begin
  if(!reset_n2)
     stop_bus_reg <= 1'b0;
  else
     begin
       if({sda_regi0,sda_regi,sda_in}==3'b011 && {scl_regi0,scl_regi,scl}==3'b111)
            stop_bus_reg <= 1'b1;
       else
            stop_bus_reg <= 1'b0;
     end
 end
 
//----------------- addr in statemachine -------------------------------
 
parameter addr_in6   		= 3'h0;			// chip_id
parameter addr_in5   		= 3'h1;
parameter addr_in4   		= 3'h2;
parameter addr_in3   		= 3'h3;
parameter addr_in2   		= 3'h4;
parameter addr_in1   		= 3'h5;
parameter addr_in0   		= 3'h6;
parameter addr_end   		= 3'h7;

//----------------- reg addr in statemachine ----------------------------
// 
parameter reg_addr15         =4'h0;
parameter reg_addr14         =4'h1;
parameter reg_addr13         =4'h2;
parameter reg_addr12         =4'h3;
parameter reg_addr11         =4'h4;
parameter reg_addr10         =4'h5;
parameter reg_addr9          =4'h6;
parameter reg_addr8          =4'h7;
parameter reg_addr7          =4'h8;
parameter reg_addr6          =4'h9;
parameter reg_addr5          =4'ha;
parameter reg_addr4          =4'hb;
parameter reg_addr3          =4'hc;
parameter reg_addr2          =4'hd;
parameter reg_addr1          =4'he;
parameter reg_addr0          =4'hf;
parameter reg_addr_end       =5'h10;
       
//----------------- data in statemachine -------------------------------

parameter   data_in7   		= 4'h0;
parameter   data_in6   		= 4'h1;
parameter   data_in5   		= 4'h2;
parameter   data_in4   		= 4'h3;
parameter   data_in3   		= 4'h4;
parameter   data_in2   		= 4'h5;
parameter   data_in1   		= 4'h6;
parameter   data_in0   		= 4'h7;
parameter   data_end   		= 4'h8;

//----------------- data out statemachine -------------------------------
 parameter   data_out7   		= 4'h0;
 parameter   data_out6   		= 4'h1;
 parameter   data_out5   		= 4'h2;
 parameter   data_out4   		= 4'h3;
 parameter   data_out3   		= 4'h4;
 parameter   data_out2   		= 4'h5;
 parameter   data_out1   		= 4'h6;
 parameter   data_out0   		= 4'h7;
 parameter   data_out_end  = 4'h8; 

//----------------- main statemachine ------------------------------
parameter idle                       =4'h0;
parameter addr_read                  =4'h1;
parameter write_read_flag            =4'h2;
parameter addr_ack                   =4'h3;
parameter data_write	                =4'h4;
parameter data_in_ack                =4'h5;			 	 
parameter data_read                  =4'h6;
parameter data_out_ack               =4'h7;
parameter reg_addr_read              =4'h8;
parameter reg_addr_ack               =4'h9;
parameter if_rep_start               =4'ha; 
/////add start_bus state for read mode Sr signal for PO3100K I2c MASTER read single mode frame 
parameter if_sr                      =4'hb;


reg NEEDRESTOREFLAG=1'b0;

//------------------------------------------------------------------	
//main state machine

always @(posedge clock or negedge reset_n2) 
	if(!reset_n2)
	begin
		main_state <= idle;
		write_read <= 1'b0;
		data_in_reg_bufFlag<=1'b0;
		reg_addr_inc<=0;
		first_in_data_write<=1'b1;
		first_in_data_read<=1'b1;
		
      NEEDRESTOREFLAG<=1'b0;

	end
	else
	begin
		case (main_state)	
		idle:
		begin
	   	first_in_data_write<=1'b1;
			first_in_data_read<=1'b1;
			write_read<=1'b0;		    
			if(start_bus_reg)	// receive start from SDA
			begin
				main_state	<= addr_read;							 
			end
			else					 
			begin
				main_state	<= idle;						     
			end									     					  
		end
						
		addr_read:	// read chip_id from the master
		begin	
			
		
			if(addr_in_state==addr_end)
				main_state	 <= write_read_flag;
			else					        
				main_state	 <= addr_read;
		end	
				
		write_read_flag:	// read R/W flag following chip_id 			         
		begin
		   if (data_in_reg_bufFlag) data_in_reg_bufFlag<=1'b0;
			if({scl_regi0,scl_regi,scl}==3'b011)
			begin
				write_read <= sda_in;   	                                                      
				main_state <= addr_ack;
			end	
			else
				main_state <= write_read_flag;			 
		end
				
		addr_ack:	// send chip_id_ack to the master
		begin	
			if({scl_regi0,scl_regi,scl}==3'b011) 
			begin
				if(addr_in_reg==`ADDRESS)//7'b1100110)
				begin
				if (write_read)//  master dengbw20200326
				begin
				if (first_in_data_read) begin reg_addr_inc<=9'b0;		
															 first_in_data_read<=1'b0;
															 end
				   main_state<=data_read;
				end
				else
				  	main_state <= reg_addr_read;
				end
				else                  
					main_state <= idle; 
			end
			else
				main_state <= addr_ack;   				       				     
		end	
		reg_addr_read:	// read register address form master
		begin
		   first_in_data_write<=1'b1;
			first_in_data_read<=1'b1;
			
			if(reg_addr_state==reg_addr_end)
				main_state <= reg_addr_ack;
			else                  
				main_state <= reg_addr_read;
		end
		reg_addr_ack:	// send reg_addr_ack to master
		begin
			if({scl_regi0,scl_regi,scl}==3'b011)	
			begin
			   //add start Sr
				main_state<=if_sr;

			end
			else
				main_state <= reg_addr_ack;	
		end	

		if_sr:// read sr from master dengbw 20200326
		begin
		// to test start condition: scl=1, sda_in=100 
		if({sda_regi0,sda_regi,sda_in}==3'b100 && {scl_regi0,scl_regi,scl}==3'b111)
			begin
			   main_state	<= addr_read;
			end
			else
			begin
				    
				  if(sda_out1)	
						main_state <= idle;
					else
					begin
						if(write_read)	 // '1': read	
						begin
							if (first_in_data_read) begin reg_addr_inc<=9'b0;		
															 first_in_data_read<=1'b0;
															 end
							main_state <= data_read;		
						end
						else		// '0': write
						 begin
							main_state <= data_write;
							if (first_in_data_write) begin reg_addr_inc<=9'b0;		
															 first_in_data_write<=1'b0;
															 end
							NEEDRESTOREFLAG<=1'b1;
							reg_addr_t_buf<=reg_addr_t;
							dat_reg_buf<=dat_reg_b;//buffer ram dat_reg for restoring data
    						//data_in_reg0_buf<=data_in_reg0;
							//data_in_reg1_buf<=data_in_reg1;
							//data_in_reg2_buf<=data_in_reg2;
							//data_in_reg3_buf<=data_in_reg3;
							//data_in_reg4_buf<=data_in_reg4;
							//data_in_reg5_buf<=data_in_reg5;
							//data_in_reg6_buf<=data_in_reg6;
							//data_in_reg7_buf<=data_in_reg7;
							//data_in_reg8_buf<=data_in_reg8;
							//data_in_reg9_buf<=data_in_reg9;
						 end
					end							
			end
		end
		
		data_write:	// read data from master			
		begin		
		
		// to test start condition: scl=1, sda_in=100 
      if({sda_regi0,sda_regi,sda_in}==3'b100 && {scl_regi0,scl_regi,scl}==3'b111)
			begin
			   main_state	<= addr_read;
			//	data_in_reg0<= data_in_reg0_buf;
			// for synthesis 
			   if (NEEDRESTOREFLAG)
				begin
				  NEEDRESTOREFLAG<=1'b0;
			     data_in_reg_bufFlag<=1'b1;
				 end
				 else data_in_reg_bufFlag<=1'b0;
			end
        else
      		 //  to test stop condition 
			if({sda_regi0,sda_regi,sda_in}==3'b011 && {scl_regi0,scl_regi,scl}==3'b111) 
			begin
				//reg_addr_inc<=9'b0;
				NEEDRESTOREFLAG<=1'b0;
				main_state <= if_rep_start;
			end
			else
			begin
			   NEEDRESTOREFLAG<=1'b0;
				if(data_in_state == data_end)	
	         begin	
		         
					main_state <= data_in_ack;
				end
				else
					main_state <= data_write;				
			end		
	end
						
		data_in_ack:	// write data_in_ack to master		 
		begin	
			if({scl_regi0,scl_regi,scl}==3'b011)					
				//main_state <= if_rep_start;
				begin
					main_state<=data_write;// for multi_write
					reg_addr_inc<=reg_addr_inc+1'b1;// inc register address value
				end
			else                  
				main_state <= data_in_ack;			
		end	
								 
		data_read:	// write data to master
		begin
			if(data_out_state==data_out_end && {scl_regi0,scl_regi,scl}==3'b100)		              
			begin
				main_state <= data_out_ack;		             
			end                   
			else                  
			begin                 
				main_state <= data_read;			              
			end	
		end
			
		data_out_ack:	// write data_out_ack to master
		begin			             
//			if({scl_regi0,scl_regi,scl}==3'b011)
//				main_state <= if_rep_start;
//			else                  
//				main_state <= data_out_ack;
			// receive ack 20200401
         if({sda_regi0,sda_regi,sda_in}==3'b000 && {scl_regi0,scl_regi,scl}==3'b011) 
			begin
			   reg_addr_inc<=reg_addr_inc+1'b1;// inc register address value
				main_state<= data_read;
			end
			else if({sda_regi0,sda_regi,sda_in}==3'b111 && {scl_regi0,scl_regi,scl}==3'b011) // receive nack
			begin
			  // reg_addr_inc<=9'b0;
			   main_state<= if_rep_start;
			end
			else 
			   main_state<= data_out_ack;
		end
			 
		if_rep_start:	// read restart from master
		begin
			if(stop_bus_reg)
				main_state <= idle;
			else if(start_bus_reg)
				main_state <= reg_addr_read;
			else                  
				main_state <= if_rep_start;			 
		end   
		                        
		default:	main_state <= idle;
		endcase 						 
	end 

// register address auto inc 20200331

always  @(posedge clock or negedge reset_n2) //addr ack output
begin
if(!reset_n2)
	begin 
		reg_addr_local<=16'b0;
	end
	else
	begin
	   reg_addr_t<={reg_addr[7:0],reg_addr[15:8]}+reg_addr_inc;
		reg_addr_local<={reg_addr_t[7:0],reg_addr_t[15:8]}; 
	end
end
//------------------------------------------------------------------			
// send chip_id_ack to master           
always @(posedge clock or negedge reset_n2) //addr ack output
begin
	if(!reset_n2)
	begin 
		ack_state <= 2'b00;
		sda_en    <= 1'b0;
		flag      <= 1'b0;
		sda_out1  <= 1'b0; 
	end
	else
	begin
		case(ack_state)
		2'b00:
		begin
			if(main_state==addr_ack && {scl_regi0,scl_regi,scl}==3'b100)    //to ack chip address           
			begin 
				if(addr_in_reg==`ADDRESS)//7'b1100110)   
					sda_out1 <= 1'b0;
				else
					sda_out1 <= 1'b1; 
					 
            flag      <= 1'b0;    //??1                              
				sda_en    <= 1'b1;
				ack_state <= 2'b11;
			end
			else if(main_state==reg_addr_ack && {scl_regi0,scl_regi,scl}==3'b100)// to ack register address
			begin
//				case(reg_addr_local)
//				`REGADDR:	sda_out1 <= 1'b0;// lpGBT
//				`REGADDR1:	sda_out1 <= 1'b0;  
//				`REGADDR2:	sda_out1 <= 1'b0; 
//				`REGADDR3:	sda_out1 <= 1'b0; 
//				`REGADDR4:	sda_out1 <= 1'b0; 
//				`REGADDR5:	sda_out1 <= 1'b0; 
//				`REGADDR6:	sda_out1 <= 1'b0; 
//				`REGADDR7:	sda_out1 <= 1'b0; 
//				`REGADDR8:	sda_out1 <= 1'b0; 
//				`REGADDR9:	sda_out1 <= 1'b0; 
//				default:sda_out1 <= 1'b1;
//				endcase
				if (reg_addr_t<=9'h1ce)// lpGBT銆€i2c register address: 0 to 462 
				begin
					sda_out1<=1'b0;
				end
				else
				begin
				   sda_out1<=1'b1;
				end
				
			   flag      <= 1'b0;    //??1
				sda_en    <= 1'b1;
				ack_state <= 2'b11;  
			end 					 
			else if(main_state==data_in_ack && {scl_regi0,scl_regi,scl}==3'b100)
			begin
				flag      <= 1'b0;    //??1
				sda_out1  <= 1'b0;    //??2
				sda_en    <= 1'b1;
				ack_state <= 2'b01;
			end
			else if(main_state==data_read && {scl_regi0,scl_regi,scl}==3'b100)
			begin
				flag      <= 1'b1;
				sda_en    <= 1'b1;
				ack_state <= 2'b10;	//?master??????ack???
			end
			else
				sda_en<=1'b0;
			end
		2'b01:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100)
			begin
				sda_en    <= 1'b0;
				ack_state <= 2'b00;
			end
			else
				ack_state <= 2'b01; 
		end
		2'b10:
		begin
			if(main_state==data_read)
				ack_state <= 2'b10;
			else
			begin 
				ack_state <= 2'b00;
				sda_en    <= 1'b0;  
				flag      <= 1'b0;
			end
		end
		
		2'b11:
		begin
			if(main_state==data_read && {scl_regi0,scl_regi,scl}==3'b100)
			begin
				flag      <= 1'b1;
				sda_en    <= 1'b1;
				ack_state <= 2'b10;
			end
			else if(main_state!=data_read && {scl_regi0,scl_regi,scl}==3'b100)
			begin 
				ack_state <= 2'b00;
				sda_en    <= 1'b0;  
			end
			else
				ack_state <= 2'b11;
		end  
		default:	ack_state <= 2'b00;         
		endcase				 
	end
 end

//------------------------------------------------------------------	
//to read Chip_id from master

always @(posedge clock or negedge reset_n2)//to write chip address
	if(!reset_n2)
	begin 
		addr_in_state <= addr_in6;
		addr_in_reg   <= 7'b0000000;
	end
	else if(main_state==addr_read)
	begin
		case(addr_in_state)	
		addr_in6:
		begin
			if({scl_regi0,scl_regi,scl}==3'b011)
			begin
				addr_in_state  <= addr_in5;
				addr_in_reg[6] <= sda_in;
			end
			else
				addr_in_state  <= addr_in6;
		end
			        
		addr_in5:					 
		begin
			if({scl_regi0,scl_regi,scl}==3'b011)
			begin
				addr_in_state  <= addr_in4;
				addr_in_reg[5] <= sda_in;
			end
			else
				addr_in_state  <= addr_in5;
		end				
		addr_in4:
		begin
			if({scl_regi0,scl_regi,scl}==3'b011)
			begin
				addr_in_state  <= addr_in3;
				addr_in_reg[4] <= sda_in;
			end
			else
				addr_in_state  <= addr_in4;
		end				
		addr_in3:
		begin
			if({scl_regi0,scl_regi,scl}==3'b011)
			begin
				addr_in_state  <= addr_in2;
				addr_in_reg[3] <= sda_in;
			end
			else
				addr_in_state  <= addr_in3;
		end			
		addr_in2:
		begin
			if({scl_regi0,scl_regi,scl}==3'b011)
			begin
				addr_in_state  <= addr_in1;
				addr_in_reg[2] <= sda_in;
			end
			else
				addr_in_state  <= addr_in2;
		end				
		addr_in1:
		begin
			if({scl_regi0,scl_regi,scl}==3'b011)
			begin
				addr_in_state  <= addr_in0;
				addr_in_reg[1] <= sda_in;
			end
			else
				addr_in_state  <= addr_in1;
		end				
		addr_in0:
		begin
			if({scl_regi0,scl_regi,scl}==3'b011)
			begin
				addr_in_state  <= addr_end;
				addr_in_reg[0] <= sda_in;
			end
			else
				addr_in_state <= addr_in0;		    
		end
		addr_end:	addr_in_state <= addr_in6;
		default:	addr_in_state <= addr_in6;
		endcase
	end
	else
		addr_in_state  <= addr_in6;  

//------------------------------------------------------------------	
//to read data from master
 
always @(posedge clock or negedge reset_n2)
	if(!reset_n2)
	begin
		data_in_state <= data_in7;
     
		data_in_reg1  <= 8'b00001010;  //vgf
		//data_in_reg0  <= 8'b01000000;  //bdl
		
		data_reg1     <= 8'b00001010;  //vgf
		data_reg0     <= 8'b01000000;  //bdl
	end
	else
	begin
//	   wFlag<=1'b0;
//		if(wFlag) 
   	begin
		  wren_r<=1'b0;
		  rden_r<=1'b0;
		//  wFlag<=1'b0;
		end
		if(main_state==data_write)
			case(data_in_state)	
			data_in7:
			begin	 
				if({scl_regi0,scl_regi,scl}==3'b011)          
				begin	
//					case(reg_addr_local)
//					`REGADDR:
					
					begin	//data_in_reg0[7] <= sda_in; 
					         //   wren_r<=1'b1;
								//	rden_r<=1'b0;
								//	addr_r<=reg_addr_t[8:0];
									dat_reg_b[7]<=sda_in;//buffer ram dat_reg for restoring data
								//	dat_reg[7] <= sda_in;
					         end
//					`REGADDR1:	data_in_reg1[7] <= sda_in;
//					`REGADDR2:	data_in_reg2[7] <= sda_in;
//					`REGADDR3:	data_in_reg3[7] <= sda_in;
//					`REGADDR4:	data_in_reg4[7] <= sda_in;
//					`REGADDR5:	data_in_reg5[7] <= sda_in;
//					`REGADDR6:	data_in_reg6[7] <= sda_in;
//					`REGADDR7:	data_in_reg7[7] <= sda_in;
//					`REGADDR8:	data_in_reg8[7] <= sda_in;
//					`REGADDR9:	data_in_reg9[7] <= sda_in;
//					endcase								    
					data_in_state <= data_in6;                             
				end
				else
					data_in_state <= data_in7; 
			end	
			data_in6:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)
				begin					     
//					case(reg_addr_local)
//					`REGADDR:
					begin	//data_in_reg0[6] <= sda_in; 
					         //  wren_r<=1'b1;
								//	rden_r<=1'b0;
								//	addr_r<=reg_addr_t[8:0];
									dat_reg_b[6]<=sda_in;//buffer ram dat_reg for restoring data
									//dat_reg[6] <= sda_in;
					         end
//					`REGADDR1:	data_in_reg1[6] <= sda_in;
//					`REGADDR2:	data_in_reg2[6] <= sda_in;
//					`REGADDR3:	data_in_reg3[6] <= sda_in;
//					`REGADDR4:	data_in_reg4[6] <= sda_in;
//					`REGADDR5:	data_in_reg5[6] <= sda_in;
//					`REGADDR6:	data_in_reg6[6] <= sda_in;
//					`REGADDR7:	data_in_reg7[6] <= sda_in;
//					`REGADDR8:	data_in_reg8[6] <= sda_in;
//					`REGADDR9:	data_in_reg9[6] <= sda_in;
//					endcase			
					data_in_state <= data_in5;
				end
				else
					data_in_state <= data_in6; 
			end
			data_in5:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)
				begin					     
					data_in_state <= data_in4;
//					case(reg_addr_local)
//					`REGADDR:
					begin	//data_in_reg0[5] <= sda_in; 
					          // wren_r<=1'b1;
								//	rden_r<=1'b0;
								//	addr_r<=reg_addr_t[8:0];
									dat_reg_b[5]<=sda_in;//buffer ram dat_reg for restoring data
									//dat_reg[5] <= sda_in;
					         end
//					`REGADDR1:	data_in_reg1[5] <= sda_in;
//					`REGADDR2:	data_in_reg2[5] <= sda_in;
//					`REGADDR3:	data_in_reg3[5] <= sda_in;
//					`REGADDR4:	data_in_reg4[5] <= sda_in;
//					`REGADDR5:	data_in_reg5[5] <= sda_in;
//					`REGADDR6:	data_in_reg6[5] <= sda_in;
//					`REGADDR7:	data_in_reg7[5] <= sda_in;
//					`REGADDR8:	data_in_reg8[5] <= sda_in;
//					`REGADDR9:	data_in_reg9[5] <= sda_in;
//					endcase
				end
				else
					data_in_state <= data_in5;     			
			end	
						
			data_in4:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)  	
				begin				    
					data_in_state <= data_in3;
//					case(reg_addr_local)
//					`REGADDR:
					begin	//data_in_reg0[4] <= sda_in;
				             //  wren_r<=1'b1;
								//	rden_r<=1'b0;
								//	addr_r<=reg_addr_t[8:0];
									dat_reg_b[4]<=sda_in;//buffer ram dat_reg for restoring data
									//dat_reg[4] <= sda_in;
					         end	
//					`REGADDR1:	data_in_reg1[4] <= sda_in;
//					`REGADDR2:	data_in_reg2[4] <= sda_in;
//					`REGADDR3:	data_in_reg3[4] <= sda_in;
//					`REGADDR4:	data_in_reg4[4] <= sda_in;
//					`REGADDR5:	data_in_reg5[4] <= sda_in;
//					`REGADDR6:	data_in_reg6[4] <= sda_in;
//					`REGADDR7:	data_in_reg7[4] <= sda_in;
//					`REGADDR8:	data_in_reg8[4] <= sda_in;
//					`REGADDR9:	data_in_reg9[4] <= sda_in;
//					endcase	
				end	
				else
					data_in_state <= data_in4;    	
			end
					
			data_in3: 
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)  
				begin					    
					data_in_state <= data_in2;
//					case(reg_addr_local)
//					`REGADDR:
					begin	//data_in_reg0[3] <= sda_in; 
					         //  wren_r<=1'b1;
								//	rden_r<=1'b0;
								//	addr_r<=reg_addr_t[8:0];
									dat_reg_b[3]<=sda_in;//buffer ram dat_reg for restoring data
									//dat_reg[3] <= sda_in;
					         end
//					`REGADDR1:	data_in_reg1[3] <= sda_in;
//					`REGADDR2:	data_in_reg2[3] <= sda_in;
//					`REGADDR3:	data_in_reg3[3] <= sda_in;
//					`REGADDR4:	data_in_reg4[3] <= sda_in;
//					`REGADDR5:	data_in_reg5[3] <= sda_in;
//					`REGADDR6:	data_in_reg6[3] <= sda_in;
//					`REGADDR7:	data_in_reg7[3] <= sda_in;
//					`REGADDR8:	data_in_reg8[3] <= sda_in;
//					`REGADDR9:	data_in_reg9[3] <= sda_in;
//					endcase	
				end	
				else
					data_in_state <= data_in3;  	
			end
					
			data_in2:			 
			begin		
				if({scl_regi0,scl_regi,scl}==3'b011)  
				begin				  
//					case(reg_addr_local)
//					`REGADDR:
					begin	//data_in_reg0[2] <= sda_in; 
					       //    wren_r<=1'b1;
							//		rden_r<=1'b0;
							//		addr_r<=reg_addr_t[8:0];
									dat_reg_b[2]<=sda_in;//buffer ram dat_reg for restoring data
									//dat_reg[2] <= sda_in;
					         end
//					`REGADDR1:	data_in_reg1[2] <= sda_in;
//					`REGADDR2:	data_in_reg2[2] <= sda_in;
//					`REGADDR3:	data_in_reg3[2] <= sda_in;
//					`REGADDR4:	data_in_reg4[2] <= sda_in;
//					`REGADDR5:	data_in_reg5[2] <= sda_in;
//					`REGADDR6:	data_in_reg6[2] <= sda_in;
//					`REGADDR7:	data_in_reg7[2] <= sda_in;
//					`REGADDR8:	data_in_reg8[2] <= sda_in;
//					`REGADDR9:	data_in_reg9[2] <= sda_in;
//					endcase			
					data_in_state <= data_in1;
				end
				else
					data_in_state <= data_in2; 
			end
								
			data_in1:
			begin
				if({scl_regi0,scl_regi,scl}==3'b011)   
				begin
					data_in_state <= data_in0;
//					case(reg_addr_local)
//					`REGADDR:
					begin	//data_in_reg0[1] <= sda_in; 
					         //  wren_r<=1'b1;
								//	rden_r<=1'b0;
								//	addr_r<=reg_addr_t[8:0];
									dat_reg_b[1]<=sda_in;//buffer ram dat_reg for restoring data
									//dat_reg[1] <= sda_in;
					         end
//					`REGADDR1:	data_in_reg1[1] <= sda_in;
//					`REGADDR2:	data_in_reg2[1] <= sda_in;
//					`REGADDR3:	data_in_reg3[1] <= sda_in;
//					`REGADDR4:	data_in_reg4[1] <= sda_in;
//					`REGADDR5:	data_in_reg5[1] <= sda_in;
//					`REGADDR6:	data_in_reg6[1] <= sda_in;
//					`REGADDR7:	data_in_reg7[1] <= sda_in;
//					`REGADDR8:	data_in_reg8[1] <= sda_in;
//					`REGADDR9:	data_in_reg9[1] <= sda_in;
//					endcase
				end	
				else
					data_in_state <= data_in1;   		
			end
							
			data_in0:
			begin
				if({scl_regi0,scl_regi,scl}==3'b011) 
				begin
					data_in_state <= data_end;
//					case(reg_addr_local)
//					`REGADDR:
					begin	//data_in_reg0[0] <= sda_in; 
					          // wren_r<=1'b1;
									//rden_r<=1'b0;
								//	addr_r<=reg_addr_t[8:0];
									dat_reg_b[0]<=sda_in;//buffer ram dat_reg for restoring data
									//dat_reg[0] <= sda_in;
					         end
//					`REGADDR1:	data_in_reg1[0] <= sda_in;
//					`REGADDR2:	data_in_reg2[0] <= sda_in;
//					`REGADDR3:	data_in_reg3[0] <= sda_in;
//					`REGADDR4:	data_in_reg4[0] <= sda_in;
//					`REGADDR5:	data_in_reg5[0] <= sda_in;
//					`REGADDR6:	data_in_reg6[0] <= sda_in;
//					`REGADDR7:	data_in_reg7[0] <= sda_in;
//					`REGADDR8:	data_in_reg8[0] <= sda_in;
//					`REGADDR9:	data_in_reg9[0] <= sda_in;
//					endcase
				end	
				else
					data_in_state <= data_in0;   						    
			end 
					     
			data_end:
			begin
//				case(reg_addr_local)
//				`REGADDR:
				begin	//data_reg0 <= data_in_reg0; 
				               addr_r<=reg_addr_t[8:0];
				               dat_reg<=dat_reg_b;
				               wren_r<=1'b1;
									rden_r<=1'b0;
									
					         end
				//8'h55:	data_reg1 <= data_in_reg1;
			//	endcase
				data_in_state <= data_in7;
			end
			default: begin data_in_state <= data_in7;
			                  wren_r<=1'b0;
									rden_r<=1'b0;
									
					         end
			endcase
		else
		  begin
			data_in_state <= data_in7;     
			if (data_in_reg_bufFlag)
			begin
				//data_in_reg0<=data_in_reg0_buf;// beacuse main_state entry into Sr state, restore data_in_reg0 20200328
				wren_r<=1'b1;
				rden_r<=1'b0;
				addr_r[8:0]<=reg_addr_t_buf[8:0];
				dat_reg<=dat_reg_buf;
			//	wFlag<=1'b1;
				
//				data_in_reg1<=data_in_reg1_buf;
//				data_in_reg2<=data_in_reg2_buf;
//				data_in_reg3<=data_in_reg3_buf;
//				data_in_reg4<=data_in_reg4_buf;
//				data_in_reg5<=data_in_reg5_buf;
//				data_in_reg6<=data_in_reg6_buf;
//				data_in_reg7<=data_in_reg7_buf;
//				data_in_reg8<=data_in_reg8_buf;
//				data_in_reg9<=data_in_reg9_buf;
				
			end
		  end
	end

//------------------------------------------------------------------	
//to read register addr from master

always @(posedge clock or negedge reset_n2)
begin
	if(!reset_n2)
	begin
		reg_addr       <= 16'b0000_0000_0000_0000;
		reg_addr_state <= reg_addr15; 
	end
	else
	begin
		if(main_state==reg_addr_read)
			case(reg_addr_state)	
			reg_addr15:
			begin	 
				if({scl_regi0,scl_regi,scl}==3'b011)          
				begin	
					reg_addr[15]    <= sda_in;          			    
					reg_addr_state <= reg_addr14;                             
				end
				else
					reg_addr_state <= reg_addr15; 
			end	
			reg_addr14:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)
				begin					     
					reg_addr[14]    <= sda_in; 
					reg_addr_state <= reg_addr13;
				end
				else
					reg_addr_state <= reg_addr14; 
			end
			reg_addr13:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)
				begin	
					reg_addr[13]    <= sda_in;				     
					reg_addr_state <= reg_addr12;						                      
				end
				else
					reg_addr_state <= reg_addr13;     			
			end	
			reg_addr12:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)  	
				begin				    
					reg_addr_state <= reg_addr11;
					reg_addr[12]    <= sda_in;             
				end	
				else
					reg_addr_state <= reg_addr12;    	
			end
			reg_addr11: 
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)  
				begin					    
					reg_addr_state <= reg_addr10;
					reg_addr[11]    <= sda_in;          
				end	
				else
					reg_addr_state <= reg_addr11;  	
			end
			reg_addr10:			 
			begin		
				if({scl_regi0,scl_regi,scl}==3'b011)  
				begin				  
					reg_addr[10]    <= sda_in;           
					reg_addr_state <= reg_addr9;
				end
				else
					reg_addr_state <= reg_addr10; 
			end
			reg_addr9:
			begin
				if({scl_regi0,scl_regi,scl}==3'b011)   
				begin
					reg_addr_state <= reg_addr8;
					reg_addr[9]    <= sda_in;           
				end	
				else
					reg_addr_state <= reg_addr9;   		
			end
			reg_addr8:
			begin
				if({scl_regi0,scl_regi,scl}==3'b011) 
				begin
					reg_addr_state <= reg_addr7;
					reg_addr[8]    <= sda_in;      
				end	
				else
					reg_addr_state<= reg_addr8;   						    
			end 
			
			
			reg_addr7:
			begin	 
				if({scl_regi0,scl_regi,scl}==3'b011)          
				begin	
					reg_addr[7]    <= sda_in;          			    
					reg_addr_state <= reg_addr6;                             
				end
				else
					reg_addr_state <= reg_addr7; 
			end	
			reg_addr6:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)
				begin					     
					reg_addr[6]    <= sda_in; 
					reg_addr_state <= reg_addr5;
				end
				else
					reg_addr_state <= reg_addr6; 
			end
			reg_addr5:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)
				begin	
					reg_addr[5]    <= sda_in;				     
					reg_addr_state <= reg_addr4;						                      
				end
				else
					reg_addr_state <= reg_addr5;     			
			end	
			reg_addr4:
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)  	
				begin				    
					reg_addr_state <= reg_addr3;
					reg_addr[4]    <= sda_in;             
				end	
				else
					reg_addr_state <= reg_addr4;    	
			end
			reg_addr3: 
			begin	
				if({scl_regi0,scl_regi,scl}==3'b011)  
				begin					    
					reg_addr_state <= reg_addr2;
					reg_addr[3]    <= sda_in;          
				end	
				else
					reg_addr_state <= reg_addr3;  	
			end
			reg_addr2:			 
			begin		
				if({scl_regi0,scl_regi,scl}==3'b011)  
				begin				  
					reg_addr[2]    <= sda_in;           
					reg_addr_state <= reg_addr1;
				end
				else
					reg_addr_state <= reg_addr2; 
			end
			reg_addr1:
			begin
				if({scl_regi0,scl_regi,scl}==3'b011)   
				begin
					reg_addr_state <= reg_addr0;
					reg_addr[1]    <= sda_in;           
				end	
				else
					reg_addr_state <= reg_addr1;   		
			end
			reg_addr0:
			begin
				if({scl_regi0,scl_regi,scl}==3'b011) 
				begin
					reg_addr_state <= reg_addr_end;
					reg_addr[0]    <= sda_in;      
				end	
				else
					reg_addr_state<= reg_addr0;   						    
			end 
			reg_addr_end:
			begin
				reg_addr_state  <= reg_addr15;
			end                        
			default: reg_addr_state <= reg_addr15;
			endcase
		else
			reg_addr_state <= reg_addr15;     
	end
end

//---------------------to read data in task--------------------------------
 
always@(posedge clock or negedge reset_n2) //data read
	if(!reset_n2)
	begin
		data_out_state <= data_out7;
		sda_out2       <= 1'b0;   
	end
	else
	begin   
		case(data_out_state)
		data_out7:
		begin			                    
			if(main_state==data_read&&{scl_regi0,scl_regi,scl}==3'b100)
			begin		                          
//				case(reg_addr_local)
//				`REGADDR:
				begin	//sda_out2 <= data_in_reg0[7]; 
				            wren_w<=1'b0;
								rden_w<=1'b1;
								addr_w<=reg_addr_t[8:0];
								//dat_reg_w[7]<=dat_out_regs[7];
								sda_out2<=dat_out_regs[7];
							end
//									
//				`REGADDR1:	sda_out2 <= data_in_reg1[7]; 
//				`REGADDR2:	sda_out2 <= data_in_reg2[7]; 
//				`REGADDR3:	sda_out2 <= data_in_reg3[7]; 
//				`REGADDR4:	sda_out2 <= data_in_reg4[7]; 
//				`REGADDR5:	sda_out2 <= data_in_reg5[7]; 
//				`REGADDR6:	sda_out2 <= data_in_reg6[7]; 
//				`REGADDR7:	sda_out2 <= data_in_reg7[7]; 
//				`REGADDR8:	sda_out2 <= data_in_reg8[7]; 
//				`REGADDR9:	sda_out2 <= data_in_reg9[7]; 
//			//	8'h55:	sda_out2 <= data_in_reg1[7];
//				endcase             
				data_out_state   <= data_out6;					                         
			end                         
			else                        
			begin                       
				data_out_state   <= data_out7; 
			end  
		end 
		data_out6:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100)
			begin
				data_out_state   <= data_out5;
			                            
//				case(reg_addr_local)      
//				`REGADDR:
				begin	//sda_out2 <= data_in_reg0[6]; 
				            wren_w<=1'b0;
				 				rden_w<=1'b1;
								addr_w<=reg_addr_t[8:0];
								//dat_reg_w[6]<=dat_out_regs[6];
								sda_out2<=dat_out_regs[6];
							end
//				`REGADDR1:	sda_out2 <= data_in_reg1[6]; 
//				`REGADDR2:	sda_out2 <= data_in_reg2[6]; 
//				`REGADDR3:	sda_out2 <= data_in_reg3[6]; 
//				`REGADDR4:	sda_out2 <= data_in_reg4[6]; 
//				`REGADDR5:	sda_out2 <= data_in_reg5[6]; 
//				`REGADDR6:	sda_out2 <= data_in_reg6[6]; 
//				`REGADDR7:	sda_out2 <= data_in_reg7[6]; 
//				`REGADDR8:	sda_out2 <= data_in_reg8[6]; 
//				`REGADDR9:	sda_out2 <= data_in_reg9[6]; 
//				
//				endcase	            
			end                         
			else                        
				data_out_state   <= data_out6;		
		end
		data_out5:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100)
			begin
				data_out_state   <= data_out4;		                          
//				case(reg_addr_local)      
//				`REGADDR:
				begin	//sda_out2 <= data_in_reg0[5]; 
								wren_w<=1'b0;
								rden_w<=1'b1;
								addr_w<=reg_addr_t[8:0];
								//dat_reg_w[5]<=dat_out_regs[5];
								sda_out2<=dat_out_regs[5];
							end
//				`REGADDR1:	sda_out2 <= data_in_reg1[5]; 
//				`REGADDR2:	sda_out2 <= data_in_reg2[5]; 
//				`REGADDR3:	sda_out2 <= data_in_reg3[5]; 
//				`REGADDR4:	sda_out2 <= data_in_reg4[5]; 
//				`REGADDR5:	sda_out2 <= data_in_reg5[5]; 
//				`REGADDR6:	sda_out2 <= data_in_reg6[5]; 
//				`REGADDR7:	sda_out2 <= data_in_reg7[5]; 
//				`REGADDR8:	sda_out2 <= data_in_reg8[5]; 
//				`REGADDR9:	sda_out2 <= data_in_reg9[5]; 
//				endcase			 
			end                         
			else                        
				data_out_state   <= data_out5; 
		end
		data_out4:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100)
			begin
				data_out_state   <= data_out3;			                          
//				case(reg_addr_local)      
//				`REGADDR:
				begin	//sda_out2 <= data_in_reg0[4]; 
								wren_w<=1'b0;
								rden_w<=1'b1;
								addr_w<=reg_addr_t[8:0];
								//dat_reg_w[4]<=dat_out_regs[4];
								sda_out2<=dat_out_regs[4];
							end
//				`REGADDR1:	sda_out2 <= data_in_reg1[4]; 
//				`REGADDR2:	sda_out2 <= data_in_reg2[4]; 
//				`REGADDR3:	sda_out2 <= data_in_reg3[4]; 
//				`REGADDR4:	sda_out2 <= data_in_reg4[4]; 
//				`REGADDR5:	sda_out2 <= data_in_reg5[4]; 
//				`REGADDR6:	sda_out2 <= data_in_reg6[4]; 
//				`REGADDR7:	sda_out2 <= data_in_reg7[4]; 
//				`REGADDR8:	sda_out2 <= data_in_reg8[4]; 
//				`REGADDR9:	sda_out2 <= data_in_reg9[4]; 
//				endcase             
			end	                    
			else                        
				data_out_state   <= data_out4; 		
		end
		data_out3:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100)
			begin
				data_out_state   <= data_out2;		                          
//				case(reg_addr_local)      
//				`REGADDR:
				begin	//sda_out2 <= data_in_reg0[3]; 
								wren_w<=1'b0;
								rden_w<=1'b1;
								addr_w<=reg_addr_t[8:0];
								//dat_reg_w[3]<=dat_out_regs[3];
								sda_out2<=dat_out_regs[3];
							end
//				`REGADDR1:	sda_out2 <= data_in_reg1[3]; 
//				`REGADDR2:	sda_out2 <= data_in_reg2[3]; 
//				`REGADDR3:	sda_out2 <= data_in_reg3[3]; 
//				`REGADDR4:	sda_out2 <= data_in_reg4[3]; 
//				`REGADDR5:	sda_out2 <= data_in_reg5[3]; 
//				`REGADDR6:	sda_out2 <= data_in_reg6[3]; 
//				`REGADDR7:	sda_out2 <= data_in_reg7[3]; 
//				`REGADDR8:	sda_out2 <= data_in_reg8[3]; 
//				`REGADDR9:	sda_out2 <= data_in_reg9[3]; 
//				endcase             
			end	                    
			else                        
				data_out_state   <= data_out3; 		
		end
		data_out2:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100) 
			begin
				data_out_state   <= data_out1;			                          
//				case(reg_addr_local)
//				`REGADDR:
				begin	//sda_out2 <= data_in_reg0[2];
								wren_w<=1'b0;
								rden_w<=1'b1;
								addr_w<=reg_addr_t[8:0];
								//dat_reg_w[2]<=dat_out_regs[2];
								sda_out2<=dat_out_regs[2];
							end	
//				`REGADDR1:	sda_out2 <= data_in_reg1[2]; 
//				`REGADDR2:	sda_out2 <= data_in_reg2[2]; 
//				`REGADDR3:	sda_out2 <= data_in_reg3[2]; 
//				`REGADDR4:	sda_out2 <= data_in_reg4[2]; 
//				`REGADDR5:	sda_out2 <= data_in_reg5[2]; 
//				`REGADDR6:	sda_out2 <= data_in_reg6[2]; 
//				`REGADDR7:	sda_out2 <= data_in_reg7[2]; 
//				`REGADDR8:	sda_out2 <= data_in_reg8[2]; 
//				`REGADDR9:	sda_out2 <= data_in_reg9[2]; 				 
//				endcase             
			end                         
			else                        
				data_out_state   <= data_out2; 			
		end
		data_out1:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100)
			begin
				data_out_state   <= data_out0;		                          
//				case(reg_addr_local)
//				`REGADDR:
				begin	//sda_out2 <= data_in_reg0[1]; 
								wren_w<=1'b0;
								rden_w<=1'b1;
								addr_w<=reg_addr_t[8:0];
								//dat_reg_w[1]<=dat_out_regs[1];
								sda_out2<=dat_out_regs[1];
							end
//				`REGADDR1:	sda_out2 <= data_in_reg1[1]; 
//				`REGADDR2:	sda_out2 <= data_in_reg2[1]; 
//				`REGADDR3:	sda_out2 <= data_in_reg3[1]; 
//				`REGADDR4:	sda_out2 <= data_in_reg4[1]; 
//				`REGADDR5:	sda_out2 <= data_in_reg5[1]; 
//				`REGADDR6:	sda_out2 <= data_in_reg6[1]; 
//				`REGADDR7:	sda_out2 <= data_in_reg7[1]; 
//				`REGADDR8:	sda_out2 <= data_in_reg8[1]; 
//				`REGADDR9:	sda_out2 <= data_in_reg9[1]; 
//				endcase	
			end	
			else
				data_out_state   <=data_out1; 	
		end
		data_out0:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100)
			begin  
				data_out_state   <= data_out_end;
//				case(reg_addr_local)
//				`REGADDR:
				begin	//sda_out2 <= data_in_reg0[0]; 
								wren_w<=1'b0;
								rden_w<=1'b1;
								addr_w<=reg_addr_t[8:0];
								//dat_reg_w[0]<=dat_out_regs[0];
								sda_out2<=dat_out_regs[0];
							end
//				`REGADDR1:	sda_out2 <= data_in_reg1[0]; 
//				`REGADDR2:	sda_out2 <= data_in_reg2[0]; 
//				`REGADDR3:	sda_out2 <= data_in_reg3[0]; 
//				`REGADDR4:	sda_out2 <= data_in_reg4[0]; 
//				`REGADDR5:	sda_out2 <= data_in_reg5[0]; 
//				`REGADDR6:	sda_out2 <= data_in_reg6[0]; 
//				`REGADDR7:	sda_out2 <= data_in_reg7[0]; 
//				`REGADDR8:	sda_out2 <= data_in_reg8[0]; 
//				`REGADDR9:	sda_out2 <= data_in_reg9[0]; 
//				endcase
			end
			else
				data_out_state   <= data_out0;
			  			
		end
		data_out_end:
		begin
			if({scl_regi0,scl_regi,scl}==3'b100)
			  begin
				data_out_state <= data_out7;
				wren_w<=1'b0;
				rden_w<=1'b0;
			  end
			else                      
				data_out_state <= data_out_end; 
		end                               
			                          
		default:	data_out_state <= data_out7;
		endcase	     
	end
 
endmodule

