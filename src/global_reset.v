`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:39:23 10/22/2017 
// Design Name: 
// Module Name:    global_reset 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module global_reset(
	clk, rst
    );

input clk;
output rst;

reg rst_reg = 0;
reg rst_reg2 = 0;

always @(posedge clk)
begin
	if (!rst_reg)
		rst_reg <= 1;
	rst_reg2 <= rst_reg;
end

assign rst = rst_reg ^ rst_reg2;

endmodule
