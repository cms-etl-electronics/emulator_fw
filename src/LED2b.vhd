--
-- LED flash
-- 2021-12-07 created
--
library ieee;
use ieee.std_logic_1164.all;
-- use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity LED2b is
port (
	CK : in std_logic;
	RSTin : in std_logic;
	LED2 : out std_logic;
	led : out std_logic
);
end LED2b;

architecture rtl of LED2b is
	constant CK_FREQ : integer := 25000000;
	constant BLINK_FREQ : integer := 1;
	constant CNT_MAX : integer := CK_FREQ/BLINK_FREQ/2-1;
	signal cnt : std_logic_vector(27 downto 0);
	signal cnt2 : std_logic_vector(31 downto 0);
	signal blink : std_logic;
	signal LED2x : std_logic;
	signal LED2xx : std_logic;

begin
	process(CK)
	begin
		if rising_edge(CK) then
			if cnt > CNT_MAX then
				cnt <= (others => '0');
				blink <= not blink;
			else
				cnt <= cnt + 1;
			end if;
		end if;
	end process;
	led <= blink;
	
	process(CK)
	begin
		if rising_edge(CK) then
			cnt2 <= cnt2 + 1;
			if cnt2(25 downto 20) < cnt2(15 downto 10) then
				LED2x <= cnt2(26);
			else
				LED2x <= not cnt2(26);
			end if;
		end if;
	end process;
--	LED2 <= LED2x;
	LED2xx <= RSTin;
	LED2 <= LED2xx and LED2x;


end rtl;
