/**
 *  File  : clockGeneratorDelayElement.v
 *  Author: Szymon Kulis (CERN)
 **/


`timescale 1ns/1ns

module clockGeneratorDelayElement(clkIn,clkOut);
  // trmg default triplicate
  // tmrg majority_voter_cell majorityVoterTag
  parameter DelayCellsPerElement=4;

  input  clkIn;
  output clkOut;

  wire clkInVoted=clkIn;
  wire [DelayCellsPerElement:0] clkDelayed;

  assign clkDelayed[0] = clkInVoted;
  assign clkOut = clkDelayed[DelayCellsPerElement];

  genvar i;

  generate
    for (  i = 0 ;  i < DelayCellsPerElement; i = i + 1)
      begin: DEL
        DEL4WRAPPER delayCell( .I(clkDelayed[i]),
                               .Z(clkDelayed[i+1])
                             );
    end
  endgenerate
endmodule
