`timescale 1ps/1ps

module pixelAnalog #(
  parameter REGS=32
)(
  output [REGS*8-1:0] defaultPixelConfig,
  input [REGS*8-1:0] pixelConfig,
  output [8*8-1:0] pixelStatus,
  input [7:0] pixelID,
  input   HIGH,
  input    LOW
);
  assign pixelStatus = pixelConfig[63:0] ^ 64'hFFFF_FFFF_FFFF_FFFF;//32'hFFFFFFFF;
  assign defaultPixelConfig = 256'h0;//32'h0;
endmodule
