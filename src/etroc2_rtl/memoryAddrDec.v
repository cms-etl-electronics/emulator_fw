`timescale 1ps/1ps

module memoryAddrDec(
  address,
  enable
);
  parameter MEMLEN=24;
  input  [15:0] address;
  output reg [MEMLEN-1:0] enable;

  always @(address)
    begin
        enable = {MEMLEN{1'b0}};
        if (address<MEMLEN)
          enable[address]=1'b1;
    end
endmodule
