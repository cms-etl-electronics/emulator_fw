`timescale 1ps/1ps
/*
module TAG2Gates(
  input A,
  input B,
  output Z
);
  // tmrg do_not_touch
  wire az = A&Z;
  wire bz = B&Z;
  wire ab = A&B;
  assign Z = az|bz|ab;
endmodule

module glitchFilter(
  input in,
  output out
);
  // tmrg do_not_touch
  wire inDel0,inDel1,inDel2,inDel3;
  wire voted;

  DEL4WRAPPER DL0 (.I(in), .Z(inDel0));
  DEL4WRAPPER DL1 (.I(inDel0), .Z(inDel1));
  DEL4WRAPPER DL2 (.I(inDel1), .Z(inDel2));
  DEL4WRAPPER DL3 (.I(inDel2), .Z(inDel3));

  TAG2Gates voter(.A(in), .B(inDel3), .Z(out));

endmodule
*/

module glitchFilter(
  input in,
  output out
);
  // tmrg do_not_touch
  wire inDel0,inDel1,inDel2,inDel3;
  DEL4WRAPPER DL0 (.I(in), .Z(inDel0));
  DEL4WRAPPER DL1 (.I(inDel0), .Z(inDel1));
  DEL4WRAPPER DL2 (.I(inDel1), .Z(inDel2));
  DEL4WRAPPER DL3 (.I(inDel2), .Z(inDel3));
  AO222D4GF and_or_preserve(
   .A1(in),      .A2(inDel3),
   .B1(in),      .B2(out),
   .C1(inDel3),  .C2(out),
   .Z(out)
  );
endmodule
