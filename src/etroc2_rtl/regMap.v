  localparam [7:0] MAGIC_NUMBER = 8'b10010110;
  
  localparam [7:0] REG_SEU0 = 16'h0120;
  localparam [7:0] REG_SEU1 = 16'h0121;
  localparam [7:0] REG_SEU2 = 16'h0122;
  localparam [7:0] REG_SEU3 = 16'h0123;
