/**
 *  File  : DEL4WRAPPER.v
 *  Author: Szymon Kulis (CERN)
 **/

`timescale 1ns / 1ps
//`define SIM
module DEL4WRAPPER(
  input I,
  output Z
);
  // tmrg do_not_touch

`ifdef SIM
  assign #(2:3:4) Z = I;
`else
  DEL4 DL(.I(I), .Z(Z));
`endif


endmodule

