/** ****************************************************************************
 *  lpGBTX                                                                     *
 *  Copyright (C) 2011-2016 GBTX Team, CERN                                    *
 *                                                                             *
 *  This IP block is free for HEP experiments and other scientific research    *
 *  purposes. Commercial exploitation of a chip containing the IP is not       *
 *  permitted.  You can not redistribute the IP without written permission     *
 *  from the authors. Any modifications of the IP have to be communicated back *
 *  to the authors. The use of the IP should be acknowledged in publications,  *
 *  public presentations, user manual, and other documents.                    *
 *                                                                             *
 *  This IP is distributed in the hope that it will be useful, but WITHOUT ANY *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE.                                                  *
 *                                                                             *
 *******************************************************************************
 *
 *  file: testInOut.v
 *
 *  History:
 *  2017/01/31 Szymon Kulis    : Created
 *
 **/

`timescale 1ps/1ps


module testOutMux #(parameter SELBITS=5)
(
  // input signals
  input [31:0] inputSignals,

  // control signals
  input [SELBITS-1:0] testOutSelect,
  input magicNumberOK,

  // output signals
  output testOutput,
  output testOutputEn

);
  // tmrg default do_not_triplicate
  // tmrg triplicate testOutputEn magicNumberOK  testOutSelect0  testOutSelect1
  reg [31:0] gate;
  wire  [31:0] inputSignalsGated;

  integer j;
  always @(testOutSelect)
    begin
      gate=32'b0;
      for(j=0;j<32;j=j+1)
         begin
           if (testOutSelect==j) gate[j]=1'b1;
         end
    end

  genvar i;
  generate
    for (i=0;i<32;i=i+1)
      begin : gates
        CKAN2D2 inputGate_preserve   (.A1(inputSignals[i]),  .A2(gate[i]),  .Z(inputSignalsGated[i]) );
      end
  endgenerate

  wire testOutputRaw;
  testOutMuxChn FM (
      .inputSignals(inputSignalsGated),
      .testOutSelect(testOutSelect),
      .testOutput(testOutputRaw)
  );
  assign testOutput = testOutputRaw & magicNumberOK;
  assign testOutputEn = (|testOutSelect) & magicNumberOK;

endmodule
