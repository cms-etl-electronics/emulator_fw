`timescale 1ps/1ps

// this module was encapsulated only to keep cleaner schematics
module memoryOutputMux #(parameter MEMLEN=8) (
  wbAdr,
  wbDataOut,
  values
);

  input [15:0] wbAdr;     // address
  output[7:0] wbDataOut; // databus output
  input  [MEMLEN*8-1:0] values;

  // data out multiplexing
  assign  wbDataOut =  (wbAdr < MEMLEN) ? values[ 8*wbAdr +: 8] : 8'h00;

endmodule
