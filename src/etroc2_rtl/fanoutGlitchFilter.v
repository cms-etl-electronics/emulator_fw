`timescale 1ps/1ps

module fanoutGlitchFilter(in,outA,outB,outC);
  parameter WIDTH = 1;

  input  [(WIDTH-1):0] in;
  output [(WIDTH-1):0] outA;
  output [(WIDTH-1):0] outB;
  output [(WIDTH-1):0] outC;
  genvar k;

  for (k=0;k<WIDTH;k=k+1)
  begin
    glitchFilter AVA(.in(in[k]), .out(outA[k]));
    glitchFilter AVB(.in(in[k]), .out(outB[k]));
    glitchFilter AVC(.in(in[k]), .out(outC[k]));
  end
endmodule
