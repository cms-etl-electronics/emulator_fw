`timescale 1ps/1ps

module pixelColumn #(
  parameter ROWS=16
)(
  // bus/configuration input (comming from the previous column)
  input  [3:0]   columnAddrIn,
  input          busRstIn,
  input          busWeIn,
  input          busClkIn,
  input          busReIn,
  input [15:0]   busAddrIn,
  input [7:0]    busDataMosiIn,
  output [7:0]   busDataMisoOut,
  output         busTmrErrorOut,

  // bus/configuration output (going to the next column)
  output [3:0]   columnAddrNextOut,
  output         busRstOut,
  output         busWeOut,
  output         busClkOut,
  output         busReOut,
  output [15:0]  busAddrOut,
  output [7:0]   busDataMosiOut,
  input [7:0]    busDataMisoIn,
  input          busTmrErrorIn
);
  genvar r;
  wire [ROWS:0] busRst;
  wire [ROWS:0] busWe;
  wire [ROWS:0] busRe;
  wire [ROWS:0] busClk;
  wire [15:0] busAddr [ROWS:0];
  wire [7:0] busDataMosi [ROWS:0];
  wire [7:0] busDataMiso [ROWS:0];
  wire [3:0] colAddr [ROWS:0];
  wire [ROWS:0] busTmrError;
  wire [3:0] pixelAddr [ROWS:0];

  busColumnAdapter BCA (
    .busAddrIn(busAddrIn),
    .busAddrOut(busAddrOut),
    .busClkIn(busClkIn),
    .busClkOut(busClkOut),
    .busDataMisoIn(busDataMisoIn),
    .busDataMisoOut(busDataMisoOut),
    .busDataMosiIn(busDataMosiIn),
    .busDataMosiOut(busDataMosiOut),
    .busReIn(busReIn),
    .busReOut(busReOut),
    .busRstIn(busRstIn),
    .busRstOut(busRstOut),
    .busTmrErrorIn(busTmrErrorIn),
    .busTmrErrorOut(busTmrErrorOut),
    .busWeIn(busWeIn),
    .busWeOut(busWeOut),
    .colAddrOut(busAddr[0]),
    .colClkOut(busClk[0]),
    .colDataMisoIn(busDataMiso[0]),
    .colDataMosiOut(busDataMosi[0]),
    .colReOut(busRe[0]),
    .colRstOut(busRst[0]),
    .colTmrErrorIn(busTmrError[0]),
    .colWeOut(busWe[0]),
    .columnAddrIn(columnAddrIn),
    .columnAddrNextOut(columnAddrNextOut),
    .colID(colAddr[0])
  );

  generate
    for(r=0;r<ROWS;r=r+1)
      begin: row
        pixel PIX (
          .busAddrIn(busAddr[r]),
          .busAddrOut(busAddr[r+1]),
          .busDataMisoIn(busDataMiso[r+1]),
          .busDataMisoOut(busDataMiso[r]),
          .busDataMosiIn(busDataMosi[r]),
          .busDataMosiOut(busDataMosi[r+1]),
          .busReIn(busRe[r]),
          .busReOut(busRe[r+1]),
          .busRstIn(busRst[r]),
          .busRstOut(busRst[r+1]),
          .busTmrErrorIn(busTmrError[r+1]),
          .busTmrErrorOut(busTmrError[r]),
          .busWeIn(busWe[r]),
          .busWeOut(busWe[r+1]),
          .busClkIn(busClk[r]),
          .busClkOut(busClk[r+1]),
          .colAddrIn(colAddr[r]),
          .colAddrOut(colAddr[r+1]),
          .pixelAddrIn(pixelAddr[r]),
          .pixelAddrNextOut(pixelAddr[r+1])
        );
      end
  endgenerate
  assign pixelAddr[0] = 4'b0;
  assign busTmrError[ROWS] = 1'b0;
  assign busDataMiso[ROWS] = 8'b0;

endmodule
