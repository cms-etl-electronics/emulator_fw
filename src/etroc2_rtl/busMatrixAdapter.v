`timescale 1ps/1ps

module busMatrixAdapter
(
  // wb interface
  input             rst,
  input [15:0]      wbAdr,
  input [7:0]       wbDataM2S,
  output [7:0]      wbDataS2M,
  input             wbWe,
  input             wbClk,
  output            busTmrErrorPresent,

  // matrix interface
  output            busRst,
  output            busWe,
  output            busRe,
  output            busClk,
  output [15:0]     busAddr,
  output [7:0]      busDataMosi,
  input [7:0]       busDataMiso,
  input             busTmrError
);
  wire matrixSelected = wbAdr[15];
  assign busTmrErrorPresent = busTmrError;
  assign busRst = rst;
  assign busAddr = matrixSelected ? wbAdr : 16'b0;
  assign busDataMosi = matrixSelected ? wbDataM2S : 8'b0;
  assign wbDataS2M = matrixSelected ? busDataMiso : 8'b0;

  reg [1:0] wbWeDel;
  wire [1:0] wbWeDelNext = {wbWeDel[0], wbWe};
  wire [1:0] wbWeDelNextVoted = wbWeDelNext;

  always @(posedge wbClk or posedge rst)
    if (rst)
      wbWeDel <= #1 2'b00;
    else
      wbWeDel <= #1 wbWeDelNextVoted;

  wire wbWePulse = (wbWeDel == 2'b01);

  assign busWe = wbWe & matrixSelected;

  wire busClkEnable = busTmrError | wbWePulse & matrixSelected;

  CKLNQD2 CG_preserve(.CP(wbClk), .Q(busClk), .TE(1'b0), .E(busClkEnable));

  assign busRe = 1'b1;
endmodule
