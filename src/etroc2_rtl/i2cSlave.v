`timescale 1ps/1ps

module i2cSlave (
  // I2C IO pins
  input            SDAin,
  output           SDAout,
  output           SDAen,

  input            SCLin,
  output           SCLout,
  output           SCLen,

  // configuration
  input [6:0]      i2cAddr,
  input            driveSDA,

  // asynchronous reset
  input            rst,

  // clock
  input            clk,

  // wishbone signals
  output reg [15:0] wbAdr,
  output reg       wbWe,
  input [7:0]      wbDataIn,
  output reg [7:0] wbDataOut,

  // internal state indicaators
  output reg       active,  // high during I2C transaction,
  output           start,   // start condition detected
  output           stop     // stop condition detected
);
  // tmrg tmr_error true

  // connections to I2C pads
  assign SCLout = 1'b1;
  assign SCLen  = 1'b0;
  reg    SDAoutInt;
  wire   SDAoutIntVoted = SDAoutInt;
  reg    SDAenInt;
  wire   SDAenIntVoted  = SDAenInt;

  assign SDAout = driveSDA ? SDAoutIntVoted : 1'b0;
  assign SDAen  = driveSDA ? SDAenIntVoted  : (SDAenIntVoted && ~SDAoutIntVoted);

  // sampled I2C lines (should not raise tmrError)
  // tmrg tmr_error_exclude SDAff
  // tmrg tmr_error_exclude SCLff

  reg SDAff;
  reg SCLff;

  wire SDAffVoted = SDAff;
  wire SCLffVoted = SCLff;

  // I2C lines filter
  reg  [2:0] SDAfilter;
  reg  [2:0] SDAfilterNext;
  wire [2:0] SDAfilterVoted=SDAfilter;

  reg  [2:0] SCLfilter;
  reg  [2:0] SCLfilterNext;
  wire [2:0] SCLfilterVoted=SCLfilter;

  // resampled/filtered I2C lines to be used in the state machine
  reg [1:0]  SDA;
  reg [1:0]  SDANext;
  wire [1:0] SDAVoted=SDA;

  reg [1:0]  SCL;
  reg [1:0]  SCLNext;
  wire [1:0] SCLVoted=SCL;

  always @(posedge clk or posedge rst)
    if (rst)
      begin
        SDAff     <= #1 1'b1;
        SCLff     <= #1 1'b1;
        SDAfilter <= #1 3'b111;
        SCLfilter <= #1 3'b111;
        SCL       <= #1 2'b11;
        SDA       <= #1 2'b11;
      end
    else
      begin
        SDAff     <= #1 SDAin;
        SCLff     <= #1 SCLin;
        SDAfilter <= #1 SDAfilterNext;
        SCLfilter <= #1 SCLfilterNext;
        SCL       <= #1 SCLNext;
        SDA       <= #1 SDANext;
      end

  wire SDAfiltered = SDAfilterVoted[0]&SDAfilterVoted[1] | SDAfilterVoted[1]&SDAfilterVoted[2] | SDAfilterVoted[0]&SDAfilterVoted[2];
  wire SCLfiltered = SCLfilterVoted[0]&SCLfilterVoted[1] | SCLfilterVoted[1]&SCLfilterVoted[2] | SCLfilterVoted[0]&SCLfilterVoted[2];

  wire SDA_rise = !SDAVoted[1] &  SDAVoted[0];
  wire SDA_fall =  SDAVoted[1] & !SDAVoted[0];

  wire SCL_rise = !SCLVoted[1] &  SCLVoted[0];
  wire SCL_fall =  SCLVoted[1] & !SCLVoted[0];

  assign start  = SCLVoted[0] & SDA_fall; // falling edge on SDA (when SCL high)
  assign stop   = SCLVoted[0] & SDA_rise; // rising edge on SDA (when SCL high)

  // state registers //
  reg [2:0] state;
  reg [7:0] buffer;
  reg [3:0] i;

  // - - - - - - - - - - - - - - - next state - - - - - - - - - - - - - - - - //
  reg       SDAoutIntNext;
  reg       SDAenIntNext;
  reg [2:0] stateNext;
  reg [7:0] bufferNext;
  reg [3:0] iNext;
  reg [7:0] wbDataOutNext;
  reg [15:0] wbAdrNext;
  reg       wbWeNext;

  reg  activeNext;
  wire activeVoted=active;

  // - - - - - - - - - - - - - - - voting - - - - - - - - - - - - - - - - - - //
  wire [3:0] iVoted         = i;
  wire [7:0] bufferVoted    = buffer;
  wire [7:0] wbDataOutVoted = wbDataOut;
  wire [2:0] stateVoted     = state;
  wire [15:0] wbAdrVoted     = wbAdr;
  wire       wbWeVoted      = wbWe;

  localparam [2:0]
    IDLE     = 3'd0,
    ADRI2C   = 3'd1,
    REGADR   = 3'd2,
    REGADR2  = 3'd3,
    DATA_M2S = 3'd4,
    DATA_S2M = 3'd5;

  localparam TIMEOUT_MAX=24'hffffff;
  reg [23:0]  timeoutCnt;
  reg [23:0]  timeoutCntNext;
  wire [23:0] timeoutCntVoted = timeoutCnt;
  wire timeout=(timeoutCntVoted==24'b0);

  wire [7:0] wbDataInVoted = wbDataIn;
  wire [6:0] i2cAddrVoted = i2cAddr;

  // - - - - - - - - - - - - - - - registers  - - - - - - - - - - - - - - - - //
  always  @(posedge clk or posedge rst) begin
    if (rst) begin
      i              <=#1 4'b0;
      buffer         <=#1 8'b0;
      wbDataOut      <=#1 8'b0;
      SDAoutInt      <=#1 1'b1;
      SDAenInt       <=#1 1'b0;
      state          <=#1 IDLE;
      wbAdr          <=#1 16'b0;
      wbWe           <=#1 1'b0;
      active         <=#1 1'b0;
      timeoutCnt     <=#1 TIMEOUT_MAX;
    end
    else begin
      i              <=#1 iNext;
      buffer         <=#1 bufferNext;
      wbDataOut      <=#1 wbDataOutNext;
      SDAoutInt      <=#1 SDAoutIntNext;
      SDAenInt       <=#1 SDAenIntNext;
      state          <=#1 stateNext;
      wbAdr          <=#1 wbAdrNext;
      wbWe           <=#1 wbWeNext;
      active         <=#1 activeNext;
      timeoutCnt     <=#1 timeoutCntNext;
    end
  end

  // - - - - - - - - - - - - - - - next state logic - - - - - - - - - - - - - //
  always  @ *
    begin
      SDAoutIntNext = SDAoutIntVoted;
      SDAenIntNext  = SDAenIntVoted;
      iNext         = iVoted;
      stateNext     = stateVoted;
      wbDataOutNext = wbDataOutVoted;
      bufferNext    = bufferVoted;
      wbAdrNext     = wbAdrVoted;
      wbWeNext      = wbWeVoted;
      activeNext    = activeVoted;
      timeoutCntNext= timeoutCntVoted;
      case (stateVoted)
        IDLE: ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          begin
            SDAenIntNext  = 1'b0;
            wbWeNext      = 1'b0;
          end
        ADRI2C: ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          begin      // get slave address and RW_ bit
            wbWeNext = 1'b0;
            if (SCL_rise)
              iNext= iVoted + 4'b1;

            if (iVoted<4'd8)
              begin
                SDAenIntNext = 1'b0;
                if (SCL_rise)
                  bufferNext = {bufferVoted[6:0], SDAVoted[0]};
              end

            if (iVoted==4'd8 && SCL_fall)
              begin
                if ((bufferVoted[7:1] == i2cAddrVoted) | (bufferVoted[7:1] == 7'b000_0000))  // general call address
                  begin
                    SDAenIntNext      = 1'b1;     // ACK, send 0 to SDA
                    SDAoutIntNext     = 1'b0;
                    if (bufferVoted[0])
                      wbWeNext        = 1'b0;
                  end
                else
                  stateNext           = IDLE;
              end

            if (iVoted==4'd9)
              begin
                if (bufferVoted[0])
                  begin // READ
                    iNext             = 4'b0;
                    stateNext         = DATA_S2M;
                    bufferNext        = wbDataInVoted;
                  end
                else
                  begin // WRITE
                    if (SCL_fall)
                      begin
                        iNext         = 4'b0;
                        SDAenIntNext  = 1'b0;
                        stateNext     = REGADR;
                      end
                  end
              end
          end

        REGADR: ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          begin    // get first byte of internal memory address
            wbWeNext = 1'b0;
            if (SCL_rise)
              iNext = iVoted + 4'b1;

            if (iVoted<4'd8)
              begin
                SDAenIntNext = 1'b0;
                if (SCL_rise)
                  bufferNext = {bufferVoted[6:0], SDAVoted[0]};
              end

            if (iVoted==4'd8 && SCL_fall)
              begin
                SDAenIntNext  = 1'b1;     // ACK, send 0 to SDA
                SDAoutIntNext = 1'b0;
                wbAdrNext[7:0]= bufferVoted[7:0];
              end

            if (iVoted==4'd9 && SCL_fall)
              begin
                iNext         = 4'b0;
                SDAenIntNext  = 1'b0;
                stateNext     = REGADR2;
              end
          end

        REGADR2: ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          begin    // get second byte of internal memory address
            wbWeNext = 1'b0;
            if (SCL_rise)
              iNext = iVoted + 4'b1;

            if (iVoted<4'd8)
              begin
                SDAenIntNext = 1'b0;
                if (SCL_rise)
                  bufferNext = {bufferVoted[6:0], SDAVoted[0]};
              end

            if (iVoted==4'd8 && SCL_fall)
              begin
                SDAenIntNext  = 1'b1;     // ACK, send 0 to SDA
                SDAoutIntNext = 1'b0;
                wbAdrNext[15:8]= bufferVoted[7:0];
              end

            if (iVoted==4'd9 && SCL_fall)
              begin
                iNext         = 4'b0;
                SDAenIntNext  = 1'b0;
                stateNext     = DATA_M2S;
              end
          end

         DATA_M2S:///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          begin
            if (SCL_rise)
              iNext = iVoted + 4'b1;

            if (iVoted<4'd8)
              begin
                SDAoutIntNext = 1'b1;
                SDAenIntNext  = 1'b0;
                if (SCL_rise)
                  bufferNext  = {bufferVoted[6:0], SDAVoted[0]};
              end

            if (iVoted==4'd8 && SCL_fall)
              begin
                SDAenIntNext = 1'b1;     // ACK, send 0 to SDA
                SDAoutIntNext= 1'b0;
                wbDataOutNext= bufferVoted;
                wbWeNext     = 1'b1;
              end

            if (iVoted==4'd9 && SCL_fall)
              begin
                SDAenIntNext = 1'b0;
                iNext        = 4'b0;
                wbWeNext     = 1'b0;
                wbAdrNext    = wbAdrVoted + 16'b1; // increment internal address for block
              end
          end
         DATA_S2M:
          begin // TX
            if (SCL_fall)
              iNext = iVoted + 4'b1;

            if (iVoted<4'd8)
              begin
                SDAenIntNext = 1'b1;
                if (SCL_fall)
                  begin
                    SDAoutIntNext = bufferVoted[7];
                    bufferNext = bufferVoted << 1;
                  end
              end

            if (iVoted==4'd8 && SCL_fall)
              begin
                SDAenIntNext = 1'b0;          // ACK should come from master receiver
                wbAdrNext = wbAdrVoted + 16'b1; // increment internal address for block
              end

            if (iVoted==4'd9)
              begin
                SDAenIntNext = 1'b0;
                if (SCL_rise)
                  begin
                    if (~SDAVoted[0])
                      begin
                        iNext        = 4'b0;
                        bufferNext   = wbDataInVoted;
                      end
                    else
                      begin
                        stateNext    = IDLE;
                      end
                  end
              end
          end
        default:
          stateNext = IDLE;
      endcase

      SDAfilterNext = {SDAfilterVoted[1:0], SDAffVoted};
      SCLfilterNext = {SCLfilterVoted[1:0], SCLffVoted};

      SCLNext = {SCLVoted[0], SCLfiltered};
      SDANext = {SDAVoted[0], SDAfiltered};

      if (stop | timeout)
        begin
          SDAenIntNext  = 1'b0;
          SDAoutIntNext = 1'b1;
          activeNext    = 1'b0;
          stateNext     = IDLE;
          SDAfilterNext = 3'b111;
          SCLfilterNext = 3'b111;
          SCLNext       = 2'b11;
          SDANext       = 2'b11;
        end

      if (start)
        begin
          stateNext    = ADRI2C;
          activeNext   = 1'b1;
          SDAenIntNext = 1'b0;
          iNext        = 4'b0;
        end

      // Avoid changes when SCL is high
      if (SCLVoted[0])
        begin
          SDAenIntNext  = SDAenIntVoted;
          SDAoutIntNext = SDAoutIntVoted;
        end

      if (SDA_fall | SDA_rise | SCL_rise | SCL_fall)
        timeoutCntNext = TIMEOUT_MAX;
      else
        timeoutCntNext = timeoutCntVoted - 24'b1;

  end
endmodule
