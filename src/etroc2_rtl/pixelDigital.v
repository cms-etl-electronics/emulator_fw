`timescale 1ps/1ps

/*
  (from previous pixel)                         (to next pixel)
                    /-----------------------\
  pixelAddrIn    -> |                       | -> pixelAddrOut
  busRstIn       -> |                       | -> busRstOut
  busWeIn        -> |                       | -> busWeOut
  busClkIn       -> |         pixel         | -> busWeOut
  busReIn        -> |        Digital        | -> busReOut
  busAddrIn      -> |                       | -> busAddrOut
  colAddrIn      -> |                       | -> colAddOut
  busDataMosiIn  -> |                       | -> busDataMosiOut
  busDataMisoOut <- |                       | <- busDataMisoIn
  busTmrErrorOut <- |                       | <- busTmrErrorIn
                    \-----------------------/
                       |              /\
                      \/               |
              pixelConfig[63:0]  defaultPixelConfig[63:0]
                 pixelID[7:0]       pixelStatus[63:0]

  Signal naming convention inside:
    signalIn -> [buf] -> signalInBuf -> [logic] -> signalOutInt -> [buf] -> signalOut
*/

module pixelDigitalInBuf #(
  parameter WIDTH = 16
)(
  input [WIDTH-1:0] I,
  output [WIDTH-1:0] Z
);
  genvar i;
  generate
  for(i=0; i<WIDTH; i=i+1) begin
    CKBD4 B_preserve(.I(I[i]), .Z(Z[i]));
  end
  endgenerate
endmodule

module pixelDigitalOutBuf #(
  parameter WIDTH = 16
)(
  input [WIDTH-1:0] I,
  output [WIDTH-1:0] Z
);
  genvar i;
  generate
  for(i=0; i<WIDTH; i=i+1) begin
    CKBD8 B_preserve(.I(I[i]), .Z(Z[i]));
  end
  endgenerate
endmodule

module pixelDigital #(
  parameter REGS=32,
  parameter [255:0] DEFAULT_VALUE=256'h0//parameter [31:0] DEFAULT_VALUE=32'h0
)(
  // bus/configuration input (comming from the pixel below)
  input  [3:0]        pixelAddrIn,
  input               busRstIn,
  input               busWeIn,
  input               busClkIn,
  input               busReIn,
  input [15:0]        busAddrIn,
  input [7:0]         busDataMosiIn,
  input [3:0]         colAddrIn,
  output [7:0]        busDataMisoOut,
  output              busTmrErrorOut,

  // bus/configuration output (going to the pixel above)
  output [3:0]        pixelAddrNextOut,
  output              busRstOut,
  output              busWeOut,
  output              busClkOut,
  output              busReOut,
  output [15:0]       busAddrOut,
  output [3:0]        colAddrOut,
  output [7:0]        busDataMosiOut,
  input [7:0]         busDataMisoIn,
  input               busTmrErrorIn,

  // in-pixel configuration/status registers
  input [REGS*8-1:0]  defaultPixelConfig,
  output [REGS*8-1:0] pixelConfig,
  input [8*8-1:0]  pixelStatus,
  output [7:0]        pixelID,
  output         HIGH,
  output         LOW
);

  // TMRG constraints
  // tmrg default triplicate
  // tmrg do_not_triplicate defaultPixelConfig
  // tmrg do_not_triplicate pixelConfig
  // tmrg do_not_triplicate pixelStatus
  // tmrg do_not_triplicate pixelID
  // tmrg do_not_triplicate HIGH
  // tmrg do_not_triplicate LOW
  wire tmrError = 1'b0;

  // Input buffers
  wire [63:0] pixelStatusBuf;//wire [31:0] pixelStatusBuf;
  wire [15:0] busAddrInBuf;
  wire [7:0] busDataMosiInBuf;
  wire [7:0] busDataMisoInBuf;
  wire [3:0] pixelAddrInBuf;
  wire [3:0] colAddrInBuf;
  wire busRstInBuf;
  wire busWeInBuf;
  wire busClkInBuf;
  wire busReInBuf;
  wire busTmrErrorBuf;
  wire busTmrErrorInBuf;
  pixelDigitalInBuf  #(.WIDTH(4)) IB_PA   (.I(pixelAddrIn), .Z(pixelAddrInBuf));
  pixelDigitalInBuf  #(.WIDTH(1)) IB_BRST (.I(busRstIn), .Z(busRstInBuf));
  pixelDigitalInBuf  #(.WIDTH(1)) IB_BWE  (.I(busWeIn), .Z(busWeInBuf));
  pixelDigitalInBuf  #(.WIDTH(1)) IB_BCLK (.I(busClkIn), .Z(busClkInBuf));
  pixelDigitalInBuf  #(.WIDTH(1)) IB_BRE  (.I(busReIn), .Z(busReInBuf));
  pixelDigitalInBuf #(.WIDTH(16)) IB_ADR  (.I(busAddrIn), .Z(busAddrInBuf));
  pixelDigitalInBuf  #(.WIDTH(8)) IB_MOSI (.I(busDataMosiIn), .Z(busDataMosiInBuf));
  pixelDigitalInBuf  #(.WIDTH(4)) IB_COLA (.I(colAddrIn), .Z(colAddrInBuf));
  pixelDigitalInBuf  #(.WIDTH(8)) IB_MISO (.I(busDataMisoIn), .Z(busDataMisoInBuf));
  pixelDigitalInBuf  #(.WIDTH(1)) IB_TMRE (.I(busTmrErrorIn), .Z(busTmrErrorInBuf));
  pixelDigitalInBuf #(.WIDTH(64)) IB_PS   (.I(pixelStatus), .Z(pixelStatusBuf));//#(.WIDTH(32))

  // if we are in the last pixel, we do not take data from the busDataMisoIn pins (in case the
  // some forgot to conect them)
  wire broadcast = (busAddrInBuf[13] == 1'b1); // addr[13:11]==001: BC; addr[13:11]!=001: DM
  wire [255:0] pixelConfigInt;//wire [31:0] pixelConfigInt;
  wire [7:0] busDataMisoMasked = (pixelAddrInBuf==4'hF) ? 8'h0 : busDataMisoInBuf;
  wire busTmrErrorMasked = (pixelAddrInBuf==4'hF) ? 1'h0 : busTmrErrorInBuf;
  wire pixelSelected = (busAddrInBuf[8:5] == pixelAddrInBuf);
  wire [REGS-1:0] regSelect = 1'b1<<busAddrInBuf[4:0];//busAddrInBuf[1:0];
  wire write = busWeInBuf & (pixelSelected | broadcast);
  wire [REGS-1:0] regWe = regSelect & {REGS{write}};
  wire [7:0] reg_read_mux_read_write = busAddrInBuf[4:0]<REGS ? pixelConfigInt[busAddrInBuf[4:0]*8 +: 8] : 8'b0;
  wire [7:0] reg_read_mux_read_only = busAddrInBuf[2:0]<REGS ? pixelStatusBuf[busAddrInBuf[2:0]*8 +: 8] : 8'b0;
  wire [7:0] reg_read_mux = busAddrInBuf[14] ? reg_read_mux_read_only : reg_read_mux_read_write;

  wire [3:0] pixelAddrNextOutInt = pixelAddrInBuf + 4'b1;
  wire busRstOutInt = busRstInBuf;
  wire busWeOutInt = busWeInBuf;
  wire busReOutInt = busReInBuf;
  wire [15:0] busAddrOutInt = busAddrInBuf;
  wire [7:0] busDataMosiOutInt = busDataMosiInBuf;
  wire busTmrErrorOutInt = busTmrErrorMasked | tmrError;
  wire busClkOutInt = busClkInBuf;
  wire [7:0] busDataMisoOutInt = (pixelSelected ? reg_read_mux : 8'h0) | busDataMisoMasked;
  wire [7:0] pixelIDInt = {colAddrInBuf, pixelAddrInBuf};
  wire [3:0] colAddrOutInt = colAddrInBuf;

  // Clock gating (if our pixel is not selected or there is no TMR error, we do not
  // enable the clock in this pixel)
  wire clkGated;
  wire clkEnable = broadcast|pixelSelected|tmrError;
  CKLNQD6 CG_preserve(.CP(busClkInBuf), .Q(clkGated), .TE(1'b0), .E(clkEnable));

  // memory cells
  genvar r;
  generate
    for(r=0;r<REGS;r=r+1)
      begin: REG
        memoryCell MC(
            .rst(busRstInBuf),
            .clk(clkGated),
            .dataIn(busDataMosiInBuf),
            .load(regWe[r]),
            .dataOut(pixelConfigInt[r*8 +: 8]),
            .defaultValue(defaultPixelConfig[r*8 +: 8])
        );
      end
  endgenerate

  // TIE HIGH/ TIE LOW
  //tmrg do_not_triplicate TIELOW  TIEHIGH
  TIEL TIELOW(.ZN(LOW));
  TIEH TIEHIGH(.Z(HIGH));

  // Output buffers
//  pixelDigitalOutBuf  #(.WIDTH(4)) OB_CURCOLADR (.I(colAddrOutInt), .Z(colAddrOut));
  pixelDigitalOutBuf  #(.WIDTH(8)) OB_PID  (.I(pixelIDInt), .Z(pixelID));
  pixelDigitalOutBuf  #(.WIDTH(4)) OB_PA   (.I(pixelAddrNextOutInt), .Z(pixelAddrNextOut));
  pixelDigitalOutBuf  #(.WIDTH(1)) OB_BRST (.I(busRstOutInt), .Z(busRstOut));
  pixelDigitalOutBuf  #(.WIDTH(1)) OB_BWE  (.I(busWeOutInt), .Z(busWeOut));
  pixelDigitalOutBuf  #(.WIDTH(1)) OB_BCLK (.I(busClkOutInt), .Z(busClkOut));
  pixelDigitalOutBuf  #(.WIDTH(1)) OB_BRE  (.I(busReOutInt), .Z(busReOut));
  pixelDigitalOutBuf #(.WIDTH(16)) OB_ADR  (.I(busAddrOutInt), .Z(busAddrOut));
  pixelDigitalOutBuf  #(.WIDTH(8)) OB_MOSI (.I(busDataMosiOutInt), .Z(busDataMosiOut));
  pixelDigitalOutBuf  #(.WIDTH(4)) OB_COLA (.I(colAddrOutInt), .Z(colAddrOut));
  pixelDigitalOutBuf  #(.WIDTH(8)) OB_MISO (.I(busDataMisoOutInt), .Z(busDataMisoOut));
  pixelDigitalOutBuf  #(.WIDTH(1)) OB_TMRE (.I(busTmrErrorOutInt), .Z(busTmrErrorOut));
  pixelDigitalOutBuf #(.WIDTH(256)) OB_PC   (.I(pixelConfigInt), .Z(pixelConfig));//#(.WIDTH(32))

endmodule
