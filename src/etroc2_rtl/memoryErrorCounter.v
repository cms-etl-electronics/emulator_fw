`timescale 1ps/1ps

module memoryErrorCounter (
  output[31:0] dataOut,
  input        load,
  input        errDetected,
  input        clk,
  input        rst
);

  // tmrg tmr_error true
  // tmrg tmr_error_exclude load
  // tmrg tmr_error_exclude errDetected

  reg [31:0]   mem;
  reg [31:0]   memNext;
  wire [31:0]  memVoted = mem;
  wire         loadVoted = load;
  wire         errDetectedVoted = errDetected;
  reg          errDetectedFF;
  wire         errDetectedFFVoted = errDetectedFF;
  assign       dataOut = memVoted;

  always @(memVoted or errDetectedFFVoted or loadVoted)
      if (loadVoted)
        memNext = 32'b0;
      else if (errDetectedFFVoted && memVoted < 32'hFFFFFFFF)
        memNext = memVoted + 32'h1;
      else
        memNext = memVoted;


  always@(posedge clk or posedge rst)
    if (rst) begin
        mem <= #10 32'b0;
        errDetectedFF <= #10 1'b0;
    end else begin
        mem <= #10 memNext;
        errDetectedFF <= #10 errDetectedVoted;
    end

endmodule
