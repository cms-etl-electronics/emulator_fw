`timescale 1ps/1ps

module registerFile #(
  parameter MEMLEN = 36
)(
  input [15:0] wbAdr,
  input [7:0] wbDataIn,
  input wbWe,
  input clk,
  input rst,
  output [MEMLEN*8-1:0] values,
  input [MEMLEN*8-1:0] defaultValue,
  input errDetected
);
  // tmrg tmr_error true
  // tmrg do_not_triplicate defaultValue

  //address decoder
  wire [MEMLEN-1:0] wbAdrDecoded;
  memoryAddrDec #(.MEMLEN(MEMLEN)) AD
  (
    .address(wbAdr),
    .enable(wbAdrDecoded)
  );

  // Generation of the necessary memory cells
  genvar i;
  generate
    for (i = 0; i < MEMLEN  ; i = i +1)
    begin: memgen
      memoryCell MC(
        .rst(rst),
        .clk(clk),
        .dataIn(wbDataIn),
        .load(wbAdrDecoded[i]&wbWe),
        .dataOut(values[(8*i)+7:8*i]),
        .defaultValue(defaultValue[(8*i)+7:8*i])
      );
    end
  endgenerate
endmodule
