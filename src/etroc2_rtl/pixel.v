`timescale 1ps/1ps

module pixel #(
  parameter REGS=32//4
)(
  // bus/configuration input (comming from the pixel below)
  input  [3:0]   pixelAddrIn,
  input          busRstIn,
  input          busWeIn,
  input          busClkIn,
  input          busReIn,
  input [15:0]   busAddrIn,
  input [7:0]    busDataMosiIn,
  input [3:0]    colAddrIn,
  output [7:0]   busDataMisoOut,
  output         busTmrErrorOut,

  // bus/configuration output (going to the pixel above)
  output [3:0]   pixelAddrNextOut,
  output         busRstOut,
  output         busWeOut,
  output         busClkOut,
  output         busReOut,
  output [15:0]  busAddrOut,
  output [7:0]   busDataMosiOut,
  output [3:0]   colAddrOut,
  input [7:0]    busDataMisoIn,
  input          busTmrErrorIn
);

  wire [REGS*8-1:0] pixelConfig;
  wire [8*8-1:0] pixelStatus;
  wire [REGS*8-1:0] defaultPixelConfig;
  wire [7:0]        pixelID;
  wire         HIGH;
  wire         LOW;

  pixelDigital #(.REGS(REGS)) PD (
    .busAddrIn(busAddrIn),
    .busAddrOut(busAddrOut),
    .busDataMisoIn(busDataMisoIn),
    .busDataMisoOut(busDataMisoOut),
    .busDataMosiIn(busDataMosiIn),
    .busDataMosiOut(busDataMosiOut),
    .busReIn(busReIn),
    .colAddrIn(colAddrIn),
    .busReOut(busReOut),
    .busRstIn(busRstIn),
    .busRstOut(busRstOut),
    .busTmrErrorIn(busTmrErrorIn),
    .busTmrErrorOut(busTmrErrorOut),
    .busWeIn(busWeIn),
    .busWeOut(busWeOut),
    .busClkIn(busClkIn),
    .busClkOut(busClkOut),
    .colAddrOut(colAddrOut),
    .pixelAddrIn(pixelAddrIn),
    .pixelAddrNextOut(pixelAddrNextOut),
    .defaultPixelConfig(defaultPixelConfig),
    .pixelConfig(pixelConfig),
    .pixelStatus(pixelStatus),
    .pixelID(pixelID),
    .HIGH(HIGH),
    .LOW(LOW)
  );

  pixelAnalog #(.REGS(REGS)) PA  (
    .defaultPixelConfig(defaultPixelConfig),
    .pixelConfig(pixelConfig),
    .pixelStatus(pixelStatus),
    .pixelID(pixelID),
    .HIGH(HIGH),
    .LOW(LOW)
  );
endmodule
