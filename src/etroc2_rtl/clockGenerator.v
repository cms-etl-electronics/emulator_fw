`timescale 1ps/1ps

/**
 *  File  : clockGeneratorDelayElement.v
 *  Author: Szymon Kulis (CERN)
 **/

module clockGeneratorEnable(
  input enable,
  output enableV);

  wire enableVoted=enable;
  assign enableV=enableVoted;

endmodule

module clockGenerator(
  input  enable,
  output clk
);
  // tmrg majority_voter_cell majorityVoterTag
  parameter DelayElements=8;

  wire enableV;
//  wire enableVoted=enable;
  clockGeneratorEnable CGE(
    .enable(enable),
    .enableV(enableV)
  );

  wire [DelayElements:0] clkDelayed;

  wire   clkEnd;
  assign clkEnd = clkDelayed[DelayElements];
  wire   clkEndVoted=clkEnd;

  CKND2D4 ND_preserve(.A1(clkEndVoted), .A2(enableV), .ZN(clkDelayed[0]));
//  assign clkDelayed[0] = ~(enableV & clkEndVoted);


//  assign clk = ~(enableV & clkEndVoted);
  assign clk = clkDelayed[0];

  genvar i;
  generate
    for (i = 0; i < DelayElements; i = i + 1)
      begin: DL
        clockGeneratorDelayElement CGDE(.clkIn(clkDelayed[i]),
                                        .clkOut(clkDelayed[i+1])
                                       );
      end
  endgenerate

endmodule
