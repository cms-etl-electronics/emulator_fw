/** ****************************************************************************
 *  lpGBTX                                                                     *
 *  Copyright (C) 2011-2016 GBTX Team, CERN                                    *
 *                                                                             *
 *  This IP block is free for HEP experiments and other scientific research    *
 *  purposes. Commercial exploitation of a chip containing the IP is not       *
 *  permitted.  You can not redistribute the IP without written permission     *
 *  from the authors. Any modifications of the IP have to be communicated back *
 *  to the authors. The use of the IP should be acknowledged in publications,  *
 *  public presentations, user manual, and other documents.                    *
 *                                                                             *
 *  This IP is distributed in the hope that it will be useful, but WITHOUT ANY *
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  *
 *  FOR A PARTICULAR PURPOSE.                                                  *
 *                                                                             *
 *******************************************************************************
 *
 *  file: testInOut.v
 *
 *  History:
 *  2017/01/31 Szymon Kulis    : Created
 *
 **/

`timescale 1ps/1ps

module testOutMuxChn (
  // input signals
  input [31:0] inputSignals,

  // control signals
  input [4:0] testOutSelect,

  // output signals
  output testOutput

);
  // tmrg do_not_touch
  wire [ 31:0] l0=inputSignals;
  wire [ 15:0] l1;
  wire [  7:0] l2;
  wire [  3:0] l3;
  wire [  1:0] l4;
  wire [  0:0] l5;

  genvar i;

  wire [7:0] testOutSelect_buf;

  CKBD2 BIT0_preserve(.I(testOutSelect[0]), .Z(testOutSelect_buf[0]) );
  CKBD2 BIT1_preserve(.I(testOutSelect[1]), .Z(testOutSelect_buf[1]) );
  CKBD2 BIT2_preserve(.I(testOutSelect[2]), .Z(testOutSelect_buf[2]) );
  CKBD2 BIT3_preserve(.I(testOutSelect[3]), .Z(testOutSelect_buf[3]) );
  CKBD2 BIT4_preserve(.I(testOutSelect[4]), .Z(testOutSelect_buf[4]) );


  generate
  for (i=0;i<16;i=i+1)
    begin : mux0
      CKMUX2D1 mux3_preserve(
           .I0(l0[i*2+0]),
           .I1(l0[i*2+1]),
           .S (testOutSelect_buf[0]),
           .Z (l1[i])
      );
    end
  endgenerate

  generate
  for (i=0;i<8;i=i+1)
    begin : mux1
      CKMUX2D1 mux4_preserve(
           .I0(l1[i*2+0]),
           .I1(l1[i*2+1]),
           .S (testOutSelect_buf[1]),
           .Z (l2[i])
      );
    end
  endgenerate

  generate
  for (i=0;i<4;i=i+1)
    begin : mux2
      CKMUX2D1 mux5_preserve(
           .I0(l2[i*2+0]),
           .I1(l2[i*2+1]),
           .S (testOutSelect_buf[2]),
           .Z (l3[i])
      );
    end
  endgenerate

  generate
  for (i=0;i<2;i=i+1)
    begin : mux3
      CKMUX2D1 mux6_preserve(
           .I0(l3[i*2+0]),
           .I1(l3[i*2+1]),
           .S (testOutSelect_buf[3]),
           .Z (l4[i])
      );
    end
  endgenerate

  generate
  for (i=0;i<1;i=i+1)
    begin : mux4
      CKMUX2D4 mux7_preserve(
           .I0(l4[i*2+0]),
           .I1(l4[i*2+1]),
           .S (testOutSelect_buf[4]),
           .Z (l5[0])
      );
    end
  endgenerate

  CKBD4 BITB_preserve(.I(l5[0]), .Z(testOutput) );
endmodule

