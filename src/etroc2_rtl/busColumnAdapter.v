`timescale 1ps/1ps

module busColumnAdapterInBuf #(
  parameter WIDTH = 16
)(
  input [WIDTH-1:0] I,
  output [WIDTH-1:0] Z
);
  genvar i;
  generate
  for(i=0; i<WIDTH; i=i+1) begin
    CKBD4 B_preserve(.I(I[i]), .Z(Z[i]));
  end
  endgenerate
endmodule

module busColumnAdapterOutBuf #(
  parameter WIDTH = 16
)(
  input [WIDTH-1:0] I,
  output [WIDTH-1:0] Z
);
  genvar i;
  generate
  for(i=0; i<WIDTH; i=i+1) begin
    CKBD8 B_preserve(.I(I[i]), .Z(Z[i]));
  end
  endgenerate
endmodule

module busColumnAdapter
(
  // bus/configuration input (comming from the previous column)
  input  [3:0]   columnAddrIn,
  input          busRstIn,
  input          busWeIn,
  input          busClkIn,
  input          busReIn,
  input [15:0]   busAddrIn,
  input [7:0]    busDataMosiIn,
  output [7:0]   busDataMisoOut,
  output         busTmrErrorOut,

  // bus/configuration output (going to the next column)
  output [3:0]   columnAddrNextOut,
  output         busRstOut,
  output         busWeOut,
  output         busClkOut,
  output         busReOut,
  output [15:0]  busAddrOut,
  output [7:0]   busDataMosiOut,
  input [7:0]    busDataMisoIn,
  input          busTmrErrorIn,

  // current column
  output         colRstOut,
  output         colWeOut,
  output         colClkOut,
  output         colReOut,
  output [15:0]  colAddrOut,
  output [7:0]   colDataMosiOut,
  input [7:0]    colDataMisoIn,
  input          colTmrErrorIn,
  output [3:0]   colID
);
  // tmrg default triplicate

  // Input buffers
  wire [15:0] busAddrInBuf;
  wire [7:0] busDataMosiInBuf;
  wire [7:0] busDataMisoInBuf;
  wire [3:0] columnAddrInBuf;
  wire busRstInBuf;
  wire busWeInBuf;
  wire busClkInBuf;
  wire busReInBuf;
  wire busTmrErrorInBuf;
  wire [7:0] colDataMisoInBuf;
  wire colTmrErrorInBuf;

  busColumnAdapterInBuf #(.WIDTH(16)) IB0 (.I(busAddrIn), .Z(busAddrInBuf));
  busColumnAdapterInBuf  #(.WIDTH(8)) IB1 (.I(busDataMosiIn), .Z(busDataMosiInBuf));
  busColumnAdapterInBuf  #(.WIDTH(8)) IB2 (.I(busDataMisoIn), .Z(busDataMisoInBuf));
  busColumnAdapterInBuf  #(.WIDTH(4)) IB3 (.I(columnAddrIn), .Z(columnAddrInBuf));
  busColumnAdapterInBuf  #(.WIDTH(1)) IB4 (.I(busRstIn), .Z(busRstInBuf));
  busColumnAdapterInBuf  #(.WIDTH(1)) IB5 (.I(busWeIn), .Z(busWeInBuf));
  busColumnAdapterInBuf  #(.WIDTH(1)) IB6 (.I(busClkIn), .Z(busClkInBuf));
  busColumnAdapterInBuf  #(.WIDTH(1)) IB7 (.I(busReIn), .Z(busReInBuf));
  busColumnAdapterInBuf  #(.WIDTH(1)) IB8 (.I(busTmrErrorIn), .Z(busTmrErrorInBuf));
  busColumnAdapterInBuf  #(.WIDTH(8)) IB9 (.I(colDataMisoIn), .Z(colDataMisoInBuf));
  busColumnAdapterInBuf  #(.WIDTH(1)) IB10 (.I(colTmrErrorIn), .Z(colTmrErrorInBuf));

  // core logic

  // if we are in the last column, we do not take data from the busDataMisoIn pins (in case the
  // some forgot to conect them)
  wire broadcast = (busAddrInBuf[13] == 1'b1); // addr[13:11]==001: BC; addr[13:11]!=001: DM
  wire [7:0] busDataMisoMasked = (columnAddrInBuf==4'hF) ? 8'h0 : busDataMisoInBuf;
  wire busTmrErrorMasked = (columnAddrInBuf==4'hF) ? 8'h0 : busTmrErrorInBuf;
  wire columnSelected  = (busAddrInBuf[12:9] == columnAddrInBuf);	 

  wire colRstOutInt = busRstInBuf;
  wire colReOutInt = busReInBuf;
  wire colWeOutInt = (broadcast | columnSelected) ? busWeInBuf : 1'b0;
  wire colClkOutInt = (colTmrErrorInBuf | columnSelected | broadcast) ? busClkInBuf : 1'b0;
  wire [15:0] colAddrOutInt = (broadcast | columnSelected) ? busAddrInBuf : 16'b0;
  wire [7:0] colDataMosiOutInt = (broadcast | columnSelected) ? busDataMosiInBuf : 8'b0;
  wire [3:0] colIDInt = columnAddrInBuf;

  wire [3:0] columnAddrNextOutInt = columnAddrInBuf + 4'b1;
  wire busRstOutInt = busRstInBuf;
  wire busWeOutInt = broadcast? busWeInBuf: (columnSelected ? 1'b0 : busWeInBuf);
  wire busClkOutInt = (busTmrErrorMasked | !columnSelected | broadcast)? busClkInBuf : 1'b0;
  wire busReOutInt = busReInBuf;
  wire [15:0] busAddrOutInt = broadcast? busAddrInBuf: (columnSelected ? 16'b0 : busAddrInBuf);
  wire [7:0] busDataMosiOutInt = broadcast? busDataMosiInBuf: (columnSelected ? 8'b0 : busDataMosiInBuf);

  wire [7:0] busDataMisoOutInt = columnSelected ? colDataMisoInBuf : busDataMisoMasked;
  wire busTmrErrorOutInt = colTmrErrorInBuf | busTmrErrorMasked;

  // output buffers
  busColumnAdapterOutBuf #(.WIDTH(16)) OB0 (.I(busAddrOutInt), .Z(busAddrOut));
  busColumnAdapterOutBuf #(.WIDTH(16)) OB1 (.I(colAddrOutInt), .Z(colAddrOut));
  busColumnAdapterOutBuf  #(.WIDTH(8)) OB2 (.I(busDataMisoOutInt), .Z(busDataMisoOut));
  busColumnAdapterOutBuf  #(.WIDTH(8)) OB3 (.I(busDataMosiOutInt), .Z(busDataMosiOut));
  busColumnAdapterOutBuf  #(.WIDTH(8)) OB4 (.I(colDataMosiOutInt), .Z(colDataMosiOut));
  busColumnAdapterOutBuf  #(.WIDTH(4)) OB5 (.I(colIDInt), .Z(colID));
  busColumnAdapterOutBuf  #(.WIDTH(4)) OB6 (.I(columnAddrNextOutInt), .Z(columnAddrNextOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB7 (.I(busTmrErrorOutInt), .Z(busTmrErrorOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB8 (.I(busRstOutInt), .Z(busRstOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB9 (.I(busWeOutInt), .Z(busWeOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB10 (.I(busClkOutInt), .Z(busClkOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB11 (.I(busReOutInt), .Z(busReOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB12 (.I(colRstOutInt), .Z(colRstOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB13 (.I(colWeOutInt), .Z(colWeOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB14 (.I(colClkOutInt), .Z(colClkOut));
  busColumnAdapterOutBuf  #(.WIDTH(1)) OB15 (.I(colReOutInt), .Z(colReOut));

endmodule
