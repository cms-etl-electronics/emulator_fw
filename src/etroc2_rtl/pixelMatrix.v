`timescale 1ps/1ps

module pixelMatrix #(
  parameter COLS=16
)(
  input        busRstIn,
  input        busWeIn,
  input        busReIn,
  input        busClkIn,
  input [15:0] busAddrIn,
  input [7:0]  busDataMosiIn,
  output [7:0] busDataMisoOut,
  output       busTmrErrorOut
);
  genvar r;
  wire [COLS:0] busRst;
  wire [COLS:0] busWe;
  wire [COLS:0] busClk;
  wire [COLS:0] busRe;
  wire [COLS:0] busTmrError;
  wire [8*(COLS+1)-1:0] busDataMosi;
  wire [16*(COLS+1)-1:0] busAddr;
  wire [8*(COLS+1)-1:0] busDataMiso;
  wire [4*(COLS+1)-1:0] columnAddr;

  assign busRst[0] = busRstIn;
  assign busWe[0] = busWeIn;
  assign busClk[0] = busClkIn;
  assign busRe[0] = busReIn;
  assign busTmrErrorOut = busTmrError[0];
  assign busDataMosi[7:0] = busDataMosiIn;
  assign busAddr[15:0] = busAddrIn;
  assign busDataMisoOut = busDataMiso[7:0];
  assign busDataMiso[8*COLS +: 8] = 8'b0;
  assign busTmrError[COLS] = 1'b0;
  assign columnAddr[3:0] = 4'b0;

  genvar c;
  generate
    for(c=0;c<COLS;c=c+1)
      begin: column
        pixelColumn PC(

          .columnAddrIn(columnAddr[c*4 +: 4]),
          .busAddrIn(busAddr[c*16 +: 16]),
          .busClkIn(busClk[c]),
          .busDataMisoOut(busDataMiso[c*8 +: 8]),
          .busDataMosiIn(busDataMosi[c*8 +: 8]),
          .busReIn(busRe[c]),
          .busRstIn(busRst[c]),
          .busTmrErrorOut(busTmrError[c]),
          .busWeIn(busWe[c]),

          .columnAddrNextOut(columnAddr[(c+1)*4 +: 4]),
          .busAddrOut(busAddr[(c+1)*16 +: 16]),
          .busClkOut(busClk[c+1]),
          .busDataMisoIn(busDataMiso[(c+1)*8 +: 8]),
          .busDataMosiOut(busDataMosi[(c+1)*8 +: 8]),
          .busReOut(busRe[c+1]),
          .busRstOut(busRst[c+1]),
          .busTmrErrorIn(busTmrError[c+1]),
          .busWeOut(busWe[c+1])
        );
      end
  endgenerate

endmodule
