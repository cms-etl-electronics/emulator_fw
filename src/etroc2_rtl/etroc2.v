`timescale 1ps/1ps

module etroc2(
  inout SDA,
  inout SCL,
  inout RSTN,
  inout A0,
  inout A1
);

  // tmrg default do_not_triplicate

  localparam CERN_IO_DS_HIGH     = 1'b1;
  localparam CERN_IO_DS_LOW      = 1'b0;
  localparam CERN_IO_PULLENABLE  = 1'b1;
  localparam CERN_IO_PULLDISABLE = 1'b0;
  localparam CERN_IO_PULLUP      = 1'b1;
  localparam CERN_IO_PULLDOWN    = 1'b0;
  wire SDA_out = 1'b0;
  wire SDA_en = 1'b0;
  wire SCL_in;

  wire SDAds;
  wire SDAen;
  wire SDAin;
  wire SDAout;
  wire SDApe;
  wire SDAud;
  IO_1P2V_C4 SDA_IO (
      .A(SDAout),
      .DS(SDAds),
      .IO(SDA),
      .OUT_EN( (~SDAout) & SDAen),
      .PEN(SDApe),
      .UD(SDAud),
      .Z(),
      .Z_h(SDAin)
  );

  wire SCLds;
  wire SCLen;
  wire SCLin;
  wire SCLout;
  wire SCLpe;
  wire SCLud;
  IO_1P2V_C4 SCL_IO (
      .A(SCLout),
      .DS(SCLds),
      .IO(SCL),
      .OUT_EN(SCLen),
      .PEN(SCLpe),
      .UD(SCLud),
      .Z(),
      .Z_h(SCLin)
  );

  wire A0ds;
  wire A0en;
  wire A0in;
  wire A0out;
  wire A0ud;
  wire A0pe;
  IO_1P2V_C4 A0_IO (
      .A(A0out),
      .DS(A0ds),
      .IO(A0),
      .OUT_EN(A0en),
      .PEN(A0pe),
      .UD(A0ud),
      .Z(),
      .Z_h(A0in)
  );

  wire A1ds;
  wire A1en;
  wire A1in;
  wire A1out;
  wire A1ud;
  wire A1pe;
  IO_1P2V_C4 A1_IO (
      .A(A1out),
      .DS(A1ds),
      .IO(A1),
      .OUT_EN(A1en),
      .PEN(A1pe),
      .UD(A1ud),
      .Z(),
      .Z_h(A1in)
  );

  wire RSTNds;
  wire RSTNen;
  wire RSTNin;
  wire RSTNout;
  wire RSTNud;
  wire RSTNpe;
  IO_1P2V_C4 RSTN_IO (
      .A(RSTNout),
      .DS(RSTNds),
      .IO(RSTN),
      .OUT_EN(RSTNen),
      .PEN(RSTNpe),
      .UD(RSTNud),
      .Z(),
      .Z_h(RSTNin)
  );

  localparam COLS = 16;
  localparam HIGH_LOW_OUTS = 50;
  wire [15:0] busAddr; // tmrg triplicate busAddr
  wire        busClk; // tmrg triplicate busClk
  wire [7:0]  busDataMiso; // tmrg triplicate busDataMiso
  wire [7:0]  busDataMosi; // tmrg triplicate busDataMosi
  wire        busRe; // tmrg triplicate busRe
  wire        busRst; // tmrg triplicate busRst
  wire        busTmrError; // tmrg triplicate busTmrError
  wire        busWe; // tmrg triplicate busWe
  wire [HIGH_LOW_OUTS-1:0]  HIGH;
  wire [HIGH_LOW_OUTS-1:0]  LOW;

  // configure CHIP address
  wire [6:2] Ain = {LOW[49], LOW[49], LOW[49], LOW[49], HIGH[49]};

  wire [7:0] regOut00;
  wire [7:0] regOut01;
  wire [7:0] regOut02;
  wire [7:0] regOut03;
  wire [7:0] regOut04;
  wire [7:0] regOut05;
  wire [7:0] regOut06;
  wire [7:0] regOut07;
  wire [7:0] regOut08;
  wire [7:0] regOut09;
  wire [7:0] regOut0A;
  wire [7:0] regOut0B;
  wire [7:0] regOut0C;
  wire [7:0] regOut0D;
  wire [7:0] regOut0E;
  wire [7:0] regOut0F;
  wire [7:0] regOut10;
  wire [7:0] regOut11;
  wire [7:0] regOut12;
  wire [7:0] regOut13;
  wire [7:0] regOut14;
  wire [7:0] regOut15;
  wire [7:0] regOut16;
  wire [7:0] regOut17;
  wire [7:0] regOut18;
  wire [7:0] regOut19;
  wire [7:0] regOut1A;
  wire [7:0] regOut1B;
  wire [7:0] regOut1C;
  wire [7:0] regOut1D;
  wire [7:0] regOut1E;
  wire [7:0] regOut1F;
  
  wire [7:0] defVal00 = 8'h00;
  wire [7:0] defVal01 = 8'h01;
  wire [7:0] defVal02 = 8'h02;
  wire [7:0] defVal03 = 8'h03;
  wire [7:0] defVal04 = 8'h04;
  wire [7:0] defVal05 = 8'h05;
  wire [7:0] defVal06 = 8'h06;
  wire [7:0] defVal07 = 8'h07;
  wire [7:0] defVal08 = 8'h08;
  wire [7:0] defVal09 = 8'h09;
  wire [7:0] defVal0A = 8'h0A;
  wire [7:0] defVal0B = 8'h0B;
  wire [7:0] defVal0C = 8'h0C;
  wire [7:0] defVal0D = 8'h0D;
  wire [7:0] defVal0E = 8'h0E;
  wire [7:0] defVal0F = 8'h0F;
  wire [7:0] defVal10 = 8'h10;
  wire [7:0] defVal11 = 8'h11;
  wire [7:0] defVal12 = 8'h12;
  wire [7:0] defVal13 = 8'h13;
  wire [7:0] defVal14 = 8'h14;
  wire [7:0] defVal15 = 8'h15;
  wire [7:0] defVal16 = 8'h16;
  wire [7:0] defVal17 = 8'h17;
  wire [7:0] defVal18 = 8'h18;
  wire [7:0] defVal19 = 8'h19;
  wire [7:0] defVal1A = 8'h1A;
  wire [7:0] defVal1B = 8'h1B;
  wire [7:0] defVal1C = 8'h1C;
  wire [7:0] defVal1D = 8'h1D;
  wire [7:0] defVal1E = 8'h1E;
  wire [7:0] defVal1F = 8'h1F;

  wire [7:0] regIn100 = regOut00;
  wire [7:0] regIn101 = regOut01;
  wire [7:0] regIn102 = regOut02;
  wire [7:0] regIn103 = regOut03;
  wire [7:0] regIn104 = regOut04;
  wire [7:0] regIn105 = regOut05;
  wire [7:0] regIn106 = regOut06;
  wire [7:0] regIn107 = regOut07;
  wire [7:0] regIn108 = regOut08;
  wire [7:0] regIn109 = regOut09;
  wire [7:0] regIn10A = regOut0A;
  wire [7:0] regIn10B = regOut0B;
  wire [7:0] regIn10C = regOut0C;
  wire [7:0] regIn10D = regOut0D;
  wire [7:0] regIn10E = regOut0E;
  wire [7:0] regIn10F = regOut0F;


  periphery PER (
    .A0ds(A0ds),
    .A0en(A0en),
    .A0in(A0in),
    .A0out(A0out),
    .A0pe(A0pe),
    .A0ud(A0ud),
    .A1ds(A1ds),
    .A1en(A1en),
    .A1in(A1in),
    .A1out(A1out),
    .A1pe(A1pe),
    .A1ud(A1ud),
    .Ain(Ain),
    .HIGH(HIGH),
    .LOW(LOW),
    .RSTNds(RSTNds),
    .RSTNen(RSTNen),
    .RSTNin(RSTNin),
    .RSTNout(RSTNout),
    .RSTNpe(RSTNpe),
    .RSTNud(RSTNud),
    .SCLds(SCLds),
    .SCLen(SCLen),
    .SCLin(SCLin),
    .SCLout(SCLout),
    .SCLpe(SCLpe),
    .SCLud(SCLud),
    .SDAds(SDAds),
    .SDAen(SDAen),
    .SDAin(SDAin),
    .SDAout(SDAout),
    .SDApe(SDApe),
    .SDAud(SDAud),
    .chipID(4'h3),
    .chipREV(4'b0),
    .matrixBusAddr(busAddr),
    .matrixBusClk(busClk),
    .matrixBusDataMiso(busDataMiso),
    .matrixBusDataMosi(busDataMosi),
    .matrixBusRe(busRe),
    .matrixBusRst(busRst),
    .matrixBusTmrError(busTmrError),
    .matrixBusWe(busWe),
    
    .defVal00(defVal00),
    .defVal01(defVal01),
    .defVal02(defVal02),
    .defVal03(defVal03),
    .defVal04(defVal04),
    .defVal05(defVal05),
    .defVal06(defVal06),
    .defVal07(defVal07),
    .defVal08(defVal08),
    .defVal09(defVal09),
    .defVal0A(defVal0A),
    .defVal0B(defVal0B),
    .defVal0C(defVal0C),
    .defVal0D(defVal0D),
    .defVal0E(defVal0E),
    .defVal0F(defVal0F),
    .defVal10(defVal10),
    .defVal11(defVal11),
    .defVal12(defVal12),
    .defVal13(defVal13),
    .defVal14(defVal14),
    .defVal15(defVal15),
    .defVal16(defVal16),
    .defVal17(defVal17),
    .defVal18(defVal18),
    .defVal19(defVal19),
    .defVal1A(defVal1A),
    .defVal1B(defVal1B),
    .defVal1C(defVal1C),
    .defVal1D(defVal1D),
    .defVal1E(defVal1E),
    .defVal1F(defVal1F),

    .regIn100(regIn100),
    .regIn101(regIn101),
    .regIn102(regIn102),
    .regIn103(regIn103),
    .regIn104(regIn104),
    .regIn105(regIn105),
    .regIn106(regIn106),
    .regIn107(regIn107),
    .regIn108(regIn108),
    .regIn109(regIn109),
    .regIn10A(regIn10A),
    .regIn10B(regIn10B),
    .regIn10C(regIn10C),
    .regIn10D(regIn10D),
    .regIn10E(regIn10E),
    .regIn10F(regIn10F),

    .regOut00(regOut00),
    .regOut01(regOut01),
    .regOut02(regOut02),
    .regOut03(regOut03),
    .regOut04(regOut04),
    .regOut05(regOut05),
    .regOut06(regOut06),
    .regOut07(regOut07),
    .regOut08(regOut08),
    .regOut09(regOut09),
    .regOut0A(regOut0A),
    .regOut0B(regOut0B),
    .regOut0C(regOut0C),
    .regOut0D(regOut0D),
    .regOut0E(regOut0E),
    .regOut0F(regOut0F),
    .regOut10(regOut10),
    .regOut11(regOut11),
    .regOut12(regOut12),
    .regOut13(regOut13),
    .regOut14(regOut14),
    .regOut15(regOut15),
    .regOut16(regOut16),
    .regOut17(regOut17),
    .regOut18(regOut18),
    .regOut19(regOut19),
    .regOut1A(regOut1A),
    .regOut1B(regOut1B),
    .regOut1C(regOut1C),
    .regOut1D(regOut1D),
    .regOut1E(regOut1E),
    .regOut1F(regOut1F)
  );

  pixelMatrix PM(
    .busAddrIn(busAddr),
    .busDataMisoOut(busDataMiso),
    .busDataMosiIn(busDataMosi),
    .busReIn(busRe),
    .busRstIn(busRst),
    .busTmrErrorOut(busTmrError),
    .busWeIn(busWe),
    .busClkIn(busClk)
  );

endmodule
