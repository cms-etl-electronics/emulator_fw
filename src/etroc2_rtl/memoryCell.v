`timescale 1ps / 1ps

module memoryCell (
  input        rst,
  input        clk,
  input  [7:0] dataIn,
  input        load,
  output [7:0] dataOut,
  input  [7:0] defaultValue
);
  // tmrg tmr_error true

  reg [7:0]    mem;
  reg [7:0]    memNext;
  wire [7:0]   memVoted = mem;

  assign #10   dataOut = memVoted;

  always @*
    if (load)
      memNext = dataIn;
    else
      memNext = memVoted;

  always@(posedge clk or posedge rst)
    if (rst)
        mem <= #1 defaultValue;
    else
        mem <= #1 memNext;
endmodule
