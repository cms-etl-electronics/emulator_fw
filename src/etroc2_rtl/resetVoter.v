`timescale 1ps/1ps

module resetVoter(
  input in,
  output out
);
  // tmrg majority_voter_cell majorityVoterTagGates
  wire inVoted=in;
  assign out=inVoted;
endmodule
