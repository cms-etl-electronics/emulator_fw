//
//
//  2022.1.22
//
//  
//
//
///////////////////////////////////////
`timescale 1ns/1ps
`include "./ETROC2Readout/commonDefinition.v"
`define DATA_W_REGADDR  16'h0014
`define DATA_R_REGADDR  16'h0014
`define DATA_VALUE 8'h31


module etroc2_emulator
(
input FPGA_RESETn,//AC12 CMOS1.8V
//----- RB emulator bank3B
input FPGA_CLK, //use L2 LVDS

//////////////////////////
// ETROC emulator bank2A
input ECLK0,// AB16, LVDS

//--data
output EDIN00,// AE15 diff sstl1.2
output T_EDIN00,// AF17 diff sstl1.2
//----- data end--------
//-- command
input  EDOUT00_0,// bank2A=AD13, LVDS  command   bank2J= AH11
//-----command end



//-- data
input EDIN00_test,//T6 RB :EDIN00 
input T_EDIN00_test,//U8
//-- command
output EDout00_test,// T8  diff sstl1.2

output ETH_MDC, //Used as LED output as of 2021-12-06
output ETH_RESETn, //Used as LED output as of 2021-12-06

//--I2C test master
// Used as signal output prob for testing 2022-05-01
output SCL_test,// U3 LVCMOS1.2V 
inout  SDA_test,// U1 LVCMOS1.2V
output RSTn_test,//V1 LVCMOS1.2V 

//-- I2C slave
input SCL_0,// AE20
inout SDA_0,// AA14
input RSTn_0,// AE19
//---- I2C end


output probe3B,
output probe2A


);

    localparam BCSTWIDTH = `L1BUFFER_ADDRWIDTH*2+11+2;

  //  localparam fc_IDLE 		= 8'b1111_0000; //2'hF0 -->n_fc = 0--3'h001 
   wire dnResetRB;

    wire clk320;
	 wire clk320B;
	 wire clk;  // 40MHz
	 wire clk40;
	 
	 
	 	 wire clk320RB;
	 wire clk8RB;
	 wire clk40RB;
	 wire clk8_stp;
	
	 
	 genclk2A inst_clk2A (
		.rst      (~FPGA_RESETn),      //   input,  width = 1,   reset.reset
		.refclk   (ECLK0),   //   input,  width = 1,  refclk.clk
		.locked   (),   //  output,  width = 1,  locked.export
		.outclk_0 (clk), //  output,  width = 1, outclk0.clk
		.outclk_1 (clk320)  //  output,  width = 1, outclk1.clk
		//.outclk_2 (clk40),
		//.outclk_3 (clk320B),
		//.outclk_4 (clk320RB),
		//.outclk_2(clk8RB)
		//.outclk_6 (clk40RB)
	);
	
	assign clk320B=clk320;
	//assign clk320RB=clk320;
	//assign clk40RB=clk;
	assign clk40= clk;

	 
	 
    reg fccAlign=1'b0;
    reg [16:0] chipId= 17'H1ABCD;
	 genclkRB u0 (
		.rst      (~FPGA_RESETn),      //   input,  width = 1,   reset.reset
		.refclk   (FPGA_CLK),   //   input,  width = 1,  refclk.clk
		.locked   (),   //  output,  width = 1,  locked.export
		.outclk_0 (clk320RB), //  output,  width = 1, outclk0.clk
		.outclk_1 (clk8RB),
		.outclk_2 (clk40RB),
		.outclk_3 (clk8_stp)
	);	 

	
//
assign ETH_MDC= Led002;
assign ETH_RESETn= Led004;

wire Led001;
wire Led002;
wire Led003;
wire Led004;
LED2b LED2b_inst1(
        .CK(clk8RB),
        .RSTin(FPGA_RESETn),
        .LED2(Led001),
        .led(Led002)
        );

LED2b LED2b_inst2(
        .CK(clk8RB),
        .RSTin(FPGA_RESETn),
        .LED2(Led003),
        .led(Led004)
        );

		  
//    reg [2:0] cnt_fc=3'b0;
//    reg [7:0] fc_byte;
//    always@(posedge clk320) begin
//        cnt_fc <= cnt_fc + 1'b1;
//        if(cnt_fc==3'd0)
//            fc_byte <= fc_IDLE; //sampling parallel 8 bits fc at 320 MHz clock
//        else
//            fc_byte[7:0] <= {fc_byte[6:0],fc_byte[7]}; //shift 8-bit parallel fc in 40 MHz period, serializing to 1 bit
//    end
//
//    wire fc_fc;
//    assign fc_fc = fc_byte[7];
//

wire fc_fc;
	fastCommandEncoderTop  fastCommandEncoderTop_inst(
	.clk40(clk40RB),
	.clk320(clk320RB),
	.dnReset(dnResetRB),
	.fc_fc(fc_fc)

	);
	 
	 assign EDout00_test=fc_fc;

    //wire initReset;
    reg reset2=1'b1;
    wire dnReset;
    reg [1:0] rateRight=2'b00; //serializer speed
    reg [1:0] rateLeft=2'b00; //serializer speed
    reg [6:0] ocp= 7'h01;
    reg disSCR=1'b1;  
    //reg mergeTrigData=1'b1;
    reg singlePort=1'b0;

    wire mergeTrigData;
    assign mergeTrigData = regOut14[4];	 
	 
    reg [2:0] triggerGranularity=3'd2; //2022-03-24, change from 3'd1 to 3'd2, 2 trigger bits, one for each port.
    wire soutRight;
    wire soutLeft;
    wire ws_start;
    wire ws_stop;
 //   wire chargeInjection;
    wire [3:0] fc_state_bitAlign;
    wire [3:0] fc_ed;
	 
	 
	 wire rst;
	 global_reset glrst
	(
	 .clk(clk),
	 .rst(rst)
	);
	// 1 25ns 0 75ns 1 OO
	reg rst_dly, rst_dly1;
	always @(posedge clk)
	begin
		rst_dly1<=rst;
		rst_dly<=rst_dly1;
	end
	assign dnReset=~(rst_dly | rst);
	
	
	
	
		 wire rstRB;
	 global_reset glrstRB
	(
	 .clk(clk40RB),
	 .rst(rstRB)
	);
	// 1 25ns 0 75ns 1 OO
	reg rst_dlyRB, rst_dly1RB;
	always @(posedge clk40RB)
	begin
		rst_dly1RB<=rstRB;
		rst_dlyRB<=rst_dly1RB;
	end
	assign dnResetRB=~(rst_dlyRB | rstRB);
	
//	wire initReset1;
//	buf b0(initReset1,initReset);
//	wire initReset2;
//	buf b1(initReset2,initReset1);
	
//	always @(posedge clk)
//	begin
//	  dnReset <= initReset; 
//	end
	
	
	
	reg reset2RB=1'b1;
	reg [10:0] rst_cntRB=11'b0;
	always @(posedge clk40RB)
	begin
		rst_cntRB<=rst_cntRB+1'b1;
		if (rst_cntRB< 405)
		begin
			reset2RB<=1'b1;
		end
		else if (rst_cntRB<1217)
			begin
				reset2RB<=1'b0;
				
			end
			else 
				 begin
						reset2RB<=1'b1;
						rst_cntRB<=1218;
				  end

	end
	
	
	wire [39:0]  dataframe_debug_right;//2022
wire [39:0]  dataframe_debug_left;//2022

	ETROCEmulatorTop #(.L1ADDRWIDTH(7),.BCSTWIDTH(27), .PIXELROW(3)) ETROCEmulatorTopInst
(
	.clk(clk),          
    .clk1280(clk320B),//(clk1280),  
    .clk320(clk320),
	 .clk40(clk),//(clk40),
    .clkPolarity(1'b1),//2022 02 `13(1'b0),
    .reset(dnReset),       
    .chipId(chipId),
    // .readoutClockDelayPixel(5'd0),
    // .readoutClockWidthPixel(5'd16),
    // .readoutClock(readoutClock),
    .linkResetSlowControl(1'b0),
    .linkResetTestPattern(1'b1),
    .linkResetFixedPattern(32'h3C5C3C5A),                  
    .emptySlotBCID(12'd1177),       
    .triggerGranularity(triggerGranularity),   
    .disScrambler(disSCR),
    .mergeTriggerData(mergeTrigData),
    .singlePort(singlePort),      
//	.fcAlignStart(fccAlign),		
//	.fcBitAlignError(fc_bitError),	
//	.fcBitAlignStatus(fc_ed),			
	.fcData(EDOUT00_0),//20220210(fc_fc),
//	.fcAlignFinalState(fc_state_bitAlign),
	// .chargeInjection(chargeInjection),
    // .chargeInjectionDelay(5'd24),
    .wsStart(ws_start),
    .wsStop(ws_stop),
	.ocp(ocp),  
    .addrOffset(1'b1),
    .soutRight(soutRight),  //serializer output
    .soutLeft(soutLeft)  //serializer output
	 //2022.dataframe_debug_right(dataframe_debug_right),
    //2022 .dataframe_debug_left(dataframe_debug_left)
);

assign EDIN00=soutRight;
assign T_EDIN00=soutLeft;


    wire [4:0] trigDataSizeRight;
    wire [4:0] trigDataSizeLeft;
    wire [4:0] trigDataSize;
    assign trigDataSize =    triggerGranularity == 3'd1 ? 5'd1 : 
                            (triggerGranularity == 3'd2 ? 5'd2 :
                            (triggerGranularity == 3'd3 ? 5'd4 :
                            (triggerGranularity == 3'd4 ? 5'd8 :
                            (triggerGranularity == 3'd5 ? 5'd16 : 5'd0))));
    assign trigDataSizeRight =  singlePort ? trigDataSize : 
                                (mergeTrigData ? trigDataSize >> 1 : 5'd0); 
    assign trigDataSizeLeft =  singlePort ? 5'd0 : 
                                (mergeTrigData ? trigDataSize >> 1 : trigDataSize);      

	
	wire right_aligned;
	wire right_BadSync6sec;
	wire right_Trig1b;
	wire right_DV3p;
	wire right_DataChk1b;
	wire [9:0] RC_right_goodEventRate;
    wire [1:0] RC_right_dataType;
    wire [19:0] RC_right_BCIDErrorCount;
    wire [19:0] RC_right_nullEventCount;
    wire [19:0] RC_right_goodEventCount;
    wire [19:0] RC_right_notHitEventCount;
    wire [19:0] RC_right_L1OverlfowEventCount;
    wire [19:0] RC_right_totalHitsCount;
    wire [19:0] RC_right_dataErrorCount;
    wire [19:0] RC_right_missedHitsCount;
    wire [8:0]  RC_right_hittedPixelCount;
    wire [19:0] RC_right_frameErrorCount;
    wire [19:0] RC_right_mismatchBCIDCount;
    wire [19:0] RC_right_L1FullEventCount;
    wire [19:0] RC_right_L1HalfFullEventCount;
    wire [19:0] RC_right_SEUEventCount;
    wire [19:0] RC_right_hitCountMismatchEventCount;
    wire [15:0] RC_right_triggerDataOut;
	 
	  reg [39:0] dataframe_debug_right_R;
	 
	 fullChainDataCheck fullChainDataCheckInstRight
    (
        .clk320(clk320RB),//
        .clk640(1'b0),//(clk640),
        .clk1280(1'b0),//(clk1280),
		  .clk8RB(clk8RB),//2022
		  .clk40RB(clk40RB),//2022
        .dataRate(rateRight),
        .chipId(chipId),
        .triggerDataSize(trigDataSizeRight),
        .reset(dnResetRB),
        .reset2(reset2RB),
        .aligned(right_aligned),
        .disSCR(disSCR),
        .serialIn(EDIN00_test),//(soutRight),
		  
		  .BadSync6sec(right_BadSync6sec),
        .Trig1b(right_Trig1b),
        .DV3p(right_DV3p),
        .DataChk1b(right_DataChk1b),

        .RC_BCIDErrorCount(RC_right_BCIDErrorCount),
        .RC_dataType(RC_right_dataType),
        .RC_nullEventCount(RC_right_nullEventCount),
        .RC_goodEventCount(RC_right_goodEventCount),
        .RC_notHitEventCount(RC_right_notHitEventCount),
        .RC_L1OverlfowEventCount(RC_right_L1OverlfowEventCount),
        .RC_totalHitsCount(RC_right_totalHitsCount),
        .RC_dataErrorCount(RC_right_dataErrorCount),
        .RC_missedHitsCount(RC_right_missedHitsCount),
        .RC_hittedPixelCount(RC_right_hittedPixelCount),
        .RC_frameErrorCount(RC_right_frameErrorCount),
        .RC_L1FullEventCount(RC_right_L1FullEventCount),
        .RC_L1HalfFullEventCount(RC_right_L1HalfFullEventCount),
        .RC_SEUEventCount(RC_right_SEUEventCount),
        .RC_goodEventRate(RC_right_goodEventRate),
        .RC_hitCountMismatchEventCount(RC_right_hitCountMismatchEventCount),
        .RC_mismatchBCIDCount(RC_right_mismatchBCIDCount),
        .RC_triggerDataOut(RC_right_triggerDataOut)
		  // 2022 input .dataframe_debug_right(dataframe_debug_right_R)
    );
	 
	 
//assign SCL_test=left_DataChk1b;
//assign SDA_test=left_DV3p;
//assign RSTn_test=left_BadSync6sec;

	 wire left_aligned;
	 wire left_BadSync6sec;
	 wire left_Trig1b;
	 wire left_DV3p;
 	 wire left_DataChk1b;
    wire [9:0] RC_left_goodEventRate;
    wire [1:0] RC_left_dataType;
    wire [19:0] RC_left_BCIDErrorCount;
    wire [19:0] RC_left_nullEventCount;
    wire [19:0] RC_left_goodEventCount;
    wire [19:0] RC_left_notHitEventCount;
    wire [19:0] RC_left_L1OverlfowEventCount;
    wire [19:0] RC_left_totalHitsCount;
    wire [19:0] RC_left_dataErrorCount;
    wire [19:0] RC_left_missedHitsCount;
    wire [8:0]  RC_left_hittedPixelCount;
    wire [19:0] RC_left_frameErrorCount;
    wire [19:0] RC_left_mismatchBCIDCount;
    wire [19:0] RC_left_L1FullEventCount;
    wire [19:0] RC_left_L1HalfFullEventCount;
    wire [19:0] RC_left_SEUEventCount;
    wire [19:0] RC_left_hitCountMismatchEventCount;
    wire [15:0] RC_left_triggerDataOut;

    fullChainDataCheck fullChainDataCheckInstLeft
    (
        //.clk320(clk320RB),
        .clk320(clk320),
        .clk640(1'b0),//(clk640),
        .clk1280(1'b0),//(clk1280),
		  .clk8RB(clk8RB),//2022
		  //.clk40RB(clk40RB),//2022
		  .clk40RB(clk),//2022
        .dataRate(rateLeft),
        .chipId(chipId),
        .triggerDataSize(trigDataSizeLeft),
        .reset(dnResetRB),
        .reset2(reset2RB),
        .aligned(left_aligned),
        .disSCR(disSCR),
        //.serialIn(T_EDIN00_test),//(soutLeft),
        .serialIn(soutLeft),//(soutLeft),

 		  .BadSync6sec(left_BadSync6sec),
        .Trig1b(left_Trig1b),
        .DV3p(left_DV3p),
        .DataChk1b(left_DataChk1b),
		  
        .RC_BCIDErrorCount(RC_left_BCIDErrorCount),
        .RC_dataType(RC_left_dataType),
        .RC_nullEventCount(RC_left_nullEventCount),
        .RC_goodEventCount(RC_left_goodEventCount),
        .RC_notHitEventCount(RC_left_notHitEventCount),
        .RC_L1OverlfowEventCount(RC_left_L1OverlfowEventCount),
        .RC_totalHitsCount(RC_left_totalHitsCount),
        .RC_dataErrorCount(RC_left_dataErrorCount),
        .RC_missedHitsCount(RC_left_missedHitsCount),
        .RC_hittedPixelCount(RC_left_hittedPixelCount),
        .RC_frameErrorCount(RC_left_frameErrorCount),
        .RC_L1FullEventCount(RC_left_L1FullEventCount),
        .RC_L1HalfFullEventCount(RC_left_L1HalfFullEventCount),
        .RC_SEUEventCount(RC_left_SEUEventCount),
        .RC_goodEventRate(RC_left_goodEventRate),
        .RC_hitCountMismatchEventCount(RC_left_hitCountMismatchEventCount),
        .RC_mismatchBCIDCount(RC_left_mismatchBCIDCount),
        .RC_triggerDataOut(RC_left_triggerDataOut)
    );
	 
	 
	 reg [19:0] RC_right_frameErrorCount_stp;
	 reg [19:0] RC_left_frameErrorCount_stp;
//	2022 reg [39:0] dataframe_debug_right_R_dly;
	 
	 always @(negedge clk8RB)
	 begin
	    RC_right_frameErrorCount_stp<=RC_right_frameErrorCount;
		 RC_left_frameErrorCount_stp<=RC_left_frameErrorCount;
	//2022 dataframe_debug_right_R_dly<=dataframe_debug_right;
	 
	 
	 end
//	2022 always @(posedge clk8RB)
//	 begin
//		dataframe_debug_right_R<=dataframe_debug_right_R_dly;
//	 end
	
	assign probe3B= (20'b0==RC_left_frameErrorCount_stp)?1:0|
	       (20'b0==RC_right_frameErrorCount_stp)?1:0|
			 (1'b0==left_aligned)?1:0| (1'b0==right_aligned)?1:0;
	//assign probe2A=;
			 
//I2C  //////////////////////////////////////
  reg [15:0] size=2;
	reg IIC_START=0;
	wire [15:0] i2c_w_regaddr;
	wire [15:0] i2c_r_regaddr;
	wire [7:0] i2cdata;
   //assign i2cregaddr=`DATA_REGADDR;
	assign i2c_w_regaddr=`DATA_W_REGADDR;
	assign i2c_r_regaddr=`DATA_R_REGADDR;

	// a slow changing counter used as i2cdata
	reg [31:0] cnt_i2cdata;
	always @(posedge clk8RB)
	begin
	  cnt_i2cdata <= cnt_i2cdata - 1'b1;
	end
   assign i2cdata[7:0] = cnt_i2cdata[31:24]; 
   //assign i2cdata =`DATA_VALUE; 

	// Outputs
	wire i2cclk;
	wire [5:0] current_state;
	wire [8:1] Readvalue;
	wire reg_ack;
	wire IIC_END;

	
	


	reg reset_r=1;
	reg reset_d=1;
	wire reset;
	reg [3:0] cnt=4'b0;
	always @(posedge clk8RB)
	begin
	  
	  if (reset_r==1)
	  begin
	     reset_r<=1'b0;
	  end
	  reset_d<=reset_r;
	  
	  if (cnt==10) IIC_START<=1'b1;
	  else cnt<=cnt+1'b1;
	end
	
	assign reset= ~(reset_r ^reset_d);
   assign RSTn_test=reset;

	
	
	localparam WO=    2'b00;// write only
	localparam RO=    2'b01;// read only
	localparam FWLR=  2'b10;// first WRITE, last read
	localparam FRLW=  2'b11;// first READ, last write
	
	wire [7:0] readvalue1;
	wire [7:0] readvalue2;
	wire [7:0] readvalue3;
	wire [7:0] readvalue4;
	wire [7:0] readvalue5;
	wire [7:0] readvalue6;
	wire [7:0] readvalue7;
	wire [7:0] readvalue8;
	wire [7:0] readvalue9;
	wire [7:0] readvalue10;

	// Instantiate the Unit Under Test (UUT)
	I2C_Master I2C_Master_inst (
		.sysclk(clk8RB), 
		//.reset(reset), 
		.reset(1'b1), 
		.sda(SDA_test), 
		.scl(SCL_test), 
		.i2cclk(i2cclk), 
		.current_state(current_state), 
		//.Readvalue(Readvalue), 
		.reg_ack(reg_ack), 
		.size(size), 
		.IIC_START(IIC_START), 
		.IIC_END(IIC_END), 
		//.i2cregaddr(i2cregaddr),
		.i2c_w_regaddr(i2c_w_regaddr),
		.i2c_r_regaddr(i2c_r_regaddr),
		.i2cdata(i2cdata),
		.multi_write_num(9'd4),
		.multi_read_num(9'd5),
		.op_mode(FRLW),
		.readvalue2(readvalue2),
		.readvalue3(readvalue3),
		.readvalue4(readvalue4),
		.readvalue5(readvalue5),
		.readvalue6(readvalue6),
		.readvalue7(readvalue7),
		.readvalue8(readvalue8),
		.readvalue9(readvalue9),
		.readvalue10(readvalue10)
	);
	
wire [3:0]	main_state; 

//wire [15:0] reg_addr_t_buf;
wire [7:0] dat_out_regs;
	
wire  [7:0] dat_in_regs;
wire  wea;
wire [8:0]addra;
wire  ena;


  wire [7:0] regOut00;
  wire [7:0] regOut01;
  wire [7:0] regOut02;
  wire [7:0] regOut03;
  wire [7:0] regOut04;
  wire [7:0] regOut05;
  wire [7:0] regOut06;
  wire [7:0] regOut07;
  wire [7:0] regOut08;
  wire [7:0] regOut09;
  wire [7:0] regOut0A;
  wire [7:0] regOut0B;
  wire [7:0] regOut0C;
  wire [7:0] regOut0D;
  wire [7:0] regOut0E;
  wire [7:0] regOut0F;
  wire [7:0] regOut10;
  wire [7:0] regOut11;
  wire [7:0] regOut12;
  wire [7:0] regOut13;
  wire [7:0] regOut14;
  wire [7:0] regOut15;
  wire [7:0] regOut16;
  wire [7:0] regOut17;
  wire [7:0] regOut18;
  wire [7:0] regOut19;
  wire [7:0] regOut1A;
  wire [7:0] regOut1B;
  wire [7:0] regOut1C;
  wire [7:0] regOut1D;
  wire [7:0] regOut1E;
  wire [7:0] regOut1F;


	
i2c_slave_top i2c_slave_top_inst (
    
    .sda(SDA_0), 
    .scl(SCL_0), 
    .clock(clk8RB),
	 
	 .main_state(main_state),
	//	reg_addr_t_buf,
	.dat_out_regs(dat_out_regs),
	.dat_in_regs(dat_in_regs),
	//addr_in_state,
	//	data_in_state,
	//	data_out_state,
	//	reg_addr_state,
	
    .regOut00(regOut00),
    .regOut01(regOut01),
    .regOut02(regOut02),
    .regOut03(regOut03),
    .regOut04(regOut04),
    .regOut05(regOut05),
    .regOut06(regOut06),
    .regOut07(regOut07),
    .regOut08(regOut08),
    .regOut09(regOut09),
    .regOut0A(regOut0A),
    .regOut0B(regOut0B),
    .regOut0C(regOut0C),
    .regOut0D(regOut0D),
    .regOut0E(regOut0E),
    .regOut0F(regOut0F),
    .regOut10(regOut10),
    .regOut11(regOut11),
    .regOut12(regOut12),
    .regOut13(regOut13),
    .regOut14(regOut14),
    .regOut15(regOut15),
    .regOut16(regOut16),
    .regOut17(regOut17),
    .regOut18(regOut18),
    .regOut19(regOut19),
    .regOut1A(regOut1A),
    .regOut1B(regOut1B),
    .regOut1C(regOut1C),
    .regOut1D(regOut1D),
    .regOut1E(regOut1E),
    .regOut1F(regOut1F),

	.wea(wea),
	.addra(addra),
	.ena(ena)
   
    );



//I2C end///////////////////////////////////
			 
endmodule
