## Generated SDC file "etroc_emulator.sdc"

## Copyright (C) 2018  Intel Corporation. All rights reserved.
## Your use of Intel Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Intel Program License 
## Subscription Agreement, the Intel Quartus Prime License Agreement,
## the Intel FPGA IP License Agreement, or other applicable license
## agreement, including, without limitation, that your use is for
## the sole purpose of programming logic devices manufactured by
## Intel and sold by Intel or its authorized distributors.  Please
## refer to the applicable agreement for further details.


## VENDOR  "Intel Corporation"
## PROGRAM "Quartus Prime"
## VERSION "Version 18.1.0 Build 222 09/21/2018 SJ Pro Edition"

## DATE    "Sun Sep 12 10:34:54 2021"

##
## DEVICE  "10CX220YF780E5G"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************
#create_clock -name {altera_reserved_tck} -period 200.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {clock} -period 125.000 -waveform { 0.000 62.500 } [get_registers {I2C_Master_inst|clock}]
create_clock -name {FPGA_CLK} -period 25.000 -waveform { 0.000 12.500 } [get_ports {FPGA_CLK}]
create_clock -name {ECLK1} -period 25.000 -waveform { 0.000 12.500 } [get_ports {ECLK1}]
create_clock -name {ECLK2} -period 25.000 -waveform { 0.000 12.500 } [get_ports {ECLK2}]
create_clock -name {ECLK3} -period 25.000 -waveform { 0.000 12.500 } [get_ports {ECLK3}]
create_clock -name {ECLK0} -period 25.000 -waveform { 0.000 12.500 } [get_ports {ECLK0}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]} -source [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 2 -master_clock {FPGA_CLK} [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[0]}] 
create_generated_clock -name {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]} -source [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 16 -master_clock {FPGA_CLK} [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[1]}] 
create_generated_clock -name {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]} -source [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 80 -master_clock {FPGA_CLK} [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[2]}] 
create_generated_clock -name {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]} -source [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 80 -phase 180/1 -master_clock {FPGA_CLK} [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[3]}] 
create_generated_clock -name {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]} -source [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 8 -master_clock {FPGA_CLK} [get_pins {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[4]}] 
create_generated_clock -name {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]} -source [get_pins {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 2 -master_clock {ECLK1} [get_pins {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[0]}] 
create_generated_clock -name {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]} -source [get_pins {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 16 -master_clock {ECLK1} [get_pins {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[1]}] 
create_generated_clock -name {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]} -source [get_pins {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 80 -master_clock {ECLK1} [get_pins {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[2]}] 
create_generated_clock -name {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]} -source [get_pins {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 2 -master_clock {ECLK2} [get_pins {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[0]}] 
create_generated_clock -name {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]} -source [get_pins {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 16 -master_clock {ECLK2} [get_pins {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[1]}] 
create_generated_clock -name {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]} -source [get_pins {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 80 -master_clock {ECLK2} [get_pins {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[2]}] 
create_generated_clock -name {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]} -source [get_pins {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 2 -master_clock {ECLK3} [get_pins {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[0]}] 
create_generated_clock -name {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]} -source [get_pins {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 16 -master_clock {ECLK3} [get_pins {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[1]}] 
create_generated_clock -name {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]} -source [get_pins {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 80 -master_clock {ECLK3} [get_pins {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[2]}] 
create_generated_clock -name {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]} -source [get_pins {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 2 -master_clock {ECLK0} [get_pins {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[0]}] 
create_generated_clock -name {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]} -source [get_pins {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 16 -master_clock {ECLK0} [get_pins {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[1]}] 
create_generated_clock -name {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]} -source [get_pins {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|refclk[0]}] -duty_cycle 50/1 -multiply_by 16 -divide_by 80 -master_clock {ECLK0} [get_pins {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|iopll_inst|outclk[2]}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -rise_from [get_clocks {ECLK2}] -rise_to [get_clocks {ECLK2}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {ECLK2}] -fall_to [get_clocks {ECLK2}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {ECLK2}] -rise_to [get_clocks {ECLK2}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {ECLK2}] -fall_to [get_clocks {ECLK2}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.070  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.070  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.070  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.070  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[3]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk2|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk1|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.410  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.080  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.080  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.080  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.080  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {ECLK0}] -rise_to [get_clocks {ECLK0}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {ECLK0}] -fall_to [get_clocks {ECLK0}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {ECLK0}] -rise_to [get_clocks {ECLK0}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {ECLK0}] -fall_to [get_clocks {ECLK0}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.130  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.220  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.060  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -rise_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -fall_from [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}] -fall_to [get_clocks {inst_clk|iopll_0|altera_iopll_i|c10gx_pll|outclk[0]}]  0.040  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {genclk_u4|iopll_0|altera_iopll_i|c10gx_pll|outclk[4]}]  0.390  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[2]}]  0.160  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -rise_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}] -fall_to [get_clocks {inst_clk3|iopll_0|altera_iopll_i|c10gx_pll|outclk[1]}]  0.090  


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

#set_max_skew -from [get_registers {auto_fab_0|alt_sld_fab_0|alt_sld_fab_0|auto_signaltap_auto_signaltap_0|sld_signaltap_inst|sld_signaltap_body|sld_signaltap_body|jtag_acq_clk_xing|intel_stp_status_bits_cdc_u1|stp_status_bits_in_reg[*]}] -to [get_registers {auto_fab_0|alt_sld_fab_0|alt_sld_fab_0|auto_signaltap_auto_signaltap_0|sld_signaltap_inst|sld_signaltap_body|sld_signaltap_body|jtag_acq_clk_xing|intel_stp_status_bits_cdc_u1|stp_status_bits_out[*]}] 3.000